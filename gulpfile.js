var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
gulp.task('sass', function(){
  return gulp.src('./assets/sass/style.scss')
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('./assets/css/'))
});
// // gulp.task('watch', [sass], function(){
// //   gulp.watch('./assets/sass/*.scss', ['sass']); 
// //   // Other watchers
// // });
// gulp.task('browserSync', function() {
//   browserSync.init({
//     server: {
//       baseDir: 'hms'
//     },
//   })
// })