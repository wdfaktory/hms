<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">View Permissions</h2>                                
            </header>
            <?php /*$curdPer = curd_access();             
            echo '<pre>';
            print_r($curdPer); die;*/
            ?>
            <a href="<?=base_url().'permissions/add_permission'?>">Add Permission</a>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Permissions Name</th>
                                        <th>Last Updated Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($permission_details as $permission){?>
                                    <tr>
                                        <td>
                                            <h6><?=$permission->name?></h6>
                                        </td>
                                        <td><?=$permission->modified_date?></td>
                                        
                                        <td>
                                            <a href="<?=base_url().'permissions/permissionUpdate/'.$permission->id ?>">Edit</a>
                                            <a href="<?=base_url().'permissions/delete_permission/'.$permission->id ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>    
</section>                    
<?php $this->load->view('templates/footer'); ?>