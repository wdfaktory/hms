<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row" style=''>

        <div class='col-xs-12'>
            <div class="page-title">

                <div class="pull-left">
                    <!-- PAGE HEADING TAG - START -->
                    <h1 class="title">Student Dashboard</h1>
                    <!-- PAGE HEADING TAG - END -->
                </div>

            </div>
        </div>

        <div class="clearfix"></div>
        <!-- MAIN CONTENT AREA STARTS -->

        <div class="col-lg-4">
            <section class="box ">
                <div class="content-body p">
                    <div class="row">
                        <div class="doctors-list patient relative">
                            <div class="doctors-head relative text-center">
                                <div class="patient-img img-circle">
                                    <?php $stdImgPath = $studImgUrl;//base_url("student_records/".$studData->image_name);
                                    if(file_exists($stdImgPath)) $stdImgPath = 'data/profile/profile.jpg'; ?>
                                    <img src="<?=$stdImgPath?>" class="rad-50 center-block" alt="Image goes here">
                                   <!--  <div class="stutas"></div> -->
                                </div>
                                <h3 class="header w-text relative bold">ICID : <?=$this->uri->segment(2)?></h3>
                                <h3 class="header w-text relative bold">Name : <?=$studData->student_name?></h3>
                                <p class="desc g-text relative"><?=$studData->school_name.', '.$studData->city?></p>
                            </div>
                            <div class="row">
                                <div class="patients-info relative" >
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold"><?=($studData->sex == 1) ? 'Male': 'Female'?></h5>
                                                    <h6>Gender</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold"><?=$studData->standard.''.$studData->section?></h5>
                                                    <h6>Standard/Section</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold">176 cm</h5>
                                                    <h6>Student  Height</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold"> 67 Kg</h5>
                                                    <h6>Student Weight</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold"> <?=$studData->age?></h5>
                                                    <h6>Age</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="patient-card has-shadow2">
                                            <div class="doc-info-wrap">
                                                <div class="patient-info">
                                                    <h5 class="bold"> 
                                                    <?php if($studData->health_score>=75&&$studData->health_score<=100){
                                                        $badgeColour = 'badge-success';
                                                    } else if($studData->health_score>=50&&$studData->health_score<=74){
                                                        $badgeColour = 'sick_cond';
                                                    } else{
                                                        $badgeColour = 'badge-danger';
                                                    }
                                                    
                                                    ?>
                                                    <span class="badge <?=$badgeColour?>"><?=$studData->health_score?>%</span></h5>
                                                    <h6>Health score</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </section>
        </div>


            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <section class="box gradient-blue" style="padding:20px">
                        <div class="patient-personal mb-0">
                            <h4 class="w-text">RH type :</h4>
                            <p class="mb-0 g-text"><?=$studData->blood_group?></p>
                        </div>
                        <div class="patient-personal mb-0">
                            <h4 class="w-text">Allergies :</h4>
                            <p class="mb-0 g-text">Penicilin, peanuts</p>
                        </div>
                        <div class="patient-personal mb-0">
                            <h4 class="w-text">Diseases :</h4>
                            <p class="mb-0 g-text">Diabetes</p>
                        </div>
                        <div class="patient-personal mb-0">
                            <h4 class="w-text">Pressure :</h4>
                            <p class="mb-0 g-text">130/80 mmHG</p>
                        </div>
                        <div class="patient-personal mb-0">
                            <h4 class="w-text">Temperture :</h4>
                            <p class="mb-0 g-text">36.8 Degree</p>
                        </div>

                    </section>
                </div>
                <div class="col-md-6 col-sm-12">
                    <section class="box gradient-pink" style="padding:20px">
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text"> <span class="text-info bold">-- </span> Reported Conditions</h4>
                            <p class="mb-0 g-text"></p>
                        </div>
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text">Sick</h4>
                            <p class="mb-0 g-text">20/02/2020</p>
                        </div>
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text">Head Ache </h4>
                            <p class="mb-0 g-text">01/01/2020</p>
                        </div>
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text">Stomach Problem</h4>
                            <p class="mb-0 g-text">22/07/2019</p>
                        </div>
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text">Fever</h4>
                            <p class="mb-0 g-text">01/03/2019</p>
                        </div>
                        <div class="patient-personal v2 mb-0">
                            <h4 class="w-text" style="color: #000 !important; text-decoration: underline;">View all reports</h4>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        

        <div class="clearfix"></div>

        <div class="col-md-7 col-sm-12 col-xs-12">

            <div class="row mt-15">
                <div class="col-md-6 col-xs-12 ">
                    <div class="r1_graph1 db_box db_box_large has-shadow2">
                        <div class="pat-info-wrapper">
                            <div class="pat-info text-left">
                                <h5 class=''>Blood pressure</h5>
                                <h6>In the normal</h6>
                            </div>
                            <div class="pat-val relative">
                                <h4 class="value p-text">120/89 <span>mmHG</span></h4>
                            </div>
                        </div>
                        <span class="sparklinedash2">Loading...</span>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="r1_graph1 db_box db_box_large has-shadow2">
                        <div class="pat-info-wrapper">
                            <div class="pat-info text-left">
                                <h5 class=''>Heart Rate</h5>
                                <h6 class="red-text">Above the normal</h6>
                            </div>
                            <div class="pat-val relative">
                                <h4 class="value red-text">107 <span>Per min</span></h4>
                            </div>
                        </div>
                        <span class="sparklinedash">Loading...</span>
                    </div>
                </div>
                
                <div class="col-md-6 col-xs-12">
                    <div class="r1_graph1 db_box db_box_large has-shadow2">
                        <div class="pat-info-wrapper">
                            <div class="pat-info text-left">
                                <h5 class=''>Glucose Rate</h5>
                                <h6>In the normal</h6>
                            </div>
                            <div class="pat-val relative">
                                <h4 class="value green-text"><i class="complete fa fa-arrow-up"></i>97<span>mg/dl</span></h4>
                            </div>
                        </div>
                        <span class="sparkline8">Loading...</span>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="r1_graph1 db_box db_box_large has-shadow2">
                        <div class="pat-info-wrapper">
                            <div class="pat-info text-left">
                                <h5 class=''>Clolesterol</h5>
                                <h6>In the normal</h6>
                            </div>
                            <div class="pat-val relative">
                                <h4 class="value blue-text"><i class="cancelled fa fa-arrow-down"></i>124<span>mg/dl</span></h4>
                            </div>
                        </div>
                        <span class="sparkline9">Loading...</span>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-xs-12 col-md-5">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Student Activities(Future Module)</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                        <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                        <a class="box_close fa fa-times"></a>
                    </div>
                </header>
                <div class="content-body pb0">    
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                            <canvas id="radar-chartjs" width="300" height="300"></canvas>
                        </div>
                    </div>
                </div>
            </section>
        </div> 
        <div class="col-md-6 col-sm-12">
            <section class="box gradient-blue" style="padding:20px">
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text"> <span class="text-info bold"></span></h4>
                    <p class="mb-0 g-text"></p>
                </div>
                <?php if($studData->sex == 2) {?>
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text">Pube (Y/N)</h4>
                    <p class="mb-0 g-text"><?=$studData->pube?'Yes':'No'?></p>
                </div>
                <?php } ?>
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text">Special Care Need (Y/N)</h4>
                    <p class="mb-0 g-text"><?=$studData->special_care_need?'Yes':'No'?></p>
                </div>
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text">Under Vaccination (Y/N)</h4>
                    <p class="mb-0 g-text"><?=$studData->under_vaccination?'Yes':'No'?></p>
                </div>
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text">Alergitic (Y/N)</h4>
                    <p class="mb-0 g-text"><?=$studData->alergitic?'Yes':'No'?></p>
                </div>
                <div class="patient-personal v2 mb-0">
                    <h4 class="w-text">Asthma (Y/N)</h4>
                    <p class="mb-0 g-text"><?=$studData->asthma?'Yes':'No'?></p>
                </div>
            </section>
        </div>
        <div class="clearfix"></div>
        <!-- MAIN CONTENT AREA ENDS -->
    </div>
</section>

<?php $this->load->view('templates/footer'); ?>

