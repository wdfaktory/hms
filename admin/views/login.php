<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" type="text/css">
	
	<link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<title>Home</title>
</head>
<body>
<div class="login-container">
	<div class="">
		<div class="login-form text-center">
			<p class="logo"><img src="<?=base_url('assets/images/dummylogo.jpg')?>" /></p>
			<h2>Infocare Health Solutions</h2>
			<p>Track your childs health, help them stay healthy.</p>
			<?php if(!empty($this->session->flashdata('is_invalid'))){ ?>
			<div class="alert alert-error">
			  <span>
				<p style="color:red;">Invalid login credentials!</p>
			  </span>
			</div>
			<?php } ?>
			<form action="<?=base_url('login/check_login')?>" method="POST" class="login">
			    <div class="form-group">
			    <label for="name">Email</label>
			    <input type="email" class="form-control" name="name" id="name" <?php if ($this->session->email_sess != '' && $this->session->remember == 1) { ?> value="<?php echo $this->session->email_sess; ?>"<?php } ?> required>
			  </div>
			    <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" class="form-control" name="password" id="password" <?php if ($this->session->password != '' && $this->session->remember == 1) { ?> value="<?php echo $this->session->password; ?>"<?php } ?> required>
			  </div>
		    <div class="form-group">        
		        <div class="rememberMe">
				<?php //echo $this->session->remember; die;?>
		          <label><input type="checkbox" name="remember" <?php if ($this->session->remember == 1) { echo 'checked="checked"'; } ?>> Remember me</label>
		        </div>
		        <div class="forgetPassword">
		        	<a href="<?=base_url().'users/open_reset_pwd'?>">Forgot Password</a>
		        </div>
		    </div>
		      <div class="form-group login-btn">        
        			<button type="submit" class="btn btn-primary">Log In</button>
    			</div>
			</form>	
		</div>
	</div>
</div>
</body>
  
</html>
