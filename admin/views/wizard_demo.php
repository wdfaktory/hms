<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row" style=''>
    
        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Form Wizard with validations</h2>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">

                            <form id="commentForm" method="post">

                                <div id="formpills" class='wizardpills'>
                                    <ul class="form-wizard">
                                        <li><a href="#pills-tab1" data-toggle="tab"><span>Basic</span></a></li>
                                        <li><a href="#pills-tab2" data-toggle="tab"><span>Profile</span></a></li>
                                        <li><a href="#pills-tab3" data-toggle="tab"><span>Address</span></a></li>
                                        <li><a href="#pills-tab4" data-toggle="tab"><span>Settings</span></a></li>
                                    </ul>
                                    <div class="progress active">
                                        <div class="progress-bar progress-bar-primary progress-bar-striped " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="pills-tab1">

                                            <h4>Basic Information</h4>
                                            <br>
                                            <div class="form-group">
                                                <label class="form-label">Full Name</label>
                                                <div class="controls">
                                                    <input type="text" placeholder="Full Name" class="form-control" name="fullName" id="txtFullName">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Email</label>
                                                <div class="controls">
                                                    <input type="text" placeholder="Email" class="form-control" name="email" id="txtEmail">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Phone</label>
                                                <div class="controls">
                                                    <input type="text" placeholder="Phone" class="form-control" name="phone" id="txtPhone">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="pills-tab2">
                                            <h4>Profile Information</h4>
                                            <br>
                                            <div class="form-group">
                                                <label class="form-label">contact</label>
                                                <div class="controls">
                                                    <input type="text" placeholder="Phone" class="form-control" name="contact" id="txtPhone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="pills-tab3">
                                            <h4>Contact Information</h4>
                                            <br>
                                            <p>Form goes here</p>
                                        </div>
                                        <div class="tab-pane" id="pills-tab4">
                                            <h4>Settings Information</h4>
                                            <br>
                                            <p>Form goes here</p>
                                        </div>
                                        <div class="tab-pane" id="pills-tab5">
                                            <h4>Portfolio Information</h4>
                                            <br>
                                            <p>Form goes here</p>
                                        </div>

                                        <div class="clearfix"></div>

                                        <ul class="pager wizard">
                                            <li class="previous first" style="display:none;"><a href="javascript:;">First</a></li>
                                            <li class="previous"><a href="javascript:;">Previous</a></li>
                                            <li class="next last" style="display:none;"><a href="javascript:;">Last</a></li>
                                            <li class="next"><a href="javascript:;">Next</a></li>
                                            <li class="finish hidden"><a href="javascript:;">Finish</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    

    </div>
</section>

<?php $this->load->view('templates/footer'); ?>
<script>
//Form Wizard Validations
// var $validator = $("#commentForm").validate({
//     rules: {
//         txtFullName: {
//             required: true,
//             minlength: 3
//         },
//         txtEmail: {
//             required: true,
//             email: true,
//         },
//         txtPhone: {
//             number: true,
//             required: true,
//         }
//     }
// });

$('#formpills').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'debug': false,
            onShow: function(tab, navigation, index) {
                console.log('onShow');
            },
            onNext: function(tab, navigation, index) {
                console.log('onNext');
                if(index == 3){
                    $('.finish').removeClass('hidden');
                }
                if ($.isFunction($.fn.validate)) {
                    var $valid = $("#commentForm").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        $('#formpills').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
                        $('#formpills').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
                    }
                }
            },
            onPrevious: function(tab, navigation, index) {
                console.log('onPrevious');
                if(index < 3){
                    $('.finish').addClass('hidden');
                }
            },
            onLast: function(tab, navigation, index) {
                console.log('onLast');
            },
            onTabClick: function(tab, navigation, index) {
                console.log('onTabClick');
                //alert('on tab click disabled');
            },
            onTabShow: function(tab, navigation, index) {
                console.log('onTabShow');
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#formpills .progress-bar').css({
                    width: $percent + '%'
                });
            }
        });
    
    $('#formpills .finish').click(function() {
        alert('Finished!, Starting over!');
        // $('#pills').find("a[href*='tab1']").trigger('click');
        $('#commentForm').submit();
    });
</script>