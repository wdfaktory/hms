<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
        <div class="col-lg-12">
            <section class="box">
                <header class="panel_header">
                    <h2 class="title pull-left">View Designations</h2>                                
                </header>
                <?php /*$curdPer = curd_access();             
                echo '<pre>';
                print_r($curdPer); die;*/
                ?>
                <a href="<?=base_url().'designations/add_designation'?>">Add Designation</a>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Designation Name</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    foreach($designation_details as $designation){?>
                                        <tr <?php if($designation->deleted==1) echo 'style="opacity:0.4"'?>>
                                            <td><h6><?=$designation->designation?></h6></td>
                                            <td>
                                                <?php if($designation->deleted==0) { ?>
                                                <a href="<?=base_url().'designations/designationUpdate/'.$designation->id ?>">Edit</a>
                                                <a href="<?=base_url().'designations/delete_designations/'.$designation->id ?>">Disable</a>
                                                <?php } else { ?>
                                                    <a href="<?=base_url().'designations/delete_designations/'.$designation->id.'/0' ?>">Enable</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>    
    </div>    
</section>                    
<?php $this->load->view('templates/footer'); ?>