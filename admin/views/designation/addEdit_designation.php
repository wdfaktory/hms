<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
    <!-- START CONTENT -->
    <section id="main-content" class=" ">
        <div class="wrapper main-wrapper row" style=''>

            <div class="clearfix"></div>
            <!-- MAIN CONTENT AREA STARTS -->

            <div class="col-lg-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Add/Edit Designation</h2>                            
                    </header>
                    <div class="content-body">
                        <div class="row">
                            <div class="col-xs-12">

                                <form id="commentForm" method="POST" action="<?=base_url().'designations/insert_designation/'.(isset($designationData->id)?$designationData->id:0)?>">

                                    <div id="pills" class='wizardpills'>
                                        <ul class="form-wizard">
                                            <li><a href="#pills-tab1" data-toggle="tab"><span>Basic Designation</span></a></li>
                                            
                                        </ul>
                                        <div id="bar" class="progress active">
                                            <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                        </div>
                                        <input type='hidden' name='designationId' id='designationData' value="<?=(isset($designationData->id)?$designationData->id:0)?>" />
                                        <div class="tab-content">
                                            <div class="tab-pane" id="pills-tab1">
                                                <div class="form-group col-lg-6">
                                                    <label class="form-label">Designation Name</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="Designation Name" class="form-control" name="designation_name" id="designation_name" value="<?=(isset($designationData->designation)?$designationData->designation:'')?>"  required>
														
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group col-lg-6">
                                            <div class="controls">
                                                <input type="submit" value="Submit" class="addNew">	
                                                <a href="<?=base_url('designations')?>" class="btn btn-warning">Cancel</a>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <!-- MAIN CONTENT AREA ENDS -->
        </div>
    </section>        
</div>
<?php $this->load->view('templates/footer'); ?>