<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" type="text/css">
	<!-- <link href="<?=base_url('assets/css/all.css')?>" rel="stylesheet" type="text/css"> -->
	<!-- <link href="<?=base_url('assets/css/brands.min.css')?>" rel="stylesheet" type="text/css"> -->
	<!-- <link href="<?=base_url('assets/css/regular.min.css')?>" rel="stylesheet" type="text/css"> -->
	<link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<title>Home</title>
</head>
<body>

	<div class='userPage'>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">
		      	<img src="" />
		      </a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i></a></li>
		      <li><a href="#"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a></li>
		    </ul>
		  </div>
		</nav>

		<section class="wrapper">
			<aside class="col-4">
				<ul>
					<li class="active info"> <a href=""><span>Personal Info</span></a> </li>
					<li class="Buddy"> <a href=""><span>Buddy Up</span></a> </li>
					<li class="health"> <a href=""><span>Health Status</span></a> </li>	
					<li class="activity"> <a href=""><span>Activity</span></a> </li>	
					<li class="articles"> <a href=""><span>Articles</span></a> </li>	
					<li class="notification"> <a href=""><span>Notification</span></a> </li>
					<li class="payments"> <a href=""><span>Payments & Subsc</span></a> </li>	
													
				</ul> 
			</aside>
			<article class="col-8">
				<section class="personal-info right-container">
					  <ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#tab1">Parent Details</a></li>
					    <li><a data-toggle="tab" href="#tab2">Children Details</a></li>
					    <li><a data-toggle="tab" href="#tab3">Emergency Contact Details</a></li>
					  </ul>

					  <div class="tab-content">
					    <div id="tab1" class="tab-pane fade in active">
					      	<h3>Parent Details</h3>
					      	<form action="" method="" class="parent-dtl" id="parent-dtl">
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="fname">
						    		<label for="name">Father name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="mname">
						    		<label for="name">Mother name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="foccupation">
						    		<label for="occupation">Father Occupation</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="number" class="form-control" id="phoneno">
						    		<label for="phone">Phone Number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="mobileno">
						    		<label for="mobile">Mobile number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="email" class="form-control" id="email">
						    		<label for="email">Email Id</label>
						  		</div>
						    	<div class="form-group col-lg-12">
						    		<textarea minlength="15" class="form-control"></textarea>
						    		<label for="address">Address</label>
						  		</div>
					      	</form>
					    </div>
					    <div id="tab2" class="tab-pane fade">
					      <h3>Menu 2</h3>
					      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					    </div>
					    <div id="tab3" class="tab-pane fade">
					      <h3>Menu 3</h3>
					      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					    </div>
					  </div>					
				</section>
			</article>
		</section>		
	</div>

<!-- <div class="login-container">
	<div class="">
		<div class="login-poster">
			<img src="<?=base_url('assets/images/log.jpg')?>" />
		</div>
		<div class="login-form text-center">
			<h2>Lorem ipsum dolor sit amet</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<form action="" method="" class="login">
			  <div class="form-group">
			    <label for="type">User Type( Select any one)</label><br>
			     <label class="container parents">
			     	<input type="radio" name="type" id="parents">
			     	<span class="checkmark"></span>
			     	<b>Parents</b>
			     </label>
			    <label class="container school">
			    	<input type="radio" name="type" id="school">
			    	<span class="checkmark"></span>
			    	<b>School</b>
			    </label>
			  </div>
			    <div class="form-group">
			    <label for="name">User name</label>
			    <input type="text" class="form-control" id="name">
			  </div>
			    <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" class="form-control" id="password">
			  </div>
		    <div class="form-group">        
		        <div class="rememberMe">
		          <label><input type="checkbox" name="remember"> Remember me</label>
		        </div>
		        <div class="forgetPassword">
		        	<a href="#">Forget Password</a>
		        </div>
		    </div>
		      <div class="form-group login-btn">        
        			<button type="submit" class="btn btn-primary">Log In</button>
    			</div>
			</form>	
		</div>
	</div>
</div> -->

</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
<!-- <script type="text/javascript" src="<?=base_url('assets/js/all.js')?>"></script> -->
</html>