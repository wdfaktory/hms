<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">Parents Lists</h2>                                
            </header>
            <!-- <a href="#">Add Parents</a> -->

            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                <thead>
                                    <tr style="white-space:nowrap;">
                                        <th>ICID</th>
                                        <th>School ID</th>
                                        <th>Students ID</th>
                                        <th>Father Name</th>
                                        <th>Mother Name</th>
                                        <th>Primary Contact Number</th>
                                        <th>Secondary Contact Number</th>
                                        <th>Guardian Contact Number</th>
                                        <th>Email</th>
                                        <th>Payment Status</th>
                                        <th>Subscription Expiry Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($parents as $key => $parent) { ?>
                                    <tr <?php if($parent->deleted==1) echo 'style="opacity:0.4"'?>>
                                        <td><?= $parent->icid?></td>
                                        <td><?= $parent->school_id?></td>
                                        <td><?= $parent->student_id?></td>
                                        <td><?= $parent->father_name?></td>
                                        <td><?= $parent->mother_name?></td>
                                        <td><?= $parent->phone_number?></td>
                                        <td><?= $parent->mobile_number?></td>
                                        <td>-</td>
                                        <td><?= $parent->email?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>
                                            <?php if($parent->deleted==0){?>
                                            <a href="#">Edit</a><br>
                                            <a href="<?=base_url().'parents/disable_parent/'.$parent->id ?>" onclick="return confirm('Are you sure want to disable?')">Disable</a><br>
                                            <a style="white-space:nowrap;" href="#">Send Reminder</a><br>
                                            <a style="white-space:nowrap;" href="#">Payment History</a>
                                            <?php } else {?>
                                                <a href="<?=base_url().'parents/disable_parent/'.$parent->id.'/0' ?>" onclick="return confirm('Are you sure want to enable?')">Enable</a>
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<?php $this->load->view('templates/footer'); ?>