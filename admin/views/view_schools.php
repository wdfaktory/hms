<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">View Schools</h2>                                
            </header>
            <?php 
            if(in_array(2, $school_permissions) || in_array(1, $school_permissions)){
            ?>
            <a href="<?=base_url().'users/schoolUpdate'?>">Add School</a>
            <?php } ?>
            <div class="content-body">
                <div class="row">
                    <form method="get" action="<?=base_url()?>school">
                        <div class="form-group col-xs-6">
                            <div class="controls">
                                <input class="form-control" placeholder="Search" type="search" name="search_query" id="search_query" value="<?= isset($_GET['search_query'])?$_GET['search_query']:''?>">
                            </div>
                        </div>
                        <div class="form-group col-xs-2">
                            <div class="controls">
                                <input class="form-control" type="submit" value="Search">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                <thead>
                                    <tr style="white-space:nowrap;">
                                        <th>School Id</th>
                                        <th>Name</th>
                                        <th>School Contact Number</th>
                                        <th>Sponsors Details</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Address</th>
                                        <?php if(!empty($school_permissions) || in_array(1, $school_permissions)){ ?>
                                            <th>Actions</th> 
                                        <?php } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($school_det as $school){?>
                                    <tr <?php if($school->deleted==1) echo 'style="opacity:0.4"'?>>
                                        <td><?=$school->id?></td>
                                        <td>
                                            <h6><?=$school->school_name?></h6>
                                        </td>
                                        <td><?=$school->phone?></td>
                                        <td>
                                            <?php if($school->sponsor_name){ ?>
                                                <p><?=$school->sponsor_name?></p>
                                                <p><?=$school->cont_number?></p>
                                                <p><?=$school->sponsor_email?></p>
                                            <?php } else { ?>
                                                <p> - </p>
                                            <?php } ?>
                                        </td>
                                        <td><?php 
                                        $schoolemail = school_email($school->user_id);
                                        echo $schoolemail;
                                        ?></td>
                                        <td><?=$school->city?></td>
                                        <td><?=$school->address?></td>
                                        <?php if(!empty($school_permissions) || in_array(1, $school_permissions)){ ?>
                                        <td>
                                            <?php if((in_array(3, $school_permissions) || in_array(1, $school_permissions)) && $school->deleted==0){ ?>
                                            <a href="<?=base_url().'users/schoolUpdate/'.$school->id ?>">Edit</a>
                                            <?php } if((in_array(5, $school_permissions) || in_array(1, $school_permissions)) && $school->deleted==0){ ?>
                                            <a href="<?=base_url().'users/delete_school/'.$school->id ?>" onclick="return confirm('Are you sure want to disable?')">Disable</a>
                                        <?php } if((in_array(5, $school_permissions) || in_array(1, $school_permissions)) && $school->deleted!=0){ ?>
                                            <a href="<?=base_url().'users/delete_school/'.$school->id.'/0' ?>" onclick="return confirm('Are you sure want to enable?')">Enable</a>
                                        <?php } ?>    
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if(empty($links)){?>
                    <nav aria-label="Page navigation">
                        <ul class="pagination pull-right">
                            <li class="disabled">
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="disabled">
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                <?php } else {?>
                <?= $links?>
                <?php } ?>
            </div>
        </section>
    </div>    
    </div>
</section>                    
<?php $this->load->view('templates/footer'); ?>