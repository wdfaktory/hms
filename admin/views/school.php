<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add Schools</h2>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    <form id="commentForm" method="POST" action="<?=base_url().'users/add_school/'.(isset($schoolData->id)?$schoolData->id:0)?>">

                                        <div id="pills" class='wizardpills'>
                                            <ul class="form-wizard">
                                                <li><a href="#pills-tab1" data-toggle="tab"><span>Basic</span></a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab"><span>Profile</span></a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab"><span>Address</span></a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab"><span>Settings</span></a></li>
                                            </ul>
                                            <div id="bar" class="progress active">
                                                <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <input type='hidden' name='schoolId' id='schoolId' value="<?=(isset($schoolData->id)?$schoolData->id:0)?>" />
                                            <div class="tab-content">
                                                <div class="tab-pane" id="pills-tab1">

                                                    <h4>Basic Information</h4>
                                                    <br>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Full Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Full Name" class="form-control" name="name" id="txtFullName" value="<?=(isset($userData->name)?$userData->name:'')?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">School Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="School Name" class="form-control" name="schoolname" id="txtSchoolName" value="<?=(isset($schoolData->school_name)?$schoolData->school_name:'')?>"  required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Email</label>
                                                        <div class="controls">
                                                            <input type="email" placeholder="Email" class="form-control" name="email" id="txtEmail"  value="<?=(isset($userData->email)?$userData->email:'')?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Phone</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Phone" class="form-control" name="phone" id="txtPhone" value="<?=(isset($userData->phone)?$userData->phone:'')?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">City</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="City" class="form-control" name="city" id="txtCity" required value="<?=(isset($schoolData->city)?$schoolData->city:'')?>" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Address</label>
                                                        <textarea minlength="15" class="form-control" name="address" required> <?=(isset($schoolData->address)?$schoolData->address:'')?></textarea>
                                                    </div>
                                                    <input type="hidden" value="3" name="user_type">
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Zone</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Zone" class="form-control" name="zone" id="txtZone" required value="<?=(isset($schoolData->zone)?$schoolData->zone:'')?>" >
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row col-lg-12">
                                                        <h4>Sponsors Details</h4>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Sponsors Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Sponsors Name" class="form-control" name="sponsor_name" id="txtspnname" required value="<?=(isset($schoolData->sponsor_name)?$schoolData->sponsor_name:'')?>" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Contact Number</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Contact Number" class="form-control" name="cont_number" id="txtcontnumber" required value="<?=(isset($schoolData->cont_number)?$schoolData->cont_number:'')?>" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Email ID</label>
                                                        <div class="controls">
                                                            <input type="email" placeholder="Sponsor Email" class="form-control" name="sponsor_email" id="txtsponsorEmail"  value="<?=(isset($schoolData->sponsor_email)?$schoolData->sponsor_email:'')?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Designation</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Designation" class="form-control" name="sponsor_design" id="txtspndesign" required value="<?=(isset($schoolData->sponsor_design)?$schoolData->sponsor_design:'')?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="submit" value="Submit" class="addNew">                                                
                                                <a href="<?=base_url('school')?>" class="btn btn-warning">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        
    </div>
   <?php $this->load->view('templates/footer'); ?>