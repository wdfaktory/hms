<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box " style="width: 30%; margin-left: 30%;">
                        <header class="panel_header">
                            <h2 class="title pull-left"><b>Your Profile</b></h2>
                            <div class="title controls pull-right"> 
                                <a href="<?=base_url().'users/edit_profile'?>">Edit</a>
                            </div>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                <?php if($this->session->flashdata('success')):?>
                                    <div class="alert alert-success"><?=$this->session->flashdata('success')?></div>	
                                <?php endif; ?><?php if($this->session->flashdata('status')):?>
                                    <div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
                                <?php endif; ?><?php if($this->session->flashdata('error')):?>
                                    <div class="alert alert-danger"><?=$this->session->flashdata('error')?></div>	
                                <?php endif; ?>

                                    <form id="commentForm" method="POST" action="<?=base_url().'users/add_school/'.(isset($schoolData->id)?$schoolData->id:0)?>">

                                        
                                            <div class="container">
                                                
													
                                                    <div class="form-group">
                                                       
                                                        <div class="controls"> <label class="form-label"><b>Full Name :</b></label>
														<span><?php echo $userdata->name;?></span>
                                                           
                                                        </div>
                                                    </div>
													
													<div class="form-group">
                                                       
                                                        <div class="controls"> <label class="form-label"><b>Email :</b></label>
														<span><?php echo $userdata->email;?></span>
                                                           
                                                        </div>
                                                    </div>
													
													<div class="form-group">
                                                       
                                                        <div class="controls"> <label class="form-label"><b>Phone :</b></label>
														<span><?php echo $userdata->phone;?></span>
                                                           
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                       
                                                        <div class="controls"> <label class="form-label"><b>Profile Image :</b></label>
														<span><img style="width:3%;" src="<?= base_url().($userdata->profile_image?('assets/uploads/'.$userdata->profile_image):'assets/images/user.png')?>" alt="profile image"></span>
                                                           
                                                        </div>
                                                    </div>
													<!-- <div class="form-group">
                                                       
                                                        <div class="controls"> <label class="form-label"><b>Permission :</b></label>
														<span><?php 
														$permissions = permssion_details($userdata->permission);
														echo $permissions;
														?></span>
                                                           
                                                        </div>
                                                    </div> -->
													
													<div class="form-group">
                                                       
                                                        <div class="controls"> 
															<a href="<?=base_url().'users/user_change_pwd'?>"> Click to reset Password</a>
                                                           
                                                        </div>
                                                    </div>
                                                    
                                                                                        
                                            </div>
                                       
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        
    </div>
   <?php $this->load->view('templates/footer'); ?>