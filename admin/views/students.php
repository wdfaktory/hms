<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" type="text/css">
	<!-- <link href="<?=base_url('assets/css/all.css')?>" rel="stylesheet" type="text/css"> -->
	<!-- <link href="<?=base_url('assets/css/brands.min.css')?>" rel="stylesheet" type="text/css"> -->
	<!-- <link href="<?=base_url('assets/css/regular.min.css')?>" rel="stylesheet" type="text/css"> -->
	<link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<title>Home</title>
	<style>
		.userPage .parent-dtl .form-control {
    height: 60px;
    font-size: 18px;
    color: #333333DE;
    font-weight: 600;
    padding-top: 20px;
}
	</style>
</head>

<section id="main-content" class="">
    <div class="wrapper main-wrapper row">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">View Students</h2>                                
            </header>
            
            <div class="userPage">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="personal-info right-container">
					  <ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#tab1">Parent Details</a></li>
					    <li><a data-toggle="tab" href="#tab2">Children Details</a></li>
					    <li><a data-toggle="tab" href="#tab3">Emergency Contact Details</a></li>
					  </ul>

					  <div class="tab-content">
					    <div id="tab1" class="tab-pane fade in active">
					      	<h3>Parent Details</h3>
					      	<form action="<?=base_url().'parents/add_students'?>" method="POST" class="parent-dtl" id="parent-dtl">
					      		<div class="form-group col-lg-6">                                    
                                    <select class="form-control m-bot15" name="school" required>
                                        <option value=''>Schools</option>    
                                        <?php foreach($schools as $school){ ?>
                                        	<option value=<?=$school->id?>><?=$school->school_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="studentname" name="studentname" required>
						    		<label for="studentname">Student name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="standard" name="standard" required>
						    		<label for="standard">Standard</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="section" name="section" required>
						    		<label for="section">Section</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="number" class="form-control" id="age" name="age" required>
						    		<label for="age">Age</label>
						  		</div>
						    	<div class="form-group col-lg-6">                                    
                                    <select class="form-control m-bot15" id="sex" name="sex" required>
                                        <option value=''>Sex</option>    
                                        <option value='1'>Male</option>
                                        <option value='2'>Female</option>
                                    </select>
                                </div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="bgroup" name="bgroup" required>
						    		<label for="bgroup">Blood Group</label>
						  		</div>	
						  		<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="fname" name="fname" required>
						    		<label for="name">Father name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="mname" name="mname" required>
						    		<label for="name">Mother name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="foccupation" name="foccupation" required>
						    		<label for="occupation">Father Occupation</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="number" class="form-control" id="phoneno" name="phoneno" required>
						    		<label for="phone">Phone Number</label>
						  		</div>
						  		<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="mobileno" name="mobileno" required>
						    		<label for="mobile">Mobile number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="email" class="form-control" id="email" name="email" required>
						    		<label for="email">Email Id</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<textarea minlength="15" class="form-control" name="address" required></textarea>
						    		<label for="address">Address</label>
						  		</div>	
						  		<div class="clearfix"></div>
						  		<input type="submit" value="Submit">				    	
					      	</form>
					    </div>
					    <div id="tab2" class="tab-pane fade">
					      <h3>Menu 2</h3>
					      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					    </div>
					    <div id="tab3" class="tab-pane fade">
					      <h3>Menu 3</h3>
					      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					    </div>
					  </div>					
				</section>
                    </div>
                </div>
            </div>
        </section>
    </div>    
    </div>
</section>                    
<?php $this->load->view('templates/footer'); ?>
