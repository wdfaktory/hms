<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add Users</h2>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form id="commentForm" method="POST" action="<?=base_url().'users/insert_user'?>">
                                        <div id="pills" class='wizardpills'>
                                            <ul class="form-wizard">
                                                <li><a href="#pills-tab1" data-toggle="tab"><span>Basic</span></a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab"><span>Profile</span></a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab"><span>Address</span></a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab"><span>Settings</span></a></li>
                                            </ul>
                                            <div id="bar" class="progress active">
                                                <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane" id="pills-tab1">
                                                    <h4>Basic Information</h4>
                                                    <br>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">First Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="First Name" class="form-control" name="first_name" id="txtFirstName" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Last Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Last Name" class="form-control" name="last_name" id="txtLastName" required>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Email</label>
                                                        <div class="controls">
                                                            <input type="email" placeholder="Email" class="form-control" name="email" id="txtEmail" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Phone</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Phone" class="form-control" name="phone" id="txtPhone">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Designation</label>
                                                        <select class="form-control m-bot15" name="designation">
                                                            <option value=''>Select</option>    
                                                            <?php 
                                                            foreach($designations as $designation){?>
                                                                <option value='<?=$designation->id?>'><?=$designation->designation?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-lg-12">
                                                        <label class="form-label">Permissions</label>
                                                        <label id='perCheck'></label>
                                                        <ul class="list-unstyled permission_ul">
                                                            <?php foreach($permissions as $permission){ ?>
                                                                <li>
                                                                    <!-- <input tabindex="5" type="checkbox" class="icheck-minimal-red permission_cls" name="permission[]" value="<?=$permission->id?>" > -->
                                                                    <input type='hidden' name="permission[]" value="<?=$permission->id?>" >
                                                                    <label class="icheck-label form-label" for="minimal-checkbox-1" style="text-decoration: underline;"><b><?=$permission->name?></b></label>
                                                                    <div class="check">
                                                                    <?php 
                                                                    $curdpermissions = $this->config->item('curdPermissions');
                                                                    foreach($curdpermissions as $key => $curd){ ?>
                                                                        <input tabindex="5" type="checkbox"  class="icheck-minimal-red curd_common curd_access_<?=$permission->id?>" name="curd[<?=$permission->id?>][]" value="<?=$key?>">
                                                                        <label class="icheck-label form-label" for="minimal-checkbox-1"><?=$curd?></label>
                                                                    <?php }
                                                                    ?>
                                                                    </div>
                                                                </li>    
                                                            <?php } ?>                                                    
                                                        </ul>
                                                    </div>
                                                    <input type="hidden" value="2" name="user_type">
                                                </div>
                                                

                                                <div class="clearfix"></div>
                                                <input type="submit" value="Submit" class="addNew">
                                                <a href="<?=base_url('users')?>" class="btn btn-warning">Cancel</a>
                                                 <!-- <ul class="pager wizard">                                                    
                                                    <li class="next"><a href="javascript:;">Next</a></li>
                                                </ul>  -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        

        
    </div>
   <?php $this->load->view('templates/footer'); ?>

   <script>
       $(document).ready(function(){ 
            $('.curd_common').click(function(){
                var currentvalue = $(this).val();                
                if(currentvalue == 1){     
                   /* if($(this).prop("checked") == true){
                        var prevvalue = $(this).prev('label').prev('input').val();                           
                        $('.curd_access_'+prevvalue).attr('checked',true);
                    }else if($(this).prop("checked") == false){
                        var prevvalue = $(this).prev('label').prev('input').val();                                    
                        $('.curd_common').attr('checked',false);
                    }*/                    
                    $(this).siblings(":not('.permission_cls')").prop('checked', $(this).is(':checked'));
                }
                
            }); 

            $('.permission_cls').click(function(){
                if ($(this).is(":checked")) {                                     
                    var crval = $(this).val();
                    $('.curd_access_'+crval).attr('required',true);
                } else { 
                    var crval = $(this).val();
                    $('.curd_access_'+crval).removeAttr('required');
                } 
            });

            $("form").submit(function (){
                var n = $("input:checked").length;    
                if(n == 0) {
                    $("#perCheck").text('Please select at least one permission below').css('color','red');
                    return false;
                } else {
                    return true;
                }

            });
       });
   </script>