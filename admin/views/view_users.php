<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
<div class="wrapper main-wrapper row">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">View Users</h2>                                
            </header>
            <?php 
            if(in_array(2, $user_permissions) || in_array(1, $user_permissions)){
            ?>
            <a href="<?=base_url().'users/add_user'?>">Add User</a>
            <?php } ?>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Designation</th>
                                        <th>Permissions</th>
                                        <?php if(!empty($user_permissions)){ ?>
                                            <th>Actions</th> 
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($user_details as $users){
                                    if($users->deleted == 1){
                                        $styleapply = "opacity:0.4";
                                    } else {
                                        $styleapply = "";
                                    }
                                    ?>
                                    <tr style="<?=$styleapply?>">
                                        <td>
                                            <h6><?=$users->name?></h6>
                                        </td>
                                        <td><?=$users->email?></td>
                                        <td><?=$users->phone?></td>
                                        <td><?php 
                                        $design = designation($users->designation);
                                        /*if($users->designation == 1){
                                            echo 'Staff';
                                        } elseif($users->designation == 2){
                                            echo 'Health Co-ordinator';
                                        }*/
                                        echo $design;
                                        ?></td>
                                        <td><?php 
                                        $permissions = permssion_details($users->permission);
                                        echo $permissions;
                                        ?></td>
                                        <?php if(!empty($user_permissions)){ ?>
                                        <td>
                                            <?php 
                                            if($users->deleted == 0){
                                                if(in_array(3, $user_permissions) || in_array(1, $user_permissions)){
                                                ?>
                                            <a href="<?=base_url().'users/edit_user/'.$users->id ?>">Edit</a>
                                                <?php }  if(in_array(5, $user_permissions) || in_array(1, $user_permissions)){ ?>
                                            <a href="<?=base_url().'users/delete_user/'.$users->id ?>" onclick="return confirm('Are you sure you want to disable?')">Disable</a>
                                        <?php } } else if($users->deleted == 1){
                                                if(in_array(5, $user_permissions) || in_array(1, $user_permissions)){ ?>
                                                <a href="<?=base_url().'users/delete_user/'.$users->id.'/0' ?>" onclick="return confirm('Are you sure you want to enable?')">Enable</a>
                                        <?php } }?>
                                        </td>
                                    <?php } ?>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>    
</div>
</section>                    
<?php $this->load->view('templates/footer'); ?>
