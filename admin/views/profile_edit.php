<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box " style="width: 30%; margin-left: 30%;">
                        <header class="panel_header">
                            <h2 class="title pull-left"><b>Edit Your Profile</b></h2>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form id="profileEditForm" method="POST" enctype="multipart/form-data" >
										<div class="container">

                                            <div class="form-group">
                                                    
                                                <div class="controls"> <label class="form-label"><b>Full Name :</b></label>
                                                    <input type="text" placeholder="Full Name" class="form-control" style="width: 20%;" name="name" id="name" value="<?=$userdata->name?>"  required>
                                                </div>
                                                
                                                <div class="controls"> <label class="form-label"><b>Email :</b></label>
                                                    <input type="text" placeholder="Email" class="form-control" style="width: 20%;" name="email" id="email" value="<?=$userdata->email?>" disabled>
                                                </div>

                                                <div class="controls"> <label class="form-label"><b>Phone :</b></label>
                                                    <input type="text" placeholder="Phone" class="form-control" style="width: 20%;" name="phone" id="phone" value="<?=$userdata->phone?>"  required>
                                                </div>  

                                                <div class="controls"> <label class="form-label"><b>Profile Image :</b></label>
                                                    <input type="file" name="profileImage" id="profileImage" accept="image/png, image/jpeg">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
											<button type="submit" id="click_submit" class="btn btn-success">Submit</button>     
                                            <a href="<?=base_url('users/view_profile')?>" class="btn btn-primary">Cancel</a>
											
                                            <?php
                                                if($this->uri->segment(3) != ''){?>
												<a id="login_id_pwd" style="display:none;" href="<?=base_url('login')?>" class="btn btn-primary">Login</a>
											<?php } ?>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
<?php $this->load->view('templates/footer'); ?>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/additional-methods.js"></script>
<script>
$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
});

$("#profileEditForm").validate({
  rules: {
    // simple rule, converted to {required:true}
    name: "required",
    phone: "required",
    profileImage: {
      accept: "png|jpg|jpeg",
      filesize: 2000000
    }
  },
  messages: {
    name: "Please enter your full name",
    phone: "Please enter your phone",
    profileImage:{
        accept: "File must be of type png, jpg or jpeg",
        filesize: "File size must be less than 2 MB"
    }
  }

});

</script>