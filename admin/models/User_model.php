<?php 
class User_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getPermission($type){
		$where = array('user_id' => $this->session->userdata('admin_user_id'),'permission' => $type);
		$this->db->where($where);
		$res = $this->db->get('curd_permissions');
		return $res->row();
	}

}
?>