<?php 
class School_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getAllSchools(){
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->result();
	}

	public function getschool($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->row();
	}

	public function updateDel($id, $deleted = 1){
		$this->db->update('schools', array('deleted' => $deleted),array('id' => $id));
	}


	public function getAllStudents(){
		$this->db->select('stud.*,sc.id as school_id,sc.school_name');
		$this->db->where('stud.deleted',0);
		$this->db->join('schools sc','sc.id = stud.school_id');
		$res = $this->db->get('students stud');
		return $res->result();
	}

	public function getAllStudentsForDashboard(){
		$this->db->select('stud.*,sc.id as school_id,sc.school_name');
		$this->db->where('stud.deleted',0);
		$this->db->where('sc.deleted',0);
		$this->db->join('schools sc','sc.id = stud.school_id');
		$res = $this->db->get('students stud');
		return $res->result();
	}

	public function getStudentAllData($id){
        $this->db->select('s.*,p.*,sc.school_name,sc.address as "school_address",sc.city');
		$this->db->where('s.id',$id);
        $this->db->where('s.deleted',0);
        //$this->db->where('s.school_id',$school_id);
        $this->db->join('parents p','s.parent_id = p.id','left');
        $this->db->join('schools sc','sc.id = s.school_id');
		$res = $this->db->get('students s');
		return $res->row();
	}
	
	public function updateStudentDeleted($id){
		$this->db->update('students', array('deleted' => 1),array('id' => $id));
		//$this->db->update('parents', array('deleted' => 1),array('student_id' => $id));
	}

	function getParent($parentEmail){
		$this->db->where('email',$parentEmail);
		$this->db->where('deleted',0);
		$res = $this->db->get('parents');
		return $res->row();
	}

	public function get_count($table,$whrArr = array())
    {
        if(!empty($whrArr))
            $this->db->where($whrArr);
        $q = $this->db->get($table);
        return $q->num_rows();
        
    }

}
?>