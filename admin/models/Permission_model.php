<?php 
class Permission_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getAllPermissions(){
		$this->db->where('deleted',0);
		$res = $this->db->get('permissions');
		return $res->result();
	}

	public function getPermissions($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('permissions');
		return $res->row();
	}

	public function updateDel($id){
		$this->db->update('permissions', array('deleted' => 1),array('id' => $id));
	}



}
?>