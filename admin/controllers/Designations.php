<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designations extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('designation_model');
	}

	public function index(){
		$data = array();
		$designation_details = $this->db->query('select id,designation,deleted,modified_date from designations where type =1')->result();		
		$data['designation_details'] = $designation_details;		
		$this->load->view('designation/view_designation',$data);		
	}

	public function add_designation(){
		$data = array();
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 1');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 1');	
		$data['designations'] = $designations->result();
		$this->load->view('designation/addEdit_designation',$data);
	}

	public function insert_designation($desId = ''){	
		
		if(isset($_POST)){
			
			//echo '<pre>'; print_r($insertArr); die;	

			if($desId != 0){
				$updateArr = array(
					'designation'=>$_POST['designation_name'],
					'type'=>1,
					'modified_date'=>date('Y-m-d h:i:s'),
				);
				$this->db->update('designations', $updateArr,array('id' => $desId));
			}else{
				$insertArr = array(
					'designation'=>$_POST['designation_name'],
					'type'=>1,
					'created_date'=>date('Y-m-d h:i:s'),
				);
				$this->db->insert('designations', $insertArr);
			}

			$url = base_url().'designations';
			redirect($url);
		}
	}

	public function delete_designations($id, $deleted=1){
		$delid = $this->uri->segment('3');
		$delarr = array(
			'deleted'=>$deleted
		);
		$this->db->where('id', $delid);
		$this->db->update('designations', $delarr);
		$url = base_url().'designations';
		redirect($url);
	}



	public function designationUpdate($id = ''){ 
		$data = array();
		if($id != "")
			$data['designationData'] = $this->designation_model->getDesignations($id);
			$this->load->view('designation/addEdit_designation',$data);
	}



}
