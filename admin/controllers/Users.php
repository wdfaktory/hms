<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('school_model','user_model'));
		//var_dump($this->session->all_userdata());
		//die;
	}

	public function index(){
		$data = array();
		$user_details = $this->db->query('select u.id,u.name,u.email,u.phone,u.designation,u.permissions,u.type,u.deleted,up.id as upid,up.user_id,up.permission from users as u left join user_permissions as up on u.id = up.user_id where u.permissions = 1 and u.type = 2')->result();		
		$data['user_details'] = $user_details;
		$perAllowed = $this->user_model->getPermission(3);
		$data['user_permissions'] = ($perAllowed) ? explode(',', $perAllowed->curd): array();
		$this->load->view('view_users',$data);		
	}

	public function add_user(){
		$data = array();
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 1');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 1');	
		$data['designations'] = $designations->result();
		$this->load->view('add_user',$data);
	}

	public function edit_user(){
		$data = array();
		$editid = $this->uri->segment('3');
		$userdata = $this->db->query('select u.id,u.name,u.first_name,u.last_name,u.email,u.phone,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u left join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$editid)->row();
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 1');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 1');	
		$data['designations'] = $designations->result();
		$data['userdata'] = $userdata;	
		//pr($data);die;	
		$this->load->view('edit_user',$data);			
	}	

	public function update_user(){
		if(isset($_POST)){						
			$updateArr = array(
				'name' => $_POST['first_name'].' '.$_POST['last_name'],
				'first_name'=>$_POST['first_name'],
				'last_name'=>$_POST['last_name'],
				'email'=>$_POST['email'],
				'phone'=>$_POST['phone'],
				'designation'=>$_POST['designation'],
				'permissions'=>1,
				'type'=>$_POST['user_type']
			);
			$this->db->where('id', $_POST['user_id']);
			$this->db->update('users', $updateArr);
			/* $permissionarr = array(
				'permission'=>implode(',',$_POST['permission'])
			); */
			foreach($_POST['curd'] as $key => $permi){
				$usrPer .= "$key,";	
			}
			$permissionarr = array(
				'permission'=>rtrim($usrPer,','), //implode(',',$_POST['permission'])
			);

			$this->db->where('user_id', $_POST['user_id']);
			$this->db->update('user_permissions', $permissionarr);

			/* $this->db->where('user_id', $_POST['user_id']);
			$this->db->delete('curd_permissions'); */
			$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 1');	
			$prCurt = $permissions->result();
			$perId = $perId1 = array();
			foreach($prCurt as $pr){
				foreach($_POST['curd'] as $key => $permi){
					$curd_permsArr = array();
					if($pr->id == $key){
						$perId[] = $pr->id;
						$curd_permsArr = array(
							'user_id'=>$_POST['user_id'],
							'permission'=>$key,
							'curd'=>implode(',',$permi),
						);
						$checPer = curd_permission($_POST['user_id'],$key);
						if($checPer){
							$this->db->where('user_id', $_POST['user_id']);
							$this->db->where('permission', $key);
							$this->db->update('curd_permissions', $curd_permsArr);
						}
						else{
							$this->db->insert('curd_permissions', $curd_permsArr);
						}
						pr($curd_permsArr);
					}
					else{
						$perId1[] = $pr->id;
					}
				}
			}
			
			$perId = array_diff(array_unique($perId1),$perId);
			
			if(!empty($perId)){
				foreach($perId as $pId){
					$checPer = curd_permission($_POST['user_id'],$pId);
					$curd_permsArr = array(
						'user_id'=>$_POST['user_id'],
						'permission'=>$pId,
						'curd'=>'0',
					);
					$this->db->where('user_id', $_POST['user_id']);
					$this->db->where('permission', $pId);
					$this->db->update('curd_permissions', $curd_permsArr);				
				}
			}

			/* foreach($_POST['permission'] as $key => $permi){
				$curd_permsArr = array(
					'user_id'=>$_POST['user_id'],
					'permission'=>$permi,
					'curd'=>implode(',',$_POST['curd'][$permi])
				);				
				/*$this->db->where('user_id', $_POST['user_id']);
				$this->db->where('permission', $permi);
				$this->db->update('curd_permissions', $curd_permsArr);//*
				$this->db->insert('curd_permissions', $curd_permsArr);
			} */
			$url = base_url().'users';
			redirect($url);
		}		
	}

	public function delete_user($id,$deleted=1){
		$delid = $this->uri->segment('3');
		$delarr = array(
			'deleted'=>$deleted
		);
		$this->db->where('id', $delid);
		$this->db->update('users', $delarr);
		$url = base_url().'users';
		redirect($url);
	}

	public function insert_user(){
		$randpassword = $this->password_generate(7);			
		if(isset($_POST)){						
			$permission = isset($_POST['permission'])?$_POST['permission']:'';
			$insertArr = array(
				'name' => $_POST['first_name'].' '.$_POST['last_name'],
				'first_name'=>$_POST['first_name'],
				'last_name'=>$_POST['last_name'],
				'email'=>$_POST['email'],
				'password'=>md5($randpassword),
				'phone'=>$_POST['phone'],
				'designation'=>$_POST['designation'],
				'permissions'=>1,
				'type'=>$_POST['user_type']
			);
			//pr($_POST);die;
			$this->db->insert('users', $insertArr);
			$insert_id = $this->db->insert_id();			
			if($permission != ''){
				foreach($_POST['curd'] as $key => $permi){
					$usrPer .= "$key,";	
				}
				$permissionarr = array(
					'permission'=>rtrim($usrPer,','), //implode(',',$_POST['permission'])
					'user_id'=>$insert_id,
				);
	
				/* $permissionarr = array(
					'user_id'=>$insert_id,
					'permission'=>implode(',',$permission)
				); */
				$this->db->insert('user_permissions', $permissionarr);
			}
			
			if(!empty($permission) && isset($_POST['curd'])){
				foreach($_POST['curd'] as $key => $permi){
					$curd_permsArr = array(
						'user_id'=>$insert_id,
						'permission'=>$key,
						'curd'=>implode(',',$permi)
					);
					$this->db->insert('curd_permissions', $curd_permsArr);
				}
			}

			/** Sunil **/
			$template_val =' <p>Hi <b>'.ucfirst($_POST['name']).',</p>
                        <p>We have created the user login for HMS Portal and school has been added Kindly check the below credentials for your login.</p><br>
						<span><b>Email</b> : '.$_POST['email'].'</span><br>
						<span><b>Password</b> : '.$randpassword.'</span><br>
						<span><b>Link</b> : <a href="<?=base_url('."admin/login/".')?>" target="_blank">Click to Login</a></span><br>
						<br>
						<br>
                        <p>Good luck! Hope it works.</p>';
			
			$from = 'shabeer77777@gmail.com';
			$to = 	$_POST['email'];
			$subject = 'User ID Created';
			send_email($from, $to, $subject,$template_val,'html');		

			$url = base_url().'users';
			redirect($url);
		}
	}

	public function email_test(){
		send_email('shabeerksbr@hotmail.com', 'shabeer77777@gmail.com','Some subject','Message Goes here','plain');
	}


	public function schools(){
		$this->load->model('school_model');

		$limit = 5;
		$start = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

		$this->db->select('schools.*, users.phone');

		if(isset($_GET['search_query'])){
			$this->db->where('school_name like','%'.$_GET['search_query'].'%');
		}

		$this->db->join('users','users.id=schools.user_id');

		$this->db->limit($limit, $start);

		$schDet = $this->db->get('schools')->result();
		if(isset($_GET['search_query']) && !empty($this->input->get('search_query'))){
			$this->db->where('schools.school_name like','%'.$_GET['search_query'].'%');
			$this->db->join('users','users.id=schools.user_id');
			$this->db->from('schools');
			$total_rows = $this->db->count_all_results();
		}
		else{
			$total_rows = $this->db->count_all('schools');
		}
		

		$this->load->library('pagination');
		$config['base_url'] = base_url().'school';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $limit;
		$config['reuse_query_string'] = TRUE;
		// $config['uri_segment'] = 2;
		// $config['enable_query_strings'] = TRUE;
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();

		// $schDet = $this->school_model->getAllSchools();				
		$data['school_det'] = $schDet;
		$perAllowed = $this->user_model->getPermission(1);
		//var_dump($perAllowed);die;
		$data['school_permissions'] = ($perAllowed) ? explode(',', $perAllowed->curd): array();
		//die(print_r($schDet));
		//$this->load->view('school');
		$this->load->view('view_schools',$data);
	}

	public function schoolUpdate($id = ''){
		$data = array();
		if($id != ""){
			$data['schoolData'] = $this->school_model->getschool($id);
			$userid = $data['schoolData']->user_id; 
			$data['userData'] = $this->db->query('select * from users where id = '.$userid)->row();			
			$this->load->view('school',$data);
		} else {
			$this->load->view('school',$data);
		}
	}

	public function add_school($scId = ''){
		$randpassword = $this->password_generate(7);
		if(isset($_POST)){
			if($scId != 0){
				$insertSchoArr = array(
					'name'=>$_POST['name'],
					'email'=>$_POST['email'],
					'phone'=>$_POST['phone'],
					'permissions'=>2,
					'type'=>$_POST['user_type']
				);
				$getuserid = $this->db->query('select * from schools where id ='.$scId)->row()->user_id;
				$this->db->update('users', $insertSchoArr,array('id' => $getuserid));
			} else {
				$insertSchoArr = array(
					'name'=>$_POST['name'],
					'email'=>$_POST['email'],
					'password'=>md5($randpassword),
					'phone'=>$_POST['phone'],
					'permissions'=>2,
					'type'=>$_POST['user_type']
				);				
				$this->db->insert('users', $insertSchoArr);
				$getuserid = $this->db->insert_id();

				$getschoolper = $this->db->query('select * from permissions where type = 2 and deleted = 0')->result();
				$impper = array();
				foreach($getschoolper as $scper){
					$impper[] = $scper->id;
				}
				$schooladminpermission = array(
					'user_id'=>$getuserid,
					'permission'=>implode(',',$impper)
				);
				$this->db->insert('user_permissions', $schooladminpermission);

				foreach($getschoolper as $curper){
					$curdper = array(
						'user_id'=>$getuserid,
						'permission'=>$curper->id,
						'curd'=>'1,2,3,4,5'
					);
					$this->db->insert('curd_permissions', $curdper);
				}
			}

			$insertArr = array(
				'school_name'=>$_POST['schoolname'],
				'user_id' => $getuserid,				
				'city'=>$_POST['city'],
				'address'=>	$_POST['address'],
				'zone'=>$_POST['zone'],
				'sponsor_name'=> $_POST['sponsor_name'],
				'cont_number'=>	$_POST['cont_number'],
				'sponsor_email'=> $_POST['sponsor_email'],
				'sponsor_design'=> $_POST['sponsor_design']
			);
			if($scId != 0){
				$this->db->update('schools', $insertArr,array('id' => $scId));
			}
			else{
				$this->db->insert('schools', $insertArr);
				$getschoolid = $this->db->insert_id();
				$updateuserArr = array(
					'school_id' => $getschoolid
				);
				$this->db->update('users', $updateuserArr,array('id' => $getuserid));
				//add student permission
				$studAddPer = array(
					'school_id' => $getschoolid,
					'user_id'=>$getuserid,
					'standard'=>'1,2,3',
				);
				$this->db->insert('health_standard', $studAddPer);
				//health_standard
			}

			$url = base_url().'users/schools';
			redirect($url);
		}
	}

	function password_generate($chars){
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}

	function delete_school($id, $deleted=1){
		if($id){
			$this->school_model->updateDel($id,$deleted);
		}
		// $this->schools();
		$url = base_url().'school';
		redirect($url);
	}
	
	
	
	/** sunil start**/
	
	public function view_profile(){
		$data = array();

		$this->load->library('session');
		$user_id =$this->session->admin_user_id;
		
		$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.profile_image,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$user_id)->row();
		//echo '<pre>'; print_r($userdata); die;
		
		$data['userdata'] = $userdata;
		
		$this->load->view('profile_view',$data);		
	}

	public function edit_profile()
	{
		$data = array();

		$this->load->library('session');
		$user_id = $this->session->admin_user_id;

		$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.profile_image,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$user_id)->row();

		if($this->input->method() == 'post'){
			$this->load->library('image_lib');
			// echo '<pre>'; print_r($this->input->post()); die;
			// pr(FCPATH);die;
			$old_image_name = $userdata->profile_image;
			$update_arr = array(
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone')
			);
			$config['upload_path'] = FCPATH.'/assets/uploads/';
			$config['max_size'] = 2048;
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			// if ( ! $this->upload->do_upload('profileImage'))
			// {
			// 	$error = array('error' => $this->upload->display_errors());
			// 	echo '<pre>'; print_r($error); die;
			// }
			if($this->upload->do_upload('profileImage'))
			{
				$upload_data = $this->upload->data();
				$update_arr['profile_image'] = $upload_data['file_name'];
				// echo '<pre>'; print_r($upload_data); die;
				$configure =  array(
					'image_library'   => 'gd2',
					'source_image'    =>  $upload_data['full_path'],
					'maintain_ratio'  =>  FALSE,
					'width'           =>  100,
					'height'          =>  100,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configure);
				$this->image_lib->resize();
				if(!empty($old_image_name)){
					unlink($config['upload_path'].$old_image_name);
				}
			}
			
			$this->db->update('users', $update_arr, array('id' => $user_id));
			$url = base_url().'users/view_profile';
			redirect($url);
		}
		// echo '<pre>'; print_r($userdata); die;
		
		$data['userdata'] = $userdata;
		
		$this->load->view('profile_edit',$data);
	}
	
	public function open_reset_pwd(){
		$this->load->view('reset_pwd');		
	}
	
	public function reset_pwd(){
		
		$chk_email = $this->db->query('select * from users where email = "'.$_POST['email'].'"')->row();
		//echo '<pre>'; print_r($chk_email); die;
		if(!empty($chk_email)){
			$template_val =' <p>Hi <b>'.ucfirst($chk_email->name).',</p>
                        <p>Kindly click the below link to reset the password of your account.</p><br>
						
						<span><b>Link</b> : <a href="'. base_url('users/change_pwd/'.$chk_email->id) .'" target="_blank">Click to Login</a></span><br>
						<br>
						<br>
                        <p>Good luck! Hope it works.</p>';
			
			$from = 'shabeer77777@gmail.com';
			//$to = 	$_POST['email'];
			$to = 	'sun123@yopmail.com';
			$subject = 'Forgot Password';
			send_email($from, $to, $subject,$template_val,'html');		
			echo json_encode( array('success' => 1) );
		}else{
			echo json_encode( array('success' => 0) );
			
		}
	}
	
	public function change_pwd(){
		
		$this->load->library('session');
		$user_id =$this->uri->segment(3);
	
		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
	
		if($chk_email != NULL){
			$this->load->view('change_pwd');		
		} 
	}
	
	
	public function update_pwd(){
		$user_id =$this->session->admin_user_id;
		$insertArr = array(
			'password'=>md5($_POST['password'])
		);
		
		$this->db->update('users', $insertArr,array('id' => $user_id));
		//echo json_encode( array('success' => 1) );
		$this->session->set_flashdata('success', 'Password changed successfully');
		redirect(base_url('users/view_profile'));
	}
	
	
	public function user_change_pwd(){
		
		$data = array();
	
		$this->load->library('session');
		$user_id =$this->session->admin_user_id;

		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
	
		if($chk_email != NULL){
			$data['user_id'] = $user_id;
			$this->load->view('change_pwd', $data);		
		} 
	}

	
	
	
	/** sunil end**/

}
