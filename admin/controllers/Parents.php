<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('parents');
	}	

	public function students(){
		$data = array();
		$getSchools = $this->db->query('select * from schools where deleted = 0');
		$data['schools'] = $getSchools->result();		
		$this->load->view('students',$data);

	}

	public function add_students(){	
		$randpassword = $this->password_generate(7);	
		if(isset($_POST)){
			$insertArr = array(
				'school_id'=>$_POST['school'],
				'student_name'=>$_POST['studentname'],
				'standard'=>$_POST['standard'],
				'section'=>$_POST['section'],
				'age'=>$_POST['age'],
				'sex'=>$_POST['sex'],
				'blood_group'=>$_POST['bgroup']
			);
			$this->db->insert('students', $insertArr);
			$insert_id = $this->db->insert_id();

			$parentArr = array(
				'student_id'=>$insert_id,
				'father_name'=>$_POST['fname'],
				'mother_name'=>$_POST['mname'],
				'occupation'=>$_POST['foccupation'],
				'phone_number'=>$_POST['phoneno'],
				'mobile_number'=>$_POST['mobileno'],
				'email'=>$_POST['email'],
				'password'=>md5($randpassword),
				'address'=>$_POST['address']
			);
			$this->db->insert('parents', $parentArr);

			$url = base_url().'parents/students';
			redirect($url);
		}
	}



	function password_generate($chars){
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}


	public function parent_details()
	{
		$data = array();
		$this->db->select('parents.*, students.id as icid, 
		students.school_id as school_id, students.roll_no as student_id');
		// $this->db->where('parents.deleted',0);
		$this->db->from('parents');
		$this->db->join('students', 'parents.id = students.parent_id');
		$data['parents'] = $this->db->get()->result();
		// echo pr($data['parents']).die;
		$this->load->view('view_parents', $data);
	}

	public function disable_parent($id, $deleted=1){
		// $delid = $this->uri->segment('3');
		$delarr = array(
			'deleted'=>$deleted
		);
		$this->db->where('id', $id);
		$this->db->update('parents', $delarr);
		$url = base_url().'parent-details';
		redirect($url);
	}
}
