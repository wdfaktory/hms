<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $userpermission_table = 'user_permissions';
	private $permission_table = 'permissions';

	public function __construct(){
		parent::__construct();
		if(!$this->session->admin_user_id){
			redirect(base_url('login'));
		}
	}

	public function index()
	{
		/*$this->load->library('session');
		$this->load->model('login_model');
		$this->load->model('user_model');
		$data = array();
		$userid = $this->session->userdata('admin_user_id');
		$whrprArr = array('user_id'=>$userid);
		$chkpermission = $this->user_model->get_row($this->userpermission_table, $whrprArr);
		$explodepermission = explode(',',$chkpermission->permission);
		$data['userpermissions'] = $explodepermission;
		
		$perdata = $this->db->query('select * from permissions where deleted = 0')->result();
		$data['permissions'] = $perdata;*/
		$this->load->model('school_model');
		$data['studCount'] = $this->school_model->get_count('students',array('deleted'=>0));
		$data['schoolCount'] = $this->school_model->get_count('schools',array('deleted'=>0));
		$data['parentCount'] = $this->school_model->get_count('parents',array('deleted'=>0));

		$data['healthyStdCount'] = $this->school_model->get_count('students', 
		array('deleted'=>0,'health_score >=' => 75,'health_score <=' => 100));

		$data['sickStdCount'] = $this->school_model->get_count('students', 
		array('deleted'=>0,'health_score >=' => 50,'health_score <=' => 74));

		$data['badHealthStdCount'] = $this->school_model->get_count('students', 
		array('deleted'=>0,'health_score >=' => 0,'health_score <=' => 49));

		$students = $this->school_model->getAllStudentsForDashboard();
		$stud = [];
		$data['healthySchoolCount'] = 0;
		$data['sickSchoolCount'] = 0;
		$data['badSchoolCount'] = 0;
		foreach ($students as $key => $student) {
			$stud[$student->school_id][] = $student->health_score;
		}
		foreach ($stud as $key => $healthScores) {
			$sum = 0;
			$total = count($healthScores);
			foreach ($healthScores as $key => $value) {
				$sum += $value;
			}
			$avg = $sum/$total;
			if($avg>=75 && $avg<=100){
				$data['healthySchoolCount'] += 1;
			}
			elseif ($avg>=50 && $avg<=74) {
				$data['sickSchoolCount'] += 1;
			}
			else{
				$data['badSchoolCount'] += 1;
			}
		}
		
		$this->load->view('dashboard',$data);
	}

	public function wizard_demo()
	{
		if($this->input->method() == 'post'){
			pr($this->input->post());die;
		}
		$this->load->view('wizard_demo');
	}
}
