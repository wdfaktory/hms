<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	private $parents_table = 'users';
	private $permission_table = 'user_permissions';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function check_login(){
		$this->load->library('session');
		$this->load->model('login_model');
		$user_name = $this->input->post('name');
		$password = $this->input->post('password');
		$md5_pass = md5($password);
		$wherArr = array('email' => $user_name, 'password' => $md5_pass, 'permissions' => 1);
		$chk = $this->login_model->get_row($this->parents_table, $wherArr);		
		if($chk != NULL){
			
			if($this->input->post('remember') == 'on'){
				$remember ='1';
			}else{
				$remember ='0';
			}
			//echo '<pre>'; print_r($remember); die;
			$sessArr = array('admin_user_name' => $user_name, 'admin_user_id' => $chk->id,'user_type'=>$chk->type,'remember'=>$remember,'password'=>$password ,'email_sess' =>$user_name);
			$this->session->set_userdata($sessArr);	

			
			redirect(base_url('dashboard'));			
		} else {
			$this->session->set_flashdata('is_invalid',1);
			redirect(base_url('login'));
		}
	}

	public function logout(){
		$this->load->library('session');
		$this->session->unset_userdata('admin_user_id');
		$this->session->unset_userdata('admin_user_name');
		
		redirect(base_url('login'));
	}
}
