<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('permission_model');
	}

	public function index(){
		$data = array();
		$permission_details = $this->db->query('select id,name,modified_date from permissions where deleted =0 and type=1')->result();		
		$data['permission_details'] = $permission_details;		
		$this->load->view('permission/view_permission',$data);		
	}

	public function add_permission(){
		$data = array();
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 1');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 1');	
		$data['designations'] = $designations->result();
		$this->load->view('permission/addEdit_permission',$data);
	}

	public function insert_permission($prId = ''){	
		
		if(isset($_POST)){
			
			//echo '<pre>'; print_r($insertArr); die;	

			if($prId != 0){
				$updateArr = array(
					'name'=>$_POST['permission_name'],
					'type'=>1,
					'modified_date'=>date('Y-m-d h:i:s'),
				);
				$this->db->update('permissions', $updateArr,array('id' => $prId));
			}else{
				$insertArr = array(
					'name'=>$_POST['permission_name'],
					'type'=>1,
					'created_date'=>date('Y-m-d h:i:s'),
				);
				$this->db->insert('permissions', $insertArr);
			}

			$url = base_url().'permissions';
			redirect($url);
		}
	}

	public function delete_permission(){
		$delid = $this->uri->segment('3');
		$delarr = array(
			'deleted'=>1
		);
		$this->db->where('id', $delid);
		$this->db->update('permissions', $delarr);
		$url = base_url().'permissions';
		redirect($url);
	}



	public function schools(){
		$this->load->model('school_model');
		$schDet = $this->school_model->getAllSchools();
		$data['school_det'] = $schDet;
		//echo '<pre>'; die(print_r($schDet));
		//$this->load->view('school');
		$this->load->view('view_schools',$data);
	}

	public function permissionUpdate($id = ''){ 
		$data = array();
		if($id != "")
			$data['permissionData'] = $this->permission_model->getPermissions($id);
			$this->load->view('permission/addEdit_permission',$data);
	}

	public function add_school($scId = ''){
		$randpassword = $this->password_generate(7);
		if(isset($_POST)){
			$insertArr = array(
				'school_name'=>$_POST['schoolname'],
				'email'=>$_POST['email'],
				'password'=>md5($randpassword),
				'city'=>$_POST['city'],
				'address'=>	$_POST['address']
			);
			if($scId != 0)
				$this->db->update('schools', $insertArr,array('id' => $scId));
			else
				$this->db->insert('schools', $insertArr);

			$url = base_url().'users/schools';
			redirect($url);
		}
	}

	function password_generate($chars){
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}

	function delete_school($id){
		if($id){
			$this->school_model->updateDel($id);
		}
		$this->schools();
	}


}
