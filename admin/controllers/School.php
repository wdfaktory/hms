<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('school_model','user_model'));
		if(!$this->session->admin_user_id){
			redirect(base_url('login'));
		}
		//var_dump($this->session->all_userdata());
		//die;
    }
    
    public function viewStudents(){
		$data = array();
        $data['students'] = $this->school_model->getAllStudents();
        $data['schools'] = $this->school_model->getAllSchools();
        //pr($data);die;
		$this->load->view('admin_student_view',$data);
	}


	function studentUploadTemplate(){
		$download_data['header'] = array('School Id','Roll No','Student Name','Standard','Section','Date of Birth (YYYY-MM-DD)','Age','Sex','Blood Group','Father Name','Mother Name','Occupation','Mobile Number','Phone Number','Email','Address',
		'Guardian Name','Pube (Y/N)','Subscription Package','Special Care Need (Y/N)','Sports Plan (Y/N)','Under Vaccination (Y/N)','Alergitic (Y/N)','Asthma (Y/N)','Insurance Id','Insurance Name','Category');
		$path = str_replace('\\', '/', BASEPATH).'../third_party/Excel.php';
		require $path;
		$excel = Excel::downloadStudetTemp($download_data,'admin');
		//$this->load->library('excel');
        //$this->excel->downloadStudetTemp($download_data);
			exit;
    }
    
    function saveUploadedStud(){
		ini_set('max_execution_time', 300);
		set_time_limit(300);
		if(isset($_FILES['stdFileUpload']['name']) && $_FILES['stdFileUpload']['name'] != ''){

			$path = str_replace('\\', '/', BASEPATH).'../third_party/Excel.php';
			require $path;
			$fileName = $_FILES['stdFileUpload']['name'];
			$config['upload_path'] = FCPATH . 'report_files/';                                
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['overwrite']     = true;
			//$config['max_size'] = 10000;
			//echo $fileName;
			$this->load->library('upload');
			$this->upload->initialize($config);
			$isUploaded = $this->upload->do_upload('stdFileUpload');
			$uploadedData = $this->upload->data();
			$inputFileName = $uploadedData['full_path'];
	
			//$this->load->library('excel');
			$file_path = $inputFileName;//FCPATH . 'report_files/' . $fileName;
			
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
	
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			//echo '<pre>';print_r($cell_collection);die;
	
			//extract to a PHP readable array format
			foreach ($cell_collection as $cell) {
				$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();//getValue();
	
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				} else {
					if($data_value){
						$arr_data[$row][$column] = $data_value;
					}
				}
			}
			$data['header'] = $header;
			$data['values'] = $arr_data;
			$totalordercount=count($data['values']);
			$totalordersuccess=0;
			$leftout = array();
			$i = 1;
			foreach ($data['values'] as $key => $value) {
                $schoolId = $value['A'];
                $roll_no = $value['B'];
                $studentName = $value['C'];
				$fatherName = $value['J'];
				$parentEmail = (isset($value['O'])) ? $value['O'] : '';
				if(isset($value['F']))
					$dob = date('Y-m-d',strtotime($value['F']));
				else
					$dob = '';

				$ckRoll = $this->checkRollNo($roll_no);
				if($studentName == "" || $ckRoll > 0 || $schoolId == ''){
					continue;
				}

				$parentArr = array(
					'father_name' => (isset($value['J'])) ? $value['J'] : '',
					'mother_name' => (isset($value['K'])) ? $value['K'] : '',
					'occupation' => (isset($value['L'])) ? $value['L'] : '',
					'mobile_number' => (isset($value['M'])) ? $value['M'] : '',
					'phone_number' => (isset($value['N'])) ? $value['N'] : '',
					'email' => (isset($value['O'])) ? $value['O'] : '',
					'address' => (isset($value['P'])) ? $value['P'] : '',
					'guardian_name' => (isset($value['Q'])) ? $value['Q'] : '',
					'password'=>md5($this->password_generate(7)),
				);

				$parentId = $this->getParentDetails($parentEmail,$parentArr);

				$stdData = array(
					'parent_id' => $parentId,
					'school_id' => ($value['A']) ? $value['A'] : '',
					'roll_no' => ($value['B']) ? $value['B'] : '',
					'student_name' => ($value['C']) ? $value['C'] : '',
					'category' => ($value['AA']) ? $value['AA'] : '',
					'standard' => ($value['D']) ? strtoupper($value['D']) : '',
					'section' => ($value['E']) ? $value['E'] : '',
					'dob' => ($value['F']) ? $dob : '',
					'age' => ($value['G']) ? $value['G'] : '',
					'sex' => (strtolower($value['H']) == 'male') ? 1 : 2,
					'blood_group' => ($value['I']) ? $value['I'] : '',
					'pube' => (strtolower($value['R']) == 'y') ? 1 : 0,
					'subscription_package' => ($value['S']) ? $value['S'] : '',
					'special_care_need' => (strtolower($value['T']) == 'y') ? 1 : 0,
					'sports_plan' => (strtolower($value['U']) == 'y') ? 1 : 0,
					'under_vaccination' => (strtolower($value['V']) == 'y') ? 1 : 0,
					'alergitic' => (strtolower($value['W']) == 'y') ? 1 : 0,
					'asthma' => (strtolower($value['X']) == 'y') ? 1 : 0,
					'insurance_id' => ($value['Y']) ? $value['Y'] : '',
					'insurance_name' => ($value['Z']) ? $value['Z'] : '',
				);
				$userName = substr($studentName,0,4).substr($fatherName,0,4).date('dm', strtotime($dob));
				$userName = strtolower($userName);
				$userCheck = $this->checkStudUserName($userName);
				if($userCheck === 0)
					$stdData['user_name'] = $userName;
				else{
					$time = strval(time());
					$stdData['user_name'] = $userName.substr($time,-4);
				}

				$this->db->insert('students', $stdData);
				$insert_id = $this->db->insert_id();
				
				// $this->db->insert('parents', $parentArr);
				$stdData = array();
				$parentArr = array();
			}
	
			$this->session->set_flashdata('status', 'Uploaded Studend data successfully');
		}
		$url = base_url('students');
		redirect($url);
    }
    
    public function student_dashboard($studId){
		if($studId != ''){
			$data['studData'] = $this->school_model->getStudentAllData($studId);
			// pr($data['studData']);die;
			$data['studImgUrl'] = '';
			//get auth url from s3
			if($data['studData']->school_id != '' && $data['studData']->image_name != ''){
				$bucketName = $this->config->item('studImgBucket');
				$uploadName = $data['studData']->school_id."/".$data['studData']->image_name;
				$path = str_replace('\\', '/', BASEPATH).'../third_party/S3.php';
				require $path;
				$accessKey = $this->config->item('accessKey');
				$secretKey = $this->config->item('secretKey');
				$response = S3::getAuthenticatedURL($bucketName, $uploadName,300);
				$data['studImgUrl'] = $response;
			}
			$this->load->view('admin_student_dashboard',$data);
		}
		else{
			$url = base_url('students');
			redirect($url);
		}
	}



	public function studentUpdate($studId = ''){
		$this->load->view('admin_student_view');

		/* if((isset($_POST)) && isset($_FILES)){
			
		}else{
			$this->load->view('admin_student_view');
		} */
		
    }


    public function checkStudUserName($userName){
		$checkUser = $this->db->get_where('students', array("user_name" => $userName))->num_rows();
		return $checkUser;
	}

	public function checkRollNo($roll_no){
		$checkRoll = $this->db->get_where('students', array("roll_no" => $roll_no,'school_id' => $this->schoolId))->num_rows();
		return $checkRoll;

    }

    public function students_data($studId = ''){
		$data = array();
        $data['schools'] = $this->school_model->getAllSchools();
		$data['studImgUrl'] = '';
			if($studId != ''){
				$data['studData'] = $this->school_model->getStudentAllData($studId);
				//get auth url from s3
				if($data['studData']->school_id != '' && $data['studData']->image_name != ''){
					$bucketName = $this->config->item('studImgBucket');
					$uploadName = $data['studData']->school_id."/".$data['studData']->image_name;
					$path = str_replace('\\', '/', BASEPATH).'../third_party/S3.php';
					require $path;
					$accessKey = $this->config->item('accessKey');
					$secretKey = $this->config->item('secretKey');
					$response = S3::getAuthenticatedURL($bucketName, $uploadName,300);
					$data['studImgUrl'] = $response;
				}
			}
			else{
				$data['studData'] = array();
			}
	
			$this->load->view('admin_student_update',$data);
		
	}

	public function add_students($studId = ''){
		ini_set('max_execution_time', 300);
		set_time_limit(300);
		//$randpassword = $this->password_generate(7);	
		$update = false;
		//$ckRoll = false;
		$fileName = '';
		$insertArr = array();
		if(isset($_POST)){
            $school_id = $_POST['school_id'];
            $studentName = $_POST['studentname'];
			$fatherName = $_POST['fname'];
			$parentEmail = $_POST['email'];
			$dob = date('Y-m-d', strtotime($_POST['dob']));
			$roll_no = $_POST['roll_no'];
			/* if($studId == '')
				$ckRoll = $this->checkRollNo($roll_no); */
			//if(!$ckRoll){
				
				$randpassword = $this->password_generate(7);
				$parentArr = array(
					'father_name'=>$fatherName,
					'mother_name'=>$_POST['mname'],
					'occupation'=>$_POST['foccupation'],
					'phone_number'=>$_POST['phoneno'],
					'mobile_number'=>$_POST['mobileno'],
					'email'=>$_POST['email'],
					//'password'=>md5($randpassword),
					'address'=>$_POST['address'],
					'guardian_name'=>$_POST['gname']
				);

				$parentId = $this->getParentDetails($parentEmail,$parentArr);

				$insertArr = array(
					'school_id'=>$school_id,
					'parent_id' => $parentId,
					'roll_no'=>$roll_no,
					'student_name'=>$studentName,
					'category' => $_POST['category'],
					'standard'=>$_POST['standard'],
					'section'=>$_POST['section'],
					'dob'=> $dob,
					'age'=>$_POST['age'],
					'sex'=>$_POST['sex'],
					'blood_group'=>$_POST['bgroup'],
					'pube'=>$_POST['pube'],
					'subscription_package'=>$_POST['spackage'],
					'special_care_need'=>$_POST['scneed'],
					'sports_plan'=>$_POST['splan'],
					'under_vaccination'=>$_POST['uvaccination'],
					'alergitic'=>$_POST['alergitic'],
					'asthma'=>$_POST['asthma'],
					'insurance_id'=>$_POST['insuranceid'],
					'insurance_name'=>$_POST['insurancename'],
				);
	
				$fileUp = false;
				if($_FILES['studImg']['name'] != ''){
					$fileName = $_FILES['studImg']['name'];
					$fileName = time().'_'.$fileName;
					$config['upload_path'] = FCPATH . 'student_records/';                                
					$config['file_name'] = $fileName;
					$config['allowed_types'] = 'png|jpeg|jpg';
	
					$this->load->library('upload');
					$this->upload->initialize($config);
					$isUploaded = $this->upload->do_upload('studImg');
					$uploadedData = $this->upload->data();
					$filePath = $uploadedData['full_path'];
					$fileUp = true;
					
					//upload to s3
					$bucketName = $this->config->item('studImgBucket');
					$uploadName = "$school_id/$fileName";
					$path = str_replace('\\', '/', BASEPATH).'../third_party/S3.php';
					require $path;
					$accessKey = $this->config->item('accessKey');
					$secretKey = $this->config->item('secretKey');
					$response = S3::putObject(S3::inputFile($filePath, false), $bucketName, $uploadName);
					$insertArr['image_name'] = $fileName;
				}
				
				if($studId)
				{
					$this->db->update('students', $insertArr,array('id'=>$studId));
					$insert_id = $studId;
					$update = true;
				}
				else{
					$userName = substr($studentName,0,4).substr($fatherName,0,4).date('dm', strtotime($dob));
					$userName = strtolower($userName);
					$userCheck = $this->checkStudUserName($userName);
					if($userCheck === 0)
						$insertArr['user_name'] = $userName;
					else{
						$time = strval(time());
						$insertArr['user_name'] = $userName.substr($time,-4);
					}
	
					$this->db->insert('students', $insertArr);
					$insert_id = $this->db->insert_id();
				}
			//}
		}
		else{
			$this->session->set_flashdata('error', 'This roll no already exists');
		}

		$url = base_url('students');
			redirect($url);
	}

	function password_generate($chars){
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}

	function delete_student($id){
		if($id){
			$this->school_model->updateStudentDeleted($id);
		}
		$url = base_url('students');
			redirect($url);
	}

	function getParentDetails($parentEmail,$parentArr){
		if($parentEmail == '')
			$patentData = array();
		else
			$patentData = $this->school_model->getParent($parentEmail);

		if(!empty($patentData)){
			$this->db->update('parents', $parentArr,array('email'=>$parentEmail));
			return $patentData->id;
		}
		else{
			$parentArr['password'] = md5($this->password_generate(7));
			$this->db->insert('parents', $parentArr);
			$parent_id = $this->db->insert_id();
			return $parent_id;
		}

	}
    
    public function get_count($table,$whrArr = array())
    {
        if(!empty($whrArr))
            $this->db->where($whrArr);
        $q = $this->db->get($table);
        return $q->num_rows();
        
    }



}
