<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sidebar_details(){    
    $CI =& get_instance();
    $CI->load->library('session');
    $data = array();
    $userid = $CI->session->userdata('admin_user_id'); 
    $chkpermission = $CI->db->query('select * from user_permissions where user_id = '.$userid)->row();
	//echo "<pre>";print_r($chkpermission);die;  
    $explodepermission = explode(',',$chkpermission->permission);
    $data['userpermissions'] = $explodepermission;    
    $perdata = $CI->db->query('select * from permissions where deleted = 0 and type = 1')->result();
    $data['permissions'] = $perdata;
    return $data; 
} 

function permssion_details($userpermsion = ''){
    $CI =& get_instance();
    if($userpermsion){
        $permis_details = $CI->db->query('select * from permissions where deleted = 0 and id IN('.$userpermsion.')')->result();
        $permissionstr = '';
        $permission = array();
        foreach($permis_details as $details){               
            $permission[] = $details->name;
        }
        $permissionstr = implode(',',$permission);  
        return $permissionstr;
    } return 'N/A';
}

function curd_permission($userid,$permission){
    $CI =& get_instance();
    $curd_details = $CI->db->query('select * from curd_permissions where user_id = '.$userid.' and permission = '.$permission)->row();
    return $curd_details;
}

function curd_access(){
    $CI =& get_instance();
    $CI->load->library('session');
    $userid = $CI->session->userdata('admin_user_id'); 
    $curd_details = $CI->db->query('select * from curd_permissions where user_id = '.$userid)->result();
    return $curd_details;

}

function school_email($userid){
     $CI =& get_instance();
     $user_details = $CI->db->query('select * from users where id = '.$userid)->row();
     return $user_details->email;
}

function send_email($from,$to,$subject,$message,$type = 'plain'){
    $path = str_replace('\\', '/', BASEPATH).'../third_party/sendgrid/sendgrid-php.php';
    include $path;
    $email = new \SendGrid\Mail\Mail();
    $from_name = 'infocaresolutions';
    $email->setFrom($from, $from_name);
    $email->setSubject($subject);
    $email->addTo($to);
    if($type == 'plain')
        $email->addContent("text/plain", $message);
    else
        $email->addContent("text/html", $message);

    $sendgrid = new \SendGrid('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');
    try {
        return $response = $sendgrid->send($email);
        /*print $response->statusCode() . "\n";
        print_r($response->headers());
        print $response->body() . "\n";*/
    } catch (Exception $e) {
        return false;
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}

function designation($id){    
     $CI =& get_instance();
     $designation = $CI->db->query('select * from designations where id = '.$id)->row();
     if(!empty($designation)){
        return $designation->designation;
     } else{
        return '-';
     }
}

function pr($data){
    echo "<pre>";
    $d = print_r($data)."</pre>";
    return $d;
}

function user_profile_image($userid){
    $CI =& get_instance();
    $user_details = $CI->db->query('select * from users where id = '.$userid)->row();
    return $user_details->profile_image;
}

?>