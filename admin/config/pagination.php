<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination pull-right">';
$config['full_tag_close'] = '</ul></nav>';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['next_link'] = '<span aria-hidden="true">&raquo;</span>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '<span class="sr-only"></span></a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';