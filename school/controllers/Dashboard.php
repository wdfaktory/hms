<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $userpermission_table = 'user_permissions';
	private $permission_table = 'permissions';

	public function __construct(){
		parent::__construct();
		if(!$this->session->school_user_id){
			redirect(base_url('login'));
		}
	}

	public function index()
	{
		$this->load->model('school_model');
		$school_id = $this->session->school_id;
		$data['studCount'] = $this->school_model->get_count('students',
		array('school_id'=>$school_id,'deleted'=>0));
		$data['schoolCount'] = $this->school_model->get_count('schools');

		// $this->db->select('students.*','parents.email');
		$this->db->where('school_id',$school_id);
		$this->db->where('parents.deleted',0);
		$this->db->where('students.deleted',0);
		$this->db->join('students','parents.id=students.parent_id');
		$data['parentCount'] = $this->db->get('parents')->num_rows();

		// $data['parentCount'] = $this->school_model->get_count('parents');
		$data['splCareCount'] = $this->school_model->get_count('students',array('special_care_need' => 1));
		
		$data['healthyStdCount'] = $this->school_model->get_count('students', 
		array('school_id'=>$school_id,'deleted'=>0,'health_score >=' => 75,'health_score <=' => 100));

		$data['sickStdCount'] = $this->school_model->get_count('students', 
		array('school_id'=>$school_id,'deleted'=>0,'health_score >=' => 50,'health_score <=' => 74));

		$data['badHealthStdCount'] = $this->school_model->get_count('students', 
		array('school_id'=>$school_id,'deleted'=>0,'health_score >=' => 0,'health_score <=' => 49));

		// pr($data['healthyStdCount']);exit;

		$this->load->view('dashboard',$data);
	}

}
