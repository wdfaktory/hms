<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		if(!$this->session->school_user_id){
			redirect(base_url('../login'));
		}
		/* 
			$funcName = $this->router->fetch_method();
			$sesNotReq = array('open_reset_pwd','change_pwd','update_pwd','user_change_pwd');
			if(!in_array($funcName,$sesNotReq)){
				if(!$this->session->school_user_id){
					redirect(base_url('login'));
				}
			}
		 */

		$this->load->model('school_model');
	}

	public function index(){
		$data = array();
		$this->load->library('session');
		$userid = $this->session->userdata('school_user_id'); 
		$scholid = $this->session->userdata('school_id'); 
		$getschoolid = $this->db->query('select * from schools where user_id = '.$userid)->row();	

		$user_details = $this->db->query('select u.id,u.name,u.email,u.phone,u.designation,u.school_id,u.permissions,u.type,u.deleted,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.permissions = 2 and u.type = 4 and u.school_id = '.$scholid)->result();		
		$data['user_details'] = $user_details;		
		$this->load->view('view_users',$data);		
	}

	public function add_user(){
		$data = array();
		$this->load->library('session');
		$userid = $this->session->userdata('school_user_id');
		$users = $this->db->query('select * from users where id = '.$userid)->row();
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 2');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 2 and school_id ='.$users->school_id);	
		$data['designations'] = $designations->result();
		$this->load->view('add_user',$data);
	}

	public function edit_user(){
		$data = array();
		$editid = $this->uri->segment('3');	
		/*$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.designation,u.permissions,up.id as upid,up.user_id,up.permission,hst.id as hstid,hst.school_id,hst.user_id,hst.standard,hst.deleted as hstdel from users as u inner join user_permissions as up on u.id = up.user_id inner join health_standard as hst on hst.user_id = u.id where u.deleted = 0 and hst.deleted = 0 and u.id = '.$editid)->row();*/
		$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$editid)->row();		
		$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 2');	
		$data['permissions'] = $permissions->result();
		$designations = $this->db->query('select * from designations where deleted = 0 and type = 2');	
		$data['designations'] = $designations->result();		
		$data['userdata'] = $userdata;
		$this->load->view('edit_user',$data);			
	}	

	public function update_user(){		
		if(isset($_POST)){						
			$updateArr = array(
				'name'=>$_POST['name'],
				'email'=>$_POST['email'],
				'phone'=>$_POST['phone'],
				'designation'=>$_POST['designation'],
				'permissions'=>2,
				'type'=>$_POST['user_type']
			);
			$this->db->where('id', $_POST['user_id']);
			$this->db->update('users', $updateArr);
			$usrPer = 	'';
			foreach($_POST['curd'] as $key => $permi){
				$usrPer .= "$key,";	
			}
			$permissionarr = array(
				'permission'=>rtrim($usrPer,','), //implode(',',$_POST['permission'])
			);
			$this->db->where('user_id', $_POST['user_id']);
			$this->db->update('user_permissions', $permissionarr);
			$permissions = $this->db->query('select * from permissions where deleted = 0 and type = 2');	
			$prCurt = $permissions->result();
			$perId = $perId1 = array();
			foreach($prCurt as $pr){
				foreach($_POST['curd'] as $key => $permi){
					$curd_permsArr = array();
					if($pr->id == $key){
						$perId[] = $pr->id;
						$curd_permsArr = array(
							'user_id'=>$_POST['user_id'],
							'permission'=>$key,
							'curd'=>implode(',',$permi),
						);
						$checPer = curd_permission($_POST['user_id'],$key);
						if($checPer){
							$this->db->where('user_id', $_POST['user_id']);
							$this->db->where('permission', $key);
							$this->db->update('curd_permissions', $curd_permsArr);
						}
						else{
							$this->db->insert('curd_permissions', $curd_permsArr);
						}
					}
					else{
						$perId1[] = $pr->id;
					}
				}
			}
			
			$perId = array_diff(array_unique($perId1),$perId);
			
			if(!empty($perId)){
				foreach($perId as $pId){
					$checPer = curd_permission($_POST['user_id'],$pId);
					$curd_permsArr = array(
						'user_id'=>$_POST['user_id'],
						'permission'=>$pId,
						'curd'=>'0',
					);
					$this->db->where('user_id', $_POST['user_id']);
					$this->db->where('permission', $pId);
					$this->db->update('curd_permissions', $curd_permsArr);				
				}
			}
			//pr($perId);die;
			
			//die;
			/* foreach($_POST['permission'] as $key => $permi){
				$curd_permsArr = array(
					'user_id'=>$_POST['user_id'],
					'permission'=>$permi,
					'curd'=>implode(',',$_POST['curd'][$permi])
				);
				$this->db->where('user_id', $_POST['user_id']);
				$this->db->where('permission', $permi);
				$this->db->update('curd_permissions', $curd_permsArr);				
			} */

			if($_POST['designation'] == 100){
				$healtharr = array(										
					'standard'=>implode(',',$_POST['stdperm'])			
				);
				$this->db->where('user_id', $_POST['user_id']);
				$this->db->update('health_standard', $healtharr);	
			}

			$url = base_url().'users';
			redirect($url);
		}		
	}

	public function delete_user($id, $deleted=1){
		$delid = $this->uri->segment('3');
		$delarr = array(
			'deleted'=>$deleted
		);
		$this->db->where('id', $delid);
		$this->db->update('users', $delarr);
		$url = base_url().'users';
		redirect($url);
	}

	public function insert_user(){	
		$randpassword = $this->password_generate(7);		
		$this->load->library('session');
		$userid = $this->session->userdata('school_user_id'); 
		$getschoolid = $this->db->query('select * from schools where user_id = '.$userid)->row();	
		if(isset($_POST)){									
			$insertArr = array(
				'name'=>$_POST['name'],
				'email'=>$_POST['email'],
				'password'=>md5($randpassword),
				'phone'=>$_POST['phone'],
				'designation'=>$_POST['designation'],
				'permissions'=>2,
				'type'=>$_POST['user_type'],
				'school_id'=>$getschoolid->id
			);
			$this->db->insert('users', $insertArr);
			$insert_id = $this->db->insert_id();			
			/* $permissionarr = array(
				'user_id'=>$insert_id,
				'permission'=>implode(',',$_POST['permission'])
			); */
			foreach($_POST['curd'] as $key => $permi){
				$usrPer .= "$key,";	
			}
			$permissionarr = array(
				'user_id'=>$insert_id,
				'permission'=>rtrim($usrPer,','), //implode(',',$_POST['permission'])
			);
			$this->db->insert('user_permissions', $permissionarr);
			
			foreach($_POST['curd'] as $key => $permi){
				$curd_permsArr = array(
					'user_id'=>$insert_id,
					'permission'=>$key,
					'curd'=>implode(',',$permi)
				);
				$this->db->insert('curd_permissions', $curd_permsArr);
			}
			/* foreach($_POST['permission'] as $key => $permi){
				$curd_permsArr = array(
					'user_id'=>$insert_id,
					'permission'=>$permi,
					'curd'=>implode(',',$_POST['curd'][$permi])
				);
				pr($curd_permsArr);die;
				//$this->db->insert('curd_permissions', $curd_permsArr);				
			} */

			if($_POST['designation'] == 100){
				$healtharr = array(
					'school_id'=>$getschoolid->id,
					'user_id'=>$insert_id,
					'standard'=>implode(',',$_POST['stdperm'])			
				);
				$this->db->insert('health_standard', $healtharr);	
			}
			

			/** Sunil **/
			$template_val =' <p>Hi <b>'.ucfirst($_POST['name']).',</p>
                        <p>We have created the user login for HMS Portal and school has been added Kindly check the below credentials for your login.</p><br>
						<span><b>Email</b> : '.$_POST['email'].'</span><br>
						<span><b>Password</b> : '.$randpassword.'</span><br>
						<span><b>Link</b> : <a href="http://localhost:8080/hms/school/login" target="_blank">Click to Login</a></span><br>
						<br>
						<br>
                        <p>Good luck! Hope it works.</p>';
			
			$this->load->config('email');
			$this->load->library('email');
			$from = 'no-reply@hms.com';
			$to = 'shabeerksbr@hotmail.com';//$_POST['email'];
			$subject = 'Test Subject';
			$message = 'Hello World';
			$this->email->set_newline("\r\n");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($template_val);
			$this->email->send();
			/** End **/
			$url = base_url().'users';
			redirect($url);
		}
	}

	public function schools(){
		$this->load->model('school_model');
		$schDet = $this->school_model->getAllSchools();
		$data['school_det'] = $schDet;
		//die(print_r($schDet));
		//$this->load->view('school');
		$this->load->view('view_schools',$data);
	}

	public function schoolUpdate($id = ''){
		$data = array();
		if($id != "")
			$data['schoolData'] = $this->school_model->getschool($id);
			$this->load->view('school',$data);
	}

	public function add_school($scId = ''){
		$randpassword = $this->password_generate(7);
		if(isset($_POST)){
			$insertArr = array(
				'school_name'=>$_POST['schoolname'],
				'email'=>$_POST['email'],
				'password'=>md5($randpassword),
				'city'=>$_POST['city'],
				'address'=>	$_POST['address']
			);
			if($scId != 0)
				$this->db->update('schools', $insertArr,array('id' => $scId));
			else
				$this->db->insert('schools', $insertArr);

			$url = base_url().'users/schools';
			redirect($url);
		}
	}

	function password_generate($chars){
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}

	function delete_school($id){
		if($id){
			$this->school_model->updateDel($id);
		}
		$this->schools();
	}


	function view_config(){
		$config =  $this->config->item('curdPermissions');
		print_r($config);die;
	}

	public function open_reset_pwd(){
		$this->load->view('reset_pwd');		
	}
	
	public function reset_pwd(){
		
		$chk_email = $this->db->query('select * from users where email = "'.$_POST['email'].'"')->row();
		//echo '<pre>'; print_r($chk_email); die;
		if(!empty($chk_email)){
			$template_val =' <p>Hi <b>'.ucfirst($chk_email->name).',</p>
                        <p>Kindly click the below link to reset the password of your account.</p><br>
						
						<span><b>Link</b> : <a href="'. base_url('users/change_pwd/'.$chk_email->id) .'" target="_blank">Click to Login</a></span><br>
						<br>
						<br>
                        <p>Good luck! Hope it works.</p>';
			
			$from = 'shabeer77777@gmail.com';
			//$to = 	$_POST['email'];
			$to = 	'sun123@yopmail.com';
			$subject = 'Forgot Password';
			send_email($from, $to, $subject,$template_val,'html');		
			echo json_encode( array('success' => 1) );
		}else{
			echo json_encode( array('success' => 0) );
			
		}
	}
	
	public function change_pwd(){
		
		$this->load->library('session');
		$user_id =$this->uri->segment(3);
	
		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
	
		if($chk_email != NULL){
			$this->load->view('change_pwd');		
		} 
	}
	
	
	public function update_pwd(){
		
		$pattern = '/^(?=.*[!@#$%^&*-+])(?=.*[0-9])(?=.*[A-Z]).{8,20}$/';
		$password = $_POST['password'];
		if(preg_match($pattern, $password)){ 
			$insertArr = array(
				'password'=>md5($password),
			);
			$this->db->update('users', $insertArr,array('id' => $_POST['user_id']));
			$this->session->set_flashdata('success', 'Password changed successfully');
			redirect(base_url('users/view_profile'));
		}
	}


	public function view_profile(){
		$data = array();
		// echo '<pre>'; print_r($this->session); die;
		$this->load->library('session');
		$user_id =$this->session->school_user_id;
		
		$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.profile_image,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$user_id)->row();
		//echo '<pre>'; print_r($userdata); die;
		
		$data['userdata'] = $userdata;
		
		$this->load->view('profile_view',$data);		
	}

	public function edit_profile()
	{
		$data = array();

		$this->load->library('session');
		$user_id = $this->session->school_user_id;

		$userdata = $this->db->query('select u.id,u.name,u.email,u.phone,u.profile_image,u.designation,u.permissions,up.id as upid,up.user_id,up.permission from users as u inner join user_permissions as up on u.id = up.user_id where u.deleted = 0 and u.id = '.$user_id)->row();

		if($this->input->method() == 'post'){
			$this->load->library('image_lib');
			// echo '<pre>'; print_r($this->input->post()); die;
			// pr(FCPATH);die;
			$old_image_name = $userdata->profile_image;
			$update_arr = array(
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone')
			);
			$config['upload_path'] = FCPATH.'/assets/uploads/';
			$config['max_size'] = 2048;
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			// if ( ! $this->upload->do_upload('profileImage'))
			// {
			// 	$error = array('error' => $this->upload->display_errors());
			// 	echo '<pre>'; print_r($error); die;
			// }
			if($this->upload->do_upload('profileImage'))
			{
				$upload_data = $this->upload->data();
				$update_arr['profile_image'] = $upload_data['file_name'];
				// echo '<pre>'; print_r($upload_data); die;
				$configure =  array(
					'image_library'   => 'gd2',
					'source_image'    =>  $upload_data['full_path'],
					'maintain_ratio'  =>  FALSE,
					'width'           =>  100,
					'height'          =>  100,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configure);
				$this->image_lib->resize();
				if(!empty($old_image_name)){
					unlink($config['upload_path'].$old_image_name);
				}
			}
			
			$this->db->update('users', $update_arr, array('id' => $user_id));
			$url = base_url().'users/view_profile';
			redirect($url);
		}
		// echo '<pre>'; print_r($userdata); die;
		
		$data['userdata'] = $userdata;
		
		$this->load->view('profile_edit',$data);
	}
	
	public function user_change_pwd(){
		
		$data = array();
	
		$this->load->library('session');
		$user_id =$this->session->school_user_id;

		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
	
		if($chk_email != NULL){
			$data['user_id'] = $user_id;
			$this->load->view('change_pwd', $data);		
		} 
	}

}
