<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	private $parents_table = 'users';
	private $permission_table = 'user_permissions';
	private $schools_table = 'schools';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		redirect(base_url('../login'));
		//$this->load->view('login');
	}

	public function check_login(){
		$this->load->library('session');
		$this->load->model('login_model');
		$user_name = $this->input->post('name');
		$password = $this->input->post('password');
		$md5_pass = md5($password);
		$wherArr = array('email' => $user_name, 'password' => $md5_pass, 'permissions' => 2);
		$chk = $this->login_model->get_row($this->parents_table, $wherArr);	
		if($chk != NULL){
			if($this->input->post('remember') == 'on'){
				$remember ='1';
			}else{
				$remember ='0';
			}
			$sessArr = array('school_user_name' => $user_name, 'school_user_id' => $chk->id,'user_type'=>$chk->type,'school_id' => $chk->school_id,'remember'=>$remember);
			$this->session->set_userdata($sessArr);
			/*if($chk->permissions == 1){
				$whrprArr = array('user_id'=>$chk->id);
				$chkpermission = $this->login_model->get_row($this->permission_table, $whrprArr);
				$explodepermission = explode(',',$chkpermission->permission);
				if(in_array(3, $explodepermission)){
					redirect(base_url('users'));
				} else if(in_array(1, $explodepermission)){
					redirect(base_url('users/schools'));
				} else if(in_array(2, $explodepermission)){
					redirect(base_url('parents/students'));
				}
			}*/
			redirect(base_url('dashboard'));
		}else{
			redirect(base_url());
		}
	}

	public function logout(){
		$this->load->library('session');
		$this->session->unset_userdata('school_user_id');
		$this->session->unset_userdata('school_user_name');
		$this->session->unset_userdata('school_id');
		redirect(base_url('login'));
	}
}
