<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->schoolId = $this->session->userdata('school_id');
        $this->load->model(['school_model']);
    }
    
    public function index()
    {
        $data = [];
        $this->db->select('students.*, attendance.id as atid,attendance.no_of_working_days,attendance.no_of_sick_leave');
        $this->db->where('school_id', $this->schoolId);
        $this->db->where('deleted',0);
        $this->db->join('attendance','attendance.student_id=students.id');
        $data['students'] = $this->db->get('students')->result();
        // pr($data);die();
        // pr($this->school_model->getAllStudents());die;
        $this->load->view('view_attendance', $data);
    }

    public function add_attendance()
    {
        $this->load->view('add_attendance');
    }

    public function save_attendance()
    {
        $url = base_url().'attendance';
        if($this->input->method() == 'post'){
            $health_score = floor((($_POST['workingDays']-$_POST['sickLeave'])/$_POST['workingDays'])*100);
            // $this->db->where('student_id',$_POST['studentName']);
            // $result = $this->db->get('attendance');
            $attendanceArr = [
                'student_id'=>$_POST['studentName'],
                'no_of_working_days' => $_POST['workingDays'],
                'no_of_sick_leave' => $_POST['sickLeave'],
            ];
            $this->db->insert('attendance',$attendanceArr);
            $studentArr = [
                'health_score' => $health_score
            ];
            $this->db->update('students',$studentArr, array('id'=>$_POST['studentName']));
			redirect($url);
        }
        redirect($url);
    }

    public function get_students_name()
    {
        if($this->input->method() == 'post'){
            $this->db->select('students.id,students.student_name');
            $this->db->where('school_id',$this->schoolId);
            $this->db->where('standard', $_POST['standard']);
            $this->db->where('section', $_POST['section']);
            $this->db->where('deleted',0);
            $students = $this->db->get('students')->result_array();
            echo json_encode($students);
        }
    }

    public function attendanceUploadTemplate()
    {
        $download_data['header'] = array('ICID','Roll No','Student Name','No Of Working Days',
        'No Of Sick Leave');
		$path = str_replace('\\', '/', BASEPATH).'../third_party/Excel.php';
		require $path;
		$excel = Excel::downloadAttendanceTemp($download_data);
        exit;
    }

    public function saveUploadedAttendance()
    {
        ini_set('max_execution_time', 300);
		set_time_limit(300);
		if(isset($_FILES['attendaceFileUpload']['name']) && $_FILES['attendaceFileUpload']['name'] != ''){

			$path = str_replace('\\', '/', BASEPATH).'../third_party/Excel.php';
			require $path;
			$fileName = $_FILES['attendaceFileUpload']['name'];
			$config['upload_path'] = FCPATH . 'report_files/';                                
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['overwrite']     = true;
			//$config['max_size'] = 10000;
			//echo $fileName;
			$this->load->library('upload');
			$this->upload->initialize($config);
			$isUploaded = $this->upload->do_upload('attendaceFileUpload');
			$uploadedData = $this->upload->data();
			$inputFileName = $uploadedData['full_path'];
	
			//$this->load->library('excel');
			$file_path = $inputFileName;//FCPATH . 'report_files/' . $fileName;
			
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
	
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			//echo '<pre>';print_r($cell_collection);die;
	
			//extract to a PHP readable array format
			foreach ($cell_collection as $cell) {
				$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();//getValue();
	
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				} else {
					if($data_value){
						$arr_data[$row][$column] = $data_value;
					}
				}
			}
			$data['header'] = $header;
			$data['values'] = $arr_data;
			$totalordercount=count($data['values']);
			$totalordersuccess=0;
			$leftout = array();
			$i = 1;
			$schoolId = $this->schoolId;
			foreach ($data['values'] as $key => $value) {
                $attendanceArr = [
                    'no_of_working_days' => (isset($value['D'])) ? $value['D'] : 0,
					'no_of_sick_leave' => (isset($value['E'])) ? $value['E'] : 0,
                ];
                $studentArr = [];
                if($attendanceArr['no_of_working_days'] == 0){
                    $studentArr['health_score'] = 0;
                }
                else{
                    $studentArr['health_score'] = floor((($attendanceArr['no_of_working_days']-$attendanceArr['no_of_sick_leave'])/$attendanceArr['no_of_working_days'])*100);
                }
                // pr($studentArr['health_score']);die;
                if(isset($value['A']) && isset($value['B'])){
                    $attendanceArr['student_id'] = $value['A'];
                    $this->db->insert('attendance',$attendanceArr);

                    $this->db->where('id',$value['A']);
                    $this->db->where('roll_no',$value['B']);
                    $this->db->update('students',$studentArr);
                }
            }
            $this->session->set_flashdata('status', 'Uploaded Students Attendance data successfully');
        }
        $url = base_url().'attendance';
		redirect($url);
    }

    public function edit_attendance($id)
    {
        if($this->input->method()=='post'){
            $health_score = floor((($_POST['workingDays']-$_POST['sickLeave'])/$_POST['workingDays'])*100);
            $attendanceArr = [
                'no_of_working_days' => $_POST['workingDays'],
                'no_of_sick_leave' => $_POST['sickLeave']
            ];
            $this->db->where('id',$id);
            $this->db->update('attendance',$attendanceArr);

            $studentArr = [
                'health_score' => $health_score
            ];
            $this->db->update('students',$studentArr, array('id'=>$_POST['studentId']));

            $url = base_url().'attendance';
		    redirect($url);
        }
        $data = array();
        $this->db->select('attendance.*,students.id as stid,
        students.student_name,students.standard,students.section,students.category');
        $this->db->where('attendance.id',$id);
        $this->db->join('students','students.id=attendance.student_id');
        $data['attendance'] = $this->db->get('attendance')->row();
        // pr($data);die;
        $this->load->view('edit_attendance', $data);
    }
}