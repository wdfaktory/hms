<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->school_user_id){
			redirect(base_url('login'));
		}
	}

}
