<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	private $userpermission_table = 'user_permissions';
	private $permission_table = 'permissions';
	private $sick_report = 'sick_report';

	public function __construct(){
		parent::__construct();
		if(!$this->session->school_user_id){
			redirect(base_url('login'));
		}
	}

	public function reports(){
		$school_id = $this->session->school_id;
		$this->load->model('reports_model');
		$data['reports'] = $this->reports_model->getStdentsReports($school_id);
		$this->load->view('reports_view',$data);
	}

	public function view_student_report(){
		$report_id = $this->uri->segment(2);

		$this->load->library('mongo_db');
		$this->mongo_db->connect();

		$stud_id = $this->session->school_id;
		$this->mongo_db->where('_id',new MongoDB\BSON\ObjectId($report_id));
		$result = $this->mongo_db->getOne($this->sick_report);
		
		$data['report_data'] = $result[0];

		$this->load->view('sick_report_view',$data);
	}
}
