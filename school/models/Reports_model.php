<?php 
class Reports_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getStdentsReports($school_id){
		$this->db->select('s.id,s.student_name,s.standard,s.section,s.sex,s.age,r.r_mongo_id,r.type,r.status,r.created_date');
		$this->db->where('s.school_id',$school_id);
		$this->db->join('students s','r.stud_id = s.id','inner');
		$res = $this->db->get('reports r');
		return $res->result_array();
	}

}
?>