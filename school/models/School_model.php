<?php 
class School_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getAllSchools(){
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->result();
	}

	public function getschool($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->row();
	}

	public function updateDel($id){
		$this->db->update('schools', array('deleted' => 1),array('id' => $id));
	}

	public function getAllStudents($schoolId){
		$this->db->where('deleted',0);
		$this->db->where('school_id',$schoolId);
		$res = $this->db->get('students');
		return $res->result();
	}

	public function getUserPerStud($schoolId,$permitted){
		$this->db->select('students.*, schools.school_name');
		$this->db->where('students.deleted',0);
		$this->db->where('students.school_id',$schoolId);
		$this->db->where_in('students.standard',$permitted);
		$this->db->join('schools','schools.id=students.school_id');
		$res = $this->db->get('students');
		return $res->result();
	}

	public function getStudent($id,$schoolId){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$this->db->where('school_id',$schoolId);
		$res = $this->db->get('students');
		return $res->row();
	}

	public function getStudentParent($id){
		$this->db->where('student_id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('parents');
		return $res->row();
	}

	public function updateStudentDeleted($id){
		$this->db->update('students', array('deleted' => 1),array('id' => $id));
		$this->db->update('parents', array('deleted' => 1),array('student_id' => $id));
	}

	public function getUserPermission($userid,$schoolId){
		$this->db->where('user_id',$userid);
		$this->db->where('school_id',$schoolId);
		$this->db->where('deleted',0);
		$res = $this->db->get('health_standard');
		return $res->row();
	}

	public function get_count($table,$whrArr = array())
    {
        if(!empty($whrArr))
            $this->db->where($whrArr);
        $q = $this->db->get($table);
        return $q->num_rows();
        
    }

}
?>