<?php 
class Students_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getAllSchools(){
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->result();
	}

	public function getschool($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('schools');
		return $res->row();
	}

	public function updateDel($id){
		$this->db->update('schools', array('deleted' => 1),array('id' => $id));
	}

	public function getAllStudents(){
		$this->db->where('deleted',0);
		$res = $this->db->get('students');
		return $res->result();
	}

	public function getStudent($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('students');
		return $res->row();
    }
    
    public function getStudentAllData($id,$school_id,$permitted){
        $this->db->select('s.*,p.*,sc.school_name,sc.address as "school_address",sc.city');
		$this->db->where('s.id',$id);
        $this->db->where('s.deleted',0);
		$this->db->where('s.school_id',$school_id);
		$this->db->where_in('s.standard',$permitted);
        $this->db->join('parents p','s.parent_id = p.id','left');
        $this->db->join('schools sc','sc.id = s.school_id');
		$res = $this->db->get('students s');
		return $res->row();
    }
    

	public function getStudentParent($id){
		$this->db->where('s.id',$id);
		$this->db->where('deleted',0);
		$this->db->join('students s','s.parent_id = p.id','left');
		$res = $this->db->get('parents p');
		return $res->row();
	}

	function getParent($parentEmail){
		$this->db->where('email',$parentEmail);
		$this->db->where('deleted',0);
		$res = $this->db->get('parents');
		return $res->row();
	}

	public function updateStudentDeleted($id){
		$this->db->update('students', array('deleted' => 1),array('id' => $id));
		//$this->db->update('parents', array('deleted' => 1),array('student_id' => $id));
	}


}
?>