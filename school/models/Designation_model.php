<?php 
class Designation_model extends MY_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getAllDesignations(){
		$this->db->where('deleted',0);
		$res = $this->db->get('designations');
		return $res->result();
	}

	public function getDesignations($id){
		$this->db->where('id',$id);
		$this->db->where('deleted',0);
		$res = $this->db->get('designations');
		return $res->row();
	}

	public function updateDel($id){
		$this->db->update('designations', array('deleted' => 1),array('id' => $id));
	}



}
?>