<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sidebar_details(){    
    $CI =& get_instance();
    $CI->load->library('session');
    $data = array();
    $userid = $CI->session->userdata('school_user_id'); 
    $chkpermission = $CI->db->query('select * from user_permissions where user_id = '.$userid)->row();
    $explodepermission = explode(',',$chkpermission->permission);    
    $data['userpermissions'] = $explodepermission;    
    $perdata = $CI->db->query('select * from permissions where deleted = 0 and type = 2')->result();
    $data['permissions'] = $perdata;
    return $data; 
} 

function permssion_details($userpermsion){
    $CI =& get_instance();
    $permis_details = $CI->db->query('select * from permissions where deleted = 0 and id IN('.$userpermsion.')')->result();
    $permissionstr = '';
    $permission = array();
    foreach($permis_details as $details){               
        $permission[] = $details->name;
    }
    $permissionstr = implode(',',$permission);  
    return $permissionstr;
}

function curd_permission($userid,$permission){
    $CI =& get_instance();
    $curd_details = $CI->db->query('select * from curd_permissions where user_id = '.$userid.' and permission = '.$permission)->row();
    return $curd_details;
}

function curd_access(){
    $CI =& get_instance();
    $CI->load->library('session');
    $userid = $CI->session->userdata('school_user_id'); 
    $curd_details = $CI->db->query('select * from curd_permissions where user_id = '.$userid)->result();
    return $curd_details;

}
function school_designation($id){    
    $CI =& get_instance();    
    $design = $CI->db->query('select * from designations where id = '.$id)->row();
    return $design;
}

function healthstd($uid){
    $CI =& get_instance();    
    $healtstd = $CI->db->query('select * from health_standard where user_id = '.$uid)->row();
    return $healtstd;
}

function health_standard($uid,$schoolid){    
    $CI =& get_instance();    
    $healtstd = $CI->db->query('select * from health_standard where school_id = '.$schoolid.' and user_id = '.$uid)->row();   
    if(!empty($healtstd)){
        $explodehealth = explode(',',$healtstd->standard);    
        $healtstd = $CI->config->item('stdPermissions');         
        $stdstr = '';
        $standard = array();
        foreach($explodehealth as $key=>$value){
            $standard[] = $healtstd[$value];
        }        
        $stdstr = implode(',',$standard);          
    } else {    
        $stdstr = '-';
    }        
    return $stdstr;
}

function pr($data){
    echo "<pre>";
    $d = print_r($data)."</pre>";
    return $d;
}

function getUserPermission($userid,$schoolId){
    $CI =& get_instance(); 
    $CI->db->where('user_id',$userid);
    $CI->db->where('school_id',$schoolId);
    $CI->db->where('deleted',0);
    $res = $CI->db->get('health_standard');
    $getUsrPer = $res->row();
    if($getUsrPer != ''){
        $permitted = explode(',',$getUsrPer->standard);
        $stdArr = $CI->config->item('standards');
        $stdPer = array_chunk($stdArr,5);
        $studPerArr = array();
        foreach($permitted as $per){
            $per = $per-1;
            $studPerArr = array_merge($studPerArr,$stdPer[$per]);
        }
        return $studPerArr;
    }
    else return false;
}

function user_profile_image($userid){
    $CI =& get_instance();
    $user_details = $CI->db->query('select * from users where id = '.$userid)->row();
    return $user_details->profile_image;
}


?>