<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

$config['curdPermissions'] = array(
	'1' => 'All',
	'2' => 'Add',
	'3' => 'Edit',
	'4' => 'View',
	'5' => 'Delete'
);

$config['stdPermissions'] = array(
	'1' => 'Primary',
	'2' => 'Secondary',
	'3' => 'Hr. Secondary'
);

$config['bloodGroups'] = array(
	'A+' => 'A+ve',
	'B+' => 'B+ve',
	'AB+' => 'AB+ve',
	'O+' => 'O+ve',
	'A-' => 'A-ve',
	'B-' => 'B-ve',
	'AB-' => 'AB-ve',
	'O-' => 'O-ve',
);

$config['sections'] = array(
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
);

$config['standards'] = array(
	1,2,3,4,5,6,7,8,9,10,11,12
);

$config['category'] = array('Kinder Garten','Primary','Middle','Secondary','Higher Secondary');

$config['studImgBucket'] = 'student-data-test';
$config["accessKey"] = "AKIAIZA7UQX24V4R2ZMQ";
$config["secretKey"] = "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";
//$config["useSSL"] = TRUE;

?>