<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<style>
    #std_view_wrapper .btn-action {
        padding: 1px 18px;
        border-radius: 4px;
    }
</style>

<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
        <div class="col-lg-12">
            <section class="box">
                <header class="panel_header">
                    <h2 class="title pull-left">View Students</h2>                                
                </header>
                <a href="<?=base_url().'students-data'?>">Add Student</a>
                <div class="content-body">
                <?php if($this->session->flashdata('status')):?>
					<div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
				<?php endif; ?><?php if($this->session->flashdata('error')):?>
					<div class="alert alert-danger"><?=$this->session->flashdata('error')?></div>	
				<?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="student-upload">
                                <h2>Students Bulk Upload</h2><br/>
                                <strong>Note:</strong> Download the sample document given below, update the respective values right from second row after headers and upload it here. This sheet can be used to bulk upload student's of a section, standard or schoolwise.</br>
                                <a href='<?=base_url('students/studentUploadTemplate')?>'>(Download Sample)</a>
                                <form action="<?=base_url('students/saveUploadedStud')?>" method='POST' enctype='multipart/form-data'>
                                <div class="form-group col-lg-6">
                                <label for="stdFileUpload">Upload your file here</label>
                                <input type='file' name='stdFileUpload' id='stdFileUpload' class="form-control" onchange="return fileValidation()" required/>
                                <div id="uploadedRes"></div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="submit" value="Submit">                      
                                <div class="clearfix"></div>
                                </form>
                            </div>

                            <div class="table-responsive" data-pattern="priority-columns">

                                <!-- <div class="table-responsive" data-pattern="priority-columns"> -->
                                <div class='filters col-lg-6' style=''>
                                    <label> Filter by Standard </label>
                                    <select id='std_filter' style="height: 33px;padding: 5px;">
                                    <option value=0>Standard</option>
                                    <?php for($std=1;$std<=12;$std++) { 
                                        echo "<option value = '$std'> $std </option>";
                                     } ?>
                                    </select>
                                </div>
                                <div class='filters col-lg-6 text-right' style=''>
                                    <label> Filter by Section </label>
                                    <select id='sec_filter' style="height: 33px;padding: 5px;">
                                    <option value=''>Section</option>
                                    <?php
                                        $stdArr = $this->config->item('sections');
                                         foreach($stdArr as $val){ 
                                            echo "<option value='$val' > $val </option>";
                                        }?>
                                    </select>
                                </div>
                                
                                <table id="std_view" class="table vm table-small-font no-mb table-bordered table-striped">
                                    <thead>
                                        <tr style="white-space:nowrap;">
                                            <th>ICID</th>
                                            <th>School Id</th>
                                            <th style="text-align:left;">School Name</th>
                                            <th>Student Id</th>
                                            <th>Student Name</th>
                                            <th>Category</th>
                                            <th>Standard</th>
                                            <th>Sec</th>
                                            <!-- <th>Age</th> -->
                                            <th>Gender</th>
                                            <!-- <th>RH Type</th> -->
                                            <th>Pube (Y/N)</th>
                                            <th>Special Care Need (Y/N)</th>
                                            <th>Under Vaccination (Y/N)</th>
                                            <th>Alergitic (Y/N)</th>
                                            <th>Asthma (Y/N)</th>
                                            <th>Health Score</th>
                                            <th style='min-width:210px;'>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>  
</section>                    
<?php $this->load->view('templates/footer'); ?>
<link href="<?=base_url()?>assets/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="<?=base_url()?>assets/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
 
$(function () {
var request = <?=(!empty($students))?json_encode($students):json_encode(array())?>;
  var table = $('#std_view').DataTable( {
        "data" : request,
    "columns": [
            { "data": "id" , "className":"text-center"},
            { "data": "school_id", "className":"text-center" },
            { "data": "school_name" , "className":"text-left"},
            { "data": "roll_no", "className":"text-center" },
            { "data": "student_name", "className":"text-center" },
            { "data": "category","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==""?'-':data;
            }},
            { "data": "standard", "className":"text-center" },
            { "data": "section", "className":"text-center" },
            /*{ "data": "age" },*/
            { "data": "null",
                "mRender":function(data,type,full){
                    if(full['sex'] == 1)
                        return 'Male';
                    else
                        return 'Female';
                }
            },
            { "data": "pube","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==0?'N':'Y';
            }},
            { "data": "special_care_need","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==0?'N':'Y';
            }},
            { "data": "under_vaccination","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==0?'N':'Y';
            }},
            { "data": "alergitic","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==0?'N':'Y';
            }},
            { "data": "asthma","className":"text-center",
                "render": function ( data, type, row, meta ) {
                return data==0?'N':'Y';
            }},
            { "data": "health_score","className":"text-center",
                "render": function ( data, type, row, meta ) {
                if(data>=75&&data<=100){
                    return '<span style="width:35%;" class="badge badge-success">'+data+'</span>';
                }else if(data>=50&&data<=74){
                    return '<span style="width:35%;" class="badge badge-warning sick_cond">'+data+'</span>';
                }
                else{
                    return '<span style="width:35%;" class="badge badge-danger">'+data+'</span>';
                }
            }},
            /*{ "data": "blood_group" },*/
            { "data": "null",
                "mRender":function(data,type,full){
                    let view = '<a style="margin-left:3px" target="_blank" class="editStd"  href="<?php echo base_url('student-dashboard/');?>'+full['id']+'"> <label class = "btn btn-primary btn-action"> <i class="fa fa-eye" aria-hidden="true"></i> </label> </a>';
                    let edit = '<a style="margin-left:3px" target="_blank" class="editStd"  href="<?php echo base_url('students-data/');?>'+full['id']+'"> <label class = "btn btn-primary btn-action"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </label> </a>';
                    let del = '<a style="margin-left:3px"  class="editStd deletestd"  href="<?php echo base_url('students/delete_student/');?>'+full['id']+'"> <label class = "btn btn-primary btn-action"> <i class="fa fa-trash-o" aria-hidden="true"></i> </label> </a>';
                    return view+' '+edit+' '+del;
                }
            },
            
        ],
        "order": [[ '0', "desc" ]],
        "iDisplayLength": 10,
        select: {
        style:    'os',
        selector: 'td:first-child'
        },
        createdRow: function (row, data, index) {
            $(row).css('white-space','nowrap');
        }

    });
    
$('#std_filter,#sec_filter').change( function() {
    table.draw();
} );

$('.deletestd').on('click', function(){
    if(window.confirm("Are you sure you want to disable?")){
        return true;
    } else {
        return false;
    }
 });

});
                
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        
        var selStd = parseInt( $('#std_filter').val(), 10);
        var std = parseInt( data[2], 10 );
        var sec = data[3];
        var selSec = $('#sec_filter').val();
        if(std == selStd && selSec == sec)
        return true;
        else if(std == selStd && selSec == '')
            {return true;}
        else if(selSec == sec && selStd == 0) // || strpos(selBldGrp, bldGrp) !== false)
            {return true;}
        else if(selStd == 0 && selSec == '')
            return true;
        else
            return false;
    }
);

function fileValidation(){
    var fileInput = document.getElementById('stdFileUpload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.xls|\.xlsx|\.csv)$/i;
    if(!allowedExtensions.exec(filePath)){
        $('#uploadedRes').html('<i class="fa fa-times-circle"></i> Please upload file having extensions .xlx or .xlsx').css('color','red');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadedRes').html('<i class="fa fa-check-circle"></i> Uploaded').css('color','green');
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}

</script>
