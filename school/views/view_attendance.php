<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
        <div class="col-lg-12">
            <section class="box">
                <header class="panel_header">
                    <h2 class="title pull-left">View Attendance</h2>
                </header>
                <a href="<?=base_url().'attendance/add_attendance'?>">Add Attendance</a>
                <div class="content-body">
                <?php if($this->session->flashdata('status')):?>
					<div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
				<?php endif; ?><?php if($this->session->flashdata('error')):?>
					<div class="alert alert-danger"><?=$this->session->flashdata('error')?></div>	
				<?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="student-upload">
                                <h2>Attendance Bulk Upload</h2><br/>
                                <strong>Note:</strong> Download the sample document given below, update the respective values right from second row after headers and upload it here. This sheet can be used to bulk upload student's attendance.</br>
                                <a href='<?=base_url('attendance/attendanceUploadTemplate')?>'>(Download Sample)</a>
                                <form action="<?=base_url('attendance/saveUploadedAttendance')?>" method='POST' enctype='multipart/form-data'>
                                <div class="form-group col-lg-6">
                                <label for="attendaceFileUpload">Upload your file here</label>
                                <input type='file' name='attendaceFileUpload' id='attendaceFileUpload' class="form-control" onchange="return fileValidation()" required/>
                                <div id="uploadedRes"></div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="submit" value="Submit">                      
                                <div class="clearfix"></div>
                                </form>
                            </div>


                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ICID</th>
                                            <th>Student Id</th>
                                            <th>Student Name</th>
                                            <th>No of working days</th>
                                            <th>No of sick leave</th>
                                            <th>Health Score</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($students as $key => $stud) { ?>
                                        <tr>
                                            <td><?= $stud->id?></td>
                                            <td><?= $stud->roll_no?></td>
                                            <td><?= $stud->student_name?></td>
                                            <td><?= $stud->no_of_working_days?></td>
                                            <td><?= $stud->no_of_sick_leave?></td>
                                            <!-- <td><?= floor((($stud->no_of_working_days-$stud->no_of_sick_leave)/$stud->no_of_working_days)*100)?></td> -->
                                            <td>
                                            <?php if($stud->health_score>=75 && $stud->health_score <=100){
                                                $spanclass="badge badge-success";
                                            } else if($stud->health_score>=50 && $stud->health_score<=74) {
                                                $spanclass="badge sick_cond";
                                            } else { 
                                                $spanclass="badge badge-danger";
                                            }?>
                                            <span style="width:25%;" class="<?=$spanclass?>"><?= $stud->health_score?></span>
                                            </td>
                                            <td><a href="<?= base_url().'attendance/edit_attendance/'.$stud->atid?>">Edit</a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>    
</section> 
<?php $this->load->view('templates/footer'); ?>

<script>
function fileValidation(){
    var fileInput = document.getElementById('attendaceFileUpload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.xls|\.xlsx|\.csv)$/i;
    if(!allowedExtensions.exec(filePath)){
        $('#uploadedRes').html('<i class="fa fa-times-circle"></i> Please upload file having extensions .xlx or .xlsx').css('color','red');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadedRes').html('<i class="fa fa-check-circle"></i> Uploaded').css('color','green');
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>