<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$studId = ($this->uri->segment(2) !='') ? $this->uri->segment(2) : '';
?><!DOCTYPE html>
<link href="<?=base_url('assets/css/common.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/css/main.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css">
<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<style>
.form-group .error {
    color: #f44336 !important;
    font-weight: 400 !important;
    font-size: 13px !important;
}

table.dataTable thead .sorting:after {
    content: none; !important;
}

label.error{
	top: inherit !important;
}

</style>
 <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add/Update Student</h2>                            
                        </header>
                        <!-- <a href="<?=base_url().'students-data'?>">Add Students</a> -->
                        <div class="content-body">
							<?php if($this->session->flashdata('status')):?>
								<div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
							<?php endif; ?>
                            <div class="row">

                                <div class="col-xs-12">
							      	<form action="<?=base_url().'students/add_students/'.$studId?>" method="POST" class="parent-dtl add-student custom" id="parent-dtl" name="student_data_feed" enctype='multipart/form-data' >
							      		<div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="roll_no" name="roll_no" value="<?=(isset($studData->roll_no))?$studData->roll_no:'';?>" required>
										  </div>
										  <label class="form-label" for="roll_no">Student Roll No</label>
		                                    <!-- <select class="form-control m-bot15" name="school" required>
		                                        <option value=''>Schools</option>    
		                                        <?php foreach($schools as $school){ ?>
		                                        	<option value=<?=$school->id?> <?php echo (isset($studData->school_id) && ($studData->school_id == $school->id))? 'selected':'';?> ><?=$school->school_name?></option>
		                                        <?php } ?>
		                                    </select> -->
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="studentname" name="studentname" value="<?=(isset($studData->student_name))?$studData->student_name:'';?>" required>
											</div>
											<label class="form-label" for="studentname">Student name</label>
								  		</div>
										<div class="form-group col-lg-6">
											<div class="controls">
											<select class="form-control" id="category" name="category"  required>
												<option value = ''>-- Select --</option>
												<?php foreach($this->config->item('category') as $val){ 
													echo "<option value='$val' ".(isset($studData->category) &&  ($studData->category == $val)? 'selected': '') ." > $val </option>";
												}?>
											</select>
											</div>
											<!-- <input type="text" class="form-control" id="section" name="section" value="<?=(isset($studData->section))?$studData->section:'';?>" required> -->
								    		<label class="form-label" for="category">Category</label>
								  		</div>
								    	<div class="form-group col-lg-6">
										<div class="controls">
											<select class="form-control" id="standard" name="standard"  required>
												<option value = ''>-- Select --</option>
												<?php foreach($permitted as $val){ 
													echo "<option value='$val' ".(isset($studData->standard) && ($studData->standard == $val)? 'selected': '') ." > $val </option>";
												}?>
											</select>
											</div>
											<!-- <input type="text" class="form-control" id="standard" name="standard" value="<?=(isset($studData->standard))?$studData->standard:'';?>" required> -->
								    		<label class="form-label" for="standard">Standard</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
											<select class="form-control" id="section" name="section"  required>
												<option value = ''>-- Select --</option>
												<?php foreach($this->config->item('sections') as $val){ 
													echo "<option value='$val' ".(isset($studData->section) &&  ($studData->section == $val)? 'selected': '') ." > $val </option>";
												}?>
											</select>
											</div>
											<!-- <input type="text" class="form-control" id="section" name="section" value="<?=(isset($studData->section))?$studData->section:'';?>" required> -->
								    		<label class="form-label" for="section">Section</label>
								  		</div>
										  <div class="form-group col-lg-6">
										  <div class="controls">
								    		<input type="text" class="form-control" id="dob" name="dob" value="" required> 
											</div>
											<label class="form-label" for="dob">Date of Birth</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="number" class="form-control" id="age" name="age" value="<?=(isset($studData->age))?$studData->age:'';?>" required>
											</div>
											<label class="form-label" for="age">Age</label>
								  		</div>
								    	<div class="form-group col-lg-6">                                    
										<div class="controls">
		                                    <select class="form-control m-bot15" id="sex" name="sex" required>
		                                        <option value=''>Select</option>    
		                                        <option value='1' <?php echo (isset($studData->sex) && ($studData->sex == 1))? 'selected':'';?>>Male</option>
		                                        <option value='2' <?php echo (isset($studData->sex) && ($studData->sex == 2))? 'selected':'';?>>Female</option>
											</select>
											</div>
		                                    <label class="form-label" for="age">Sex</label>
		                                </div>
								    	<div class="form-group col-lg-6">
										<div class="controls">
										<select class="form-control" id="bgroup" name="bgroup"  required>
										<option value = ''>-- Select --</option>
										<?php foreach($this->config->item('bloodGroups') as $key => $val){ 
											echo "<option value='$key' ".(isset($studData->blood_group) && ($studData->blood_group == $key)? 'selected': '') ." > $val </option>";
										}?>
										
										</select>
										</div>
								    		<!-- <input type="text" class="form-control" id="bgroup" name="bgroup" value="<?=(isset($studData->blood_group))?$studData->blood_group:'';?>" required> -->
								    		<label class="form-label" for="bgroup">RH Type</label>
								  		</div>	
								  		<div class="form-group col-lg-6">
										  <div class="controls">
											<input type="text" class="form-control" id="fname" name="fname" value="<?=(isset($studData->father_name))?$studData->father_name:'';?>" required>
											</div>
								    		<label class="form-label" for="name">Father name</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
											<input type="text" class="form-control" id="mname" name="mname" value="<?=(isset($studData->mother_name))?$studData->mother_name:'';?>" required>
											</div>
								    		<label class="form-label" for="name">Mother name</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="foccupation" name="foccupation" value="<?=(isset($studData->occupation))?$studData->occupation:'';?>" required>
											</div>
											<label class="form-label" for="occupation">Father Occupation</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="phoneno" name="phoneno" value="<?=(isset($studData->phone_number))?$studData->phone_number:'';?>" required>
											</div>
											<label class="form-label" for="phone">Phone Number</label>
								  		</div>
								  		<div class="form-group col-lg-6">
										  	<div class="controls">
								    		<input type="text" class="form-control" id="mobileno" name="mobileno" value="<?=(isset($studData->mobile_number))?$studData->mobile_number:'';?>" required>
											</div>
											<label class="form-label" for="mobile">Mobile number</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="email" class="form-control" id="email" name="email" value="<?=(isset($studData->email))?$studData->email:'';?>" required>
											</div>
											<label class="form-label" for="email">Email Id</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<textarea minlength="15" class="form-control" name="address" id="address" required> <?=(isset($studData->address))?$studData->address:'';?> </textarea>
											</div>
											<label class="form-label" for="address">Address</label>
								  		</div>	
										<div class="form-group col-lg-6">
										  	<!-- <?php if(isset($studData->image_name)) { ?>
										    <div id='studImgView'>
												<img src='<?= base_url() . 'student_records/'.$studData->image_name ?>' style='width:100px;height:auto;' />
											</div>
											<?php } ?> -->
											<div class="controls">
								    		<input type='file'  class="form-control" name="studImg" id="studImg" <?=(isset($studData->image_name)) ? '' :'required';?> onchange="return fileValidation()" />
											</div>
											<label class="form-label" for="studImg">Upload Student Image</label> <!-- style='margin-left: 300px;margin-top: 50px;' -->
											<div id="imagePreview" style='margin-top: 10px;'></div>
										  </div>	
										  
										  <div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="gname" name="gname" value="<?=(isset($studData->guardian_name))?$studData->guardian_name:'';?>" required>
								    		</div>
											<label class="form-label" for="gname">Guardian name</label>
										</div>
										
										<div class="form-group col-lg-6">                                    
											<div class="controls">
		                                    <select class="form-control m-bot15" id="pube" name="pube" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->pube) && ($studData->pube == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->pube) && ($studData->pube == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="pube">Pube</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="spackage" name="spackage" value="<?=(isset($studData->subscription_package))?$studData->subscription_package:'';?>" required>
								    		</div>
											<label class="form-label" for="spackage">Subscription Package</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">
		                                    <select class="form-control m-bot15" id="scneed" name="scneed" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->special_care_need) && ($studData->special_care_need == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->special_care_need) && ($studData->special_care_need == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="scneed">Special Care Need</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">
		                                    <select class="form-control m-bot15" id="splan" name="splan" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->sports_plan) && ($studData->sports_plan == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->sports_plan) && ($studData->sports_plan == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="splan">Sports Plan</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">                                    
		                                    <select class="form-control m-bot15" id="uvaccination" name="uvaccination" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->under_vaccination) && ($studData->under_vaccination == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->under_vaccination) && ($studData->under_vaccination == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="uvaccination">Under Vaccination</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">                                    
		                                    <select class="form-control m-bot15" id="alergitic" name="alergitic" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->alergitic) && ($studData->alergitic == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->alergitic) && ($studData->alergitic == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="alergitic">Alergitic</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">                                    
		                                    <select class="form-control m-bot15" id="asthma" name="asthma" required>
		                                        <option value=''>Select Yes/No</option>    
		                                        <option value='0' <?php echo (isset($studData->asthma) && ($studData->asthma == 0))? 'selected':'';?>>No</option>
		                                        <option value='1' <?php echo (isset($studData->asthma) && ($studData->asthma == 1))? 'selected':'';?>>Yes</option>
											</select>
											</div>
											<label class="form-label" for="asthma">Asthma</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="insuranceid" name="insuranceid" value="<?=(isset($studData->insurance_id))?$studData->insurance_id:'';?>" required>
								    		</div>
											<label class="form-label" for="insuranceid">Insurance Id</label>
										</div>
										<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="insurancename" name="insurancename" value="<?=(isset($studData->insurance_name))?$studData->insurance_name:'';?>" required>
								    		</div>
											<label class="form-label" for="insurancename">Insurance Name</label>
										</div>
											
								  		<div class="clearfix"></div>
								  		<input type="submit" value="Submit" id="submit">				    	
							      	</form>
                                </div>
								<!-- <div class="col-xs-12">
                                  <h3>Upload Student Details</h3>
								  <a href='<?=base_url('students/studentUploadTemplate')?>'>Download Template</a>
								  <form action="<?=base_url('students/saveUploadedStud')?>" method='POST' enctype='multipart/form-data'>
								  <div class="form-group col-lg-6">
								  <label for="stdFileUpload">Upload your file</label>
								  <input type='file' name='stdFileUpload' id='stdFileUpload' class="form-control" required/>
								  </div>
								  <div class="clearfix"></div>
								  <input type="submit" value="Submit">				    	
								  </form>
								</div> -->
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
        </div>
    </div>  
</section>                    
<script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
<script type="text/javascript">
function fileValidation(){
    var fileInput = document.getElementById('studImg');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'" style="width:100px;height:auto;"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>
<?php $this->load->view('templates/footer'); ?>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/js/form-validation.js')?>'></script>
<script type="text/javascript">
var stdDOB = "<?=(isset($studData->dob) && $studData->dob != '') ? $studData->dob :date('Y-m-d');?>";
var stdImg = "<?=($studImgUrl != '') ? $studImgUrl:'' ?>";

$(document).ready(function(){
	$('#dob').datepicker({
		format: 'yyyy-mm-dd',
		endDate: '<?=date('Y-m-d')?>',
	});                
$('#dob').datepicker('setDate',stdDOB);

if(stdImg !== ''){
  	$("#imagePreview").html('<img src="'+stdImg+'" style="width:100px;height:auto;" >');
}

$("form[name='student_data_feed']").validate({
	rules: {
      roll_no: "required",
	  studentname: "required",
	  standard: "required",
	  section: "required",
	  dob: "required",
	  age:{ 
		  required: true,
		  number:true
		},
	  sex: "required",
	  bgroup: "required",
	  fname: "required",
	  mname: "required",
	  foccupation: "required",
	  mobileno:{ 
		  required: true,
		  //number:true,
		  minlength:10,
		  maxlength:13
		},
      email: {
        required: true,
        email: true
	  }
	},
	  messages: {
		roll_no: "Please enter your roll no",
		studentname: "Please enter your student name",
		mobileno: {
        required: "Please provide a mobile No",
        minlength: "Your password must be at least 10 numeric long"
      },
      email: "Please enter a valid email address"
    },
 
	  
	  submitHandler: function(form) {
      form.submit();
	}
});
 
$('#dob').on('changeDate', function(e){
	var  dateDiff= Date.now() - e.date.getTime();
	var ageDate = new Date(dateDiff);
	var age = Math.abs(ageDate.getUTCFullYear() - 1970);
	$('#age').val(age);
});


});
</script>
<style type="text/css">
	.datepicker{
		z-index: 9999 !important;
	}
</style>
