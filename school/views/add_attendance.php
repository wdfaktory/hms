<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row" style=''>

        <div class="clearfix"></div>
        <!-- MAIN CONTENT AREA STARTS -->
        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Add Attendance</h2>                            
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="attendanceForm" method="POST" action="<?=base_url()?>attendance/save_attendance">
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="category">Category</label>
                                    <div class="controls">
                                        <select class="form-control" name="category" id="category">
                                            <option value="-1">Select category</option>
                                            <option value="kindergarten">Kinder Garten</option>
                                            <option value="primary">Primary</option>
                                            <option value="middle">Middle</option>
                                            <option value="secondary">Secondary</option>
                                            <option value="highersecondary">Higher Secondary</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="standard">Standard</label>
                                    <div class="controls">
                                        <select class="form-control" name="standard" id="standard">
                                            <option value="">Select Standard</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="section">Section</label>
                                    <div class="controls">
                                        <select class="form-control" name="section" id="section">
                                            <option value="-1">Select Section</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="studentName">Student Name</label>
                                    <div class="controls">
                                        <select class="form-control" name="studentName" id="studentName" required>
                                            <option value="">Select Student</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="workingDays">No of working days</label>
                                    <div class="controls">
                                        <input type="text" placeholder="No of working days" class="form-control" name="workingDays" id="workingDays" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="sickLeave">No of sick leave</label>
                                    <div class="controls">
                                        <input type="text" placeholder="No of sick leave" class="form-control" name="sickLeave" id="sickLeave" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="controls">
                                        <input type="submit" value="Add" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <div>
</section>

<?php $this->load->view('templates/footer'); ?>
<script>
var BASE_URL = "<?= base_url()?>";

$('#attendanceForm').validate({
    rules: {
        studentName: "required",
        workingDays: "required",
        sickLeave: "required",
    }
});

var sections = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
for (let index = 0; index < sections.length; index++) {
    $('#section').append(
        $('<option>')
        .attr('value',sections[index])
        .text(sections[index])
    );
}
$('#category').change(function(){
    $('#section').val('-1');
    $('#studentName').val('');
    switch ($(this).val()) {
        case 'kindergarten':
            $('#standard .optstd').remove();
            var kg = ['PKG','LKG','UKG'];
            for (let index = 0; index < kg.length; index++) {
                $('#standard').append(
                    $('<option class="optstd">')
                    .attr('value',kg[index])
                    .text(kg[index]));
            }
            break;
        case 'primary':
            $('#standard .optstd').remove();
            for (let index = 0; index < 5; index++) {
                $('#standard').append(
                    $('<option class="optstd">')
                    .attr('value',index+1)
                    .text(index+1));
            }
            break;
        case 'middle':
            $('#standard .optstd').remove();
            for (let index = 5; index < 8; index++) {
                $('#standard').append(
                    $('<option class="optstd">')
                    .attr('value',index+1)
                    .text(index+1));
            }
            break;
        case 'secondary':
            $('#standard .optstd').remove();
            for (let index = 8; index < 10; index++) {
                $('#standard').append(
                    $('<option class="optstd">')
                    .attr('value',index+1)
                    .text(index+1));
            }
            break;
        case 'highersecondary':
            $('#standard .optstd').remove();
            for (let index = 10; index < 12; index++) {
                $('#standard').append(
                    $('<option class="optstd">')
                    .attr('value',index+1)
                    .text(index+1));
            }
            break;
        default:
            $('#standard .optstd').remove();
            break;
    }
});

$('#standard').change(function(){    
    $('#section').val('-1');
    $('#studentName').val('');
});

$('#section').change(function(){    
    $.ajax({
        method: 'POST',
        url: BASE_URL+'/attendance/get_students_name', 
        data: {standard: $('#standard').val(),section: $(this).val()},
        dataType: 'json'
    })
    .done(function(data){
        $('#studentName .optstudent').remove();
        if(data.length){
            for (let index = 0; index < data.length; index++) {
                $('#studentName').append(
                    $('<option class="optstudent">')
                        .attr('value',data[index].id)
                        .text(data[index].student_name)
                );   
            }
        }
        //     $('#studentName').prop('disabled',false);
        // }
        // else{
        //     $('#studentName').prop('disabled','disabled');
        // }
    })
    .fail(function(jqxhr, textStatus, errorThrown){

    });
    // alert($(this).val());
});

</script>