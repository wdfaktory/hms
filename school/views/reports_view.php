<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="col-lg-12">
                    <section class="box">
                        <header class="panel_header">
                            <h2 class="title pull-left">Reports</h2>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Student</th>
                                                    <th>Standard</th>
                                                    <th>Section</th>
                                                    <th>Status</th>
                                                    <th>Report Type</th>
                                                    <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($reports as $report){ ?>
                                                <tr>
                                                    <td>
                                                        <div class="round"><?=substr($report['student_name'],0,1)?></div>
                                                        <div class="designer-info">
                                                            <h6><?=ucfirst($report['student_name'])?></h6>
                                                            <small class="text-muted"><?=($report['sex'] == 1)?'Male':'Female'?>, <?=$report['age']?> Years</small>
                                                        </div>
                                                    </td>
                                                    <td><?=$report['standard']?></td>
                                                    <td><?=$report['section']?></td>
                                                    <td><?=($report['status']==1)?'<span class="badge round-danger">Pending</span>':''?></td>
                                                    <td><span class="badge round-primary"><?=($report['type']==1)?'Sick Report':''?></span></td>
                                                    <td><span class="badge badge-primary"><a href="<?=base_url('view-report/'.$report["r_mongo_id"])?>" style="color:#fff">View</a></span></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    </div>
                                </div>
                            </div>
                    </section>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
    </div>

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-analytics.js"></script> -->
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-auth.js"></script>
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-firestore.js"></script> -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase.js"></script>
    <script type="text/javascript">
        let userId = 0;

        /* Live Config*/
        /*var firebaseConfig = {
          apiKey: "AIzaSyDZ7K1eKwpV7fAWxzwtsS7Z7vX7JndEyU0",
          authDomain: "infocare-1bc19.firebaseapp.com",
          databaseURL: "https://infocare-1bc19.firebaseio.com",
          projectId: "infocare-1bc19",
          storageBucket: "infocare-1bc19.appspot.com",
          messagingSenderId: "1098835201039",
          appId: "1:1098835201039:web:28bb5e27bd7978274de77a",
          measurementId: "G-QLYP135T90"
        };*/

        /* Live Config*/

        /* Dev Config */
        const firebaseConfig = {
          apiKey: "AIzaSyCIlR2mg6EESWAY8342g9DLgrA7MkM0UDM",
          authDomain: "infocare-dev.firebaseapp.com",
          databaseURL: "https://infocare-dev.firebaseio.com",
          projectId: "infocare-dev",
          storageBucket: "infocare-dev.appspot.com",
          messagingSenderId: "518631793496",
          appId: "1:518631793496:web:197ace684472dfd8247b2d",
          measurementId: "G-SNM6GX9HBN"
        };
        /* Dev Config */
        firebase.initializeApp(firebaseConfig);

        //createUserWithEmailAndPassword
        //signInWithEmailAndPassword
        firebase.auth().signInWithEmailAndPassword("notifications@infocaresolutions.in", "d3d9590e65d35d464dc894547bfe4a82").catch(function(error) {
              var errorCode = error.code;
              var errorMessage = error.message;
              console.log('errorCode',errorCode);
              console.log('errorMessage',errorMessage);
        });

        /* var db = firebase.firestore();*/
        firebase.auth().onAuthStateChanged(function(user) {
          if(user){
            console.log('User',user);
            // Get a reference to the database service
            var database = firebase.database();
            var starCountRef = firebase.database().ref('reports/'); // changes pushed to users
            starCountRef.on('value', function(snapshot) { // Realtime listening function
              console.log(snapshot.val())
            });
             
          }else{
            console.log('Invalid Login');
          }
        });
        

        function writeUserData(data) {
            //firebase.database().ref('reports/' + userId).set(data);
            firebase.database().ref('reports/'+ data.report_id).set(data);
        }
    </script>
    <?php $this->load->view('templates/footer'); ?>