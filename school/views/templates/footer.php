 <!-- END CONTAINER -->
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

    <!-- CORE JS FRAMEWORK - START -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.easing.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/pace/pace.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/viewport/viewportchecker.js"></script>
    <script>
        window.jQuery || document.write('<script src="../assets/js/jquery.min.js"><\/script>');
    </script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <script src="<?=base_url()?>assets/plugins/sparkline-chart/jquery.sparkline.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/chartjs-chart/Chart_2.9.3.min.js"></script>
    <script src="<?=base_url()?>assets/js/chart-chartjs.js"></script>

    <script src="<?=base_url()?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?=base_url()?>assets/js/form-validation.js"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="<?=base_url()?>assets/js/scripts.js"></script>
    <!-- END CORE TEMPLATE JS - END -->
