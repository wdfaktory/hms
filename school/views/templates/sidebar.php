  <!-- START CONTAINER -->
    <div class="page-container row-fluid container-fluid">

        <!-- SIDEBAR - START -->

        <div class="page-sidebar fixedscroll">

            <!-- MAIN MENU - START -->
            <div class="page-sidebar-wrapper" id="main-menu-wrapper">

                <ul class='wraplist'>
                    <li class='menusection'>Main</li>
                    <li class="">
                        <a href="<?=base_url('index.php')?>"  <?php echo (!$this->uri->segment(1)) ? "class='active-side-section'":'';?>>
                            <i class="img">
                                <img style="width: 30px" src="<?=base_url()?>data/hos-dash/icons/dashboard.png" alt="">
                            </i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <?php      
                    $sidedetails = sidebar_details(); 
                    $seg = $this->uri->segment(1);
                    $seg2 = $this->uri->segment(2);                                 
                    foreach($sidedetails['permissions'] as $permis){                    
                        $url = base_url().$permis->slug;
                        if(in_array($permis->id, $sidedetails['userpermissions'])){ ?>
                            <li class="">
                            <a href="<?=$url?>" <?php echo ($permis->slug == $seg && $seg2 == '')? "class='active-side-section'":''; ?>>
                                    <i class="img"><img style="width: 30px" src="<?=base_url()?>data/hos-dash/icons/<?=strtolower($permis->name)?>.png" alt="">
                                    </i>
                                    <span class="title"><?php echo $permis->name ?></span>
                                </a>
                            </li>
                    <?php } }

                    ?>
                    <li class="">
                        <a href="<?=base_url('users/view_profile')?>" <?php echo ($seg2 == 'view_profile')? "class='active-side-section'":''; ?>>
                            <i class="img">
                            <img src="<?=base_url().(user_profile_image($this->session->school_user_id)?'assets/uploads/'.user_profile_image($this->session->school_user_id):'assets/images/user.png')?>" alt="user-image" class="user-image img-circle img-inline">
                            </i>
                            <span class="title">Profile</span>
                        </a>
                    </li>
                    
                    <li class="">
                        <a href="<?=base_url('login/logout')?>">
                            <i class="fa fa-lock"></i> <span class="title">Logout</span>
                        </a>
                    </li>

                </ul>

            </div>
            <!-- MAIN MENU - END -->

        </div>
        <!--  SIDEBAR - END -->