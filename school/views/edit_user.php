<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Edit User</h2>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form id="commentForm" method="POST" action="<?=base_url().'users/update_user'?>">
                                        <div id="pills" class='wizardpills'>
                                            <ul class="form-wizard">
                                                <li><a href="#pills-tab1" data-toggle="tab"><span>Basic</span></a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab"><span>Profile</span></a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab"><span>Address</span></a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab"><span>Settings</span></a></li>
                                            </ul>
                                            <div id="bar" class="progress active">
                                                <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane" id="pills-tab1">
                                                    <h4>Basic Information</h4>
                                                    <br>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Full Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Full Name" class="form-control" name="name" id="txtFullName"  value="<?=$userdata->name ?>" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Email</label>
                                                        <div class="controls">
                                                            <input type="email" placeholder="Email" class="form-control" name="email" id="txtEmail" value="<?=$userdata->email ?>" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Phone</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Phone" class="form-control" name="phone" id="txtPhone" value="<?=$userdata->phone ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-label">Designation</label>
                                                        <select class="form-control m-bot15 designationCls" name="designation">
                                                            <option value=''>Select</option>  
                                                            <?php 
                                                            if($userdata->designation == 100){
                                                                $helathhecked = "selected";
                                                            } else {
                                                                $helathhecked = "";
                                                            }
                                                            ?>
                                                            <option value='100' <?=$helathhecked?>>Health Co-Ordinator</option>      
                                                            <?php 
                                                            foreach($designations as $designation){
                                                                if($designation->id == $userdata->designation){
                                                                    $selected = "selected";
                                                                } else {
                                                                    $selected = "";
                                                                }
                                                                ?>
                                                                <option value='<?=$designation->id?>' <?=$selected?>><?=$designation->designation?></option>
                                                            <?php }
                                                            ?>                                                            
                                                        </select>
                                                    </div>
                                                    <div class="form-group standardcls" style="display:none;">
                                                        <label class="form-label">Standard</label>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="1">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">1st Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="2">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">2nd Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="3">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">3rd Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="4">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">4th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="5">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">5th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="6">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">6th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="7">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">7th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="8">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">8th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="9">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">9th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="10">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">10th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="11">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">11th Std</label>
                                                            </li>
                                                            <li>
                                                                <input tabindex="5" type="checkbox"  class="icheck-minimal-red stand_common" name="standard[]" value="12">
                                                                <label class="icheck-label form-label" for="minimal-checkbox-1">12th Std</label>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="form-group col-lg-12">
                                                        <label class="form-label">Permissions</label>
                                                        <label id='perCheck'></label>
                                                        <ul class="list-unstyled permission_ul">
                                                            <?php foreach($permissions as $permission){ 
                                                                $explodeper = explode(',',$userdata->permission);                
                                                                if(in_array($permission->id,$explodeper)){
                                                                    $checked = "checked";
                                                                } else {
                                                                    $checked = "";
                                                                }
                                                                ?>
                                                                <li>
                                                                    <!-- <input tabindex="5" type="checkbox" id="minimal-checkbox-1" class="icheck-minimal-red" name="permission[]" value="<?=$permission->id?>" <?=$checked?> required> -->
                                                                    <input type='hidden' name="permission[]" value="<?=$permission->id?>" >
                                                                    <label class="icheck-label form-label" for="minimal-checkbox-1"><?=$permission->name?></label>
                                                                    <div class="check">
                                                                    <?php 
                                                                    $curdpermissions = $this->config->item('curdPermissions');
                                                                    $curdeach = curd_permission($userdata->id,$permission->id);
                                                                    foreach($curdpermissions as $key => $curd){       
                                                                        $explodecurd = (isset($curdeach->curd))?explode(',',$curdeach->curd) : array();
                                                                        if(!empty($explodecurd)){
                                                                            if(in_array($key,$explodecurd)){
                                                                                if($checked == "checked"){
                                                                                    $checked_curd = "checked";    
                                                                                } else {
                                                                                    $checked_curd = "";    
                                                                                }                  
                                                                            } else {
                                                                                $checked_curd = "";
                                                                            }
                                                                        } else $checked_curd = "";
                                                                        ?>
                                                                        <input tabindex="5" type="checkbox" id="minimal-checkbox-1" class="icheck-minimal-red curd_common curd_access_<?=$permission->id?>" name="curd[<?=$permission->id?>][]" value="<?=$key?>" <?=$checked_curd ?> >
                                                                        <label class="icheck-label form-label" for="minimal-checkbox-1"><?=$curd?></label>
                                                                    <?php }
                                                                    ?>
                                                                    </div>
                                                                </li>    
                                                            <?php } ?>                                                    
                                                        </ul>
                                                    </div>
                                                    <div class="form-group standardpermission" style="display:none;">
                                                        <label class="form-label">Standard Permissions</label>
                                                        <ul class="list-unstyled permission_ul">
                                                            <li>  
                                                                <?php 
                                                                $stdPermissions = $this->config->item('stdPermissions');
                                                                $standeach = healthstd($userdata->id);
                                                                foreach($stdPermissions as $key => $curd){ 
                                                                    $explodestd = explode(',',$standeach->standard); 
                                                                        if(in_array($key,$explodestd)){
                                                                            if($checked == "checked"){
                                                                                $checked_curd = "checked";    
                                                                            } else {
                                                                                $checked_curd = "";    
                                                                            }                  
                                                                        } else {
                                                                            $checked_curd = "";
                                                                        }
                                                                    ?>
                                                                    <input tabindex="5" type="checkbox"  class="icheck-minimal-red std_perms" name="stdperm[]" <?=$checked_curd ?> value="<?=$key?>">
                                                                    <label class="icheck-label form-label" for="minimal-checkbox-1"><?=$curd?></label>
                                                                <?php }
                                                                ?>
                                                            </li>    
                                                        </ul>
                                                    </div>
                                                    <input type="hidden" value="4" name="user_type">
                                                    <input type="hidden" value="<?=$userdata->id?>" name="user_id">
                                                </div>
                                                

                                                <div class="clearfix"></div>
                                                <input type="submit" value="Submit">
                                                 <!-- <ul class="pager wizard">                                                    
                                                    <li class="next"><a href="javascript:;">Next</a></li>
                                                </ul>  -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        

        
    </div>
   <?php $this->load->view('templates/footer'); ?>
   <script>
       $(document).ready(function(){ 
            $('.curd_common').click(function(){
                var currentvalue = $(this).val();
                if(currentvalue == 1){     
                    /*if($(this).prop("checked") == true){
                        var prevvalue = $(this).prev('label').prev('input').val(); 
                        $('.curd_access_'+prevvalue).attr('checked',true);
                    }
                    else if($(this).prop("checked") == false){
                        var prevvalue = $(this).prev('label').prev('input').val();                                    
                        $('.curd_common').attr('checked',false);
                    } */
                    $(this).siblings(":not('.permission_cls')").prop('checked', $(this).is(':checked'));                   
                }
                
            }); 
            var designation = "<?=$userdata->designation ?>";
            if(designation == 100){
                $('.standardpermission').show();
                $('.std_perms').attr('required','required');
            } else {
                $('.standardpermission').hide();
                $('.std_perms').removeAttr('required');
            }

            $('.designationCls').change(function(){
                var curvalue = this.value;
                if(curvalue == 100){
                    $('.standardpermission').show();
                    $('.std_perms').attr('required','required');
                } else {
                    $('.standardpermission').hide();
                    $('.std_perms').removeAttr('required');
                }            
            });
            $("form").submit(function (){
                var n = $("input:checked").length;    
                if(n == 0) {
                    $("#perCheck").text('Please select at least one permission below').css('color','red');
                    return false;
                } else {
                    /* $('input[type=checkbox]').each(function () {
                        if (!this.checked) {
                           console.log($(this).val()); 
                        }
                    }); */

                    return true;
                }

            });
       });
   </script>