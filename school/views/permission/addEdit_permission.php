<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">
                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">Permission Management</h1>
                            <!-- PAGE HEADING TAG - END -->
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->

                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add Permission</h2>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    <form id="commentForm" method="POST" action="<?=base_url().'permissions/insert_permission/'.(isset($permissionData->id)?$permissionData->id:0)?>">

                                        <div id="pills" class='wizardpills'>
                                            <ul class="form-wizard">
                                                <li><a href="#pills-tab1" data-toggle="tab"><span>Basic Permission</span></a></li>
                                                
                                            </ul>
                                            <div id="bar" class="progress active">
                                                <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <input type='hidden' name='permissionId' id='permissionData' value="<?=(isset($permissionData->id)?$permissionData->id:0)?>" />
                                            <div class="tab-content">
                                                <div class="tab-pane" id="pills-tab1">

                                                    <h4>Basic Information</h4>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="form-label">Permission Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="Permission Name" class="form-control" name="permission_name" id="permission_name" value="<?=(isset($permissionData->name)?$permissionData->name:'')?>"  required>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="submit" value="Submit">                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        
    </div>
   <?php $this->load->view('templates/footer'); ?>