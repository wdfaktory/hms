<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>

        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="col-lg-12">
                    <section class="box nobox marginBottom0">
                        <div class="content-body">
                            <div class="row">
                               
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/student_group.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                        <h3 class="mb-5"><?=$studCount?></h3>
                                            <span>Total Students</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box healthy_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/health.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5"><?=$healthyStdCount?></h3>
                                            <span>Healthy Students </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box sick_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/sick.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5"><?=$sickStdCount?></h3>
                                            <span>Sick Students</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box bad_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/treatment.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5"><?=$badHealthStdCount?></h3>
                                            <span>Bad Health Students</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>assets/images/parent.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                        <h3 class="mb-5"><?=$parentCount?></h3>
                                            <span>Total Parents</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box healthy_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>assets/images/business.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5">2800</h3>
                                            <span>Paid </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box sick_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>assets/images/pending.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5">120</h3>
                                            <span>Pending</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box bad_cond">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>assets/images/blocked.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5">80</h3>
                                            <span>Blocked</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/disabled.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                        <h3 class="mb-5"><?=$splCareCount?></h3>
                                            <span>Total Special Care Students </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12">
                                    <div class="r4_counter db_box">
                                        <i class="pull-left ico-icon icon-md icon-primary mt-10">
                                            <img src="<?=base_url()?>data/hos-dash/icons/student_group.png" class="ico-icon-o" alt="">
                                        </i>
                                        <div class="stats">
                                            <h3 class="mb-5">2000</h3>
                                            <span>Total Sports Kids </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-xs-12 col-lg-6">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Total Reports Statistics - Branches</h2>
                            <!-- <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div> -->
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="charty-wrapper">
                                        <canvas id="line-chartjs-1" height="350" width="600"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-xs-12  col-lg-6">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Total Reports Statistics - Students</h2>
                            <!-- <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div> -->
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="charty-wrapper">
                                        <canvas id="line-chartjs-2" height="350" width="600"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-xs-12 col-lg-6">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Total Reports Statistics - Special care</h2>
                            <!-- <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div> -->
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="charty-wrapper">
                                        <canvas id="line-chartjs-3" height="350" width="600"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-xs-12 col-lg-6">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Total Reports Statistics - Sports kids</h2>
                            <!-- <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div> -->
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="charty-wrapper">
                                        <canvas id="line-chartjs-4" height="350" width="600"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- <div class="col-lg-4">
                    <section class="box ">
                        <div class="content-body p">
                            <div class="row">
                                <div class="doctors-list relative">
                                    <div class="doctors-head text-center">
                                        <h3 class="header w-text relative bold">Healthy Schools</h3>
                                        <p class="desc g-text relative">Lorem ipsum dolor sit amet, Earum nes ciunt fugiat enim. Sequi quos labore.</p>
                                    </div>
                                    
                                    <div class="doctor-card has-shadow">
                                        <div class="doc-info-wrap">
                                            <div class="doctor-img">
                                                <img src="<?=base_url()?>data/hos-dash/doc1.jpg" alt="">
                                            </div>
                                            <div class="doc-info">
                                                <h4 class="bold">School 1</h4>
                                                <h5>Over all health</h5>
                                                <div class="doc-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <span>4.8</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="doctor-card has-shadow mb-0">
                                        <div class="doc-info-wrap">
                                            <div class="doctor-img">
                                                <img src="<?=base_url()?>data/hos-dash/doc2.jpg" alt="">
                                            </div>
                                            <div class="doc-info">
                                                <h4 class="bold">School 2</h4>
                                                <h5>Over all health</h5>
                                                <div class="doc-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half"></i>
                                                    <span>4.5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group no-mb">
                                        <button type="button" class="btn btn-primary btn-lg mt-20 gradient-blue" style="width:100%"> View all schools</button>
                                    </div>

                                </div>
                               
                            </div>
                        </div>
                    </section>
                </div> -->

                <div class="clearfix"></div>

                <!-- <div class="col-xs-12 col-md-6 col-lg-4">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Recent Visits</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="project-activity list-unstyled mb-0">
                                        <li class="activity-list warning">
                                            <div class="detail-info">
                                                <div class="doc-img-con pull-left mr-10">
                                                    <img src="<?=base_url()?>data/hos-dash/doc1.jpg" width="80" alt="">
                                                </div>
                                                <div class="visit-doc">
                                                    <p class="message">
                                                        <span class="text-info bold">Dr : </span> Mutten Sultan
                                                    </p>
                                                    <small class="text-muted">
                                                        Blood Check
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0">25 Nov</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix"></li>
                                        <li class="activity-list info">
                                            <div class="detail-info">
                                                <div class="doc-img-con pull-left mr-10">
                                                    <img src="<?=base_url()?>data/hos-dash/doc2.jpg" width="80" alt="">
                                                </div>
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                        <span class="text-info bold">Dr : </span> Smith Watson
                                                    </p>
                                                    <small class="text-muted">
                                                        Thryoid Test
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0">25 Nov</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list success">
                                            <div class="detail-info">
                                                <div class="doc-img-con pull-left mr-10">
                                                    <img src="<?=base_url()?>data/hos-dash/doc3.jpg" width="80" alt="">
                                                </div>
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                        <span class="text-info bold">Dr : </span> Sarah Mutten
                                                    </p>
                                                    <small class="text-muted">
                                                        Full Blood image
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0">25 Nov</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list danger">
                                            <div class="detail-info pb0">
                                                <div class="doc-img-con pull-left mr-10">
                                                    <img src="<?=base_url()?>data/hos-dash/doc1.jpg" width="80" alt="">
                                                </div>
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                        <span class="text-info bold">Dr : </span> Morese Sharpe
                                                    </p>
                                                    <small class="text-muted">
                                                        Full Body Test
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0">25 Nov</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>      
                            </div>
                        </div>
                    </section>
                </div> -->

                <!-- <div class="col-xs-12 col-md-6 col-lg-4">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Operation Success Rate</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-xs-12">
                                      <div class="chart-container">
                                          <div class="" style="height:200px" id="user_type"></div>
                                      </div>
                                </div>      
                            </div> 
                        </div>
                    </section>
                </div> -->



            <!-- <div class="col-xs-12 col-md-6 col-lg-4">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Pharmacy Orders</h2>
                        <div class="actions panel_actions pull-right">
                            <a class="box_toggle fa fa-chevron-down"></a>
                            <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                            <a class="box_close fa fa-times"></a>
                        </div>
                    </header>
                    <div class="content-body">    
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="chart-container">
                                    <div class="" style="height:200px" id="browser_type"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> -->

            <div class="clearfix"></div>

            <div class="col-lg-12">
                <section class="box">
                    <header class="panel_header">
                        <h2 class="title pull-left">Special care students list</h2>
                        <!-- <div class="actions panel_actions pull-right">
                            <a class="box_toggle fa fa-chevron-down"></a>
                            <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                            <a class="box_close fa fa-times"></a>
                        </div> -->
                    </header>
                    <div class="content-body">
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Student</th>
                                                <th>Standard/Section</th>
                                                <th>School Name</th>
                                                <th>Parents Details</th>
                                                <th>Under Treatment</th>
                                                <th>Ext Support</th>
                                                <th>Feeling</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="round">S</div>
                                                    <div class="designer-info">
                                                        <h6>Sunil Joshi</h6>
                                                        <small class="text-muted">Male, 14 Years</small>
                                                    </div>
                                                </td>
                                                <td>6A</td>
                                                <td>School 1</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="badge round-primary">Bad</span></td>
                                                <td><span class="badge round-primary">View</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="round round-success">V</div>
                                                    <div class="designer-info">
                                                        <h6>Messsy Adam</h6>
                                                        <small class="text-muted">Male, 7 Years</small>
                                                    </div>
                                                </td>
                                                <td>3C</td>
                                                <td>School 10</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="badge badge-primary">Not Good</span></td>
                                                <td><span class="badge round-primary">View</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="round round-warning">J</div>
                                                    <div class="designer-info">
                                                        <h6>John Richards</h6>
                                                        <small class="text-muted">Female, 14 Years</small>
                                                    </div>
                                                </td>
                                                <td>10E</td>
                                                <td>School 14</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="badge round-danger">Critical</span></td>
                                                <td><span class="badge round-primary">View</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="round round-primary">P</div>
                                                    <div class="designer-info">
                                                        <h6>Peter Meggik</h6>
                                                        <small class="text-muted">Male, 9 Years</small>
                                                    </div>
                                                </td>
                                                <td>4B</td>
                                                <td>School 52</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="badge round-warning">Getting well</span></td>
                                                <td><span class="badge round-primary">View</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="round round-danger">T</div>
                                                    <div class="designer-info">
                                                        <h6>Henry Tom</h6>
                                                        <small class="text-muted">Female, 14 Years</small>
                                                    </div>
                                                </td>
                                                <td> 9D</td>
                                                <td> School 24</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="badge badge-primary">Good</span></td>
                                                <td><span class="badge round-primary">View</span></td>
                                            </tr>
                                            

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            <div class="col-xs-12 col-md-6 col-lg-4">
                    <!-- <section class="box">
                        <div class="hos-promo">
                            <div class="promo-info">
                                <h3 class="w-text bold">Get full free <br> medical Check up!</h3>
                                <h4 class="g-text bold">29 Nov .... 20:00 PM</h4>
                            </div>
                        </div>
                    </section> -->
                    <!-- <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Students Notes</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="project-activity list-unstyled mb-0">
                                        <li class="activity-list warning">
                                            <div class="detail-info">
                                                <div class="visit-doc">
                                                    <small class="text-muted">
                                                        I feel better Now :)
                                                    </small>
                                                    <p class="message">
                                                        Meditation
                                                    </p>
                                                    
                                                </div>
                                                <div class="visit-date visit-stat pull-right">
                                                    <p class="mb-0">OPENED</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix"></li>
                                        <li class="activity-list info">
                                            <div class="detail-info">
                                                <div class="visit-doc">
                                                    <small class="text-muted">
                                                        Treatment was good!
                                                    </small>
                                                    <p class="message">
                                                        Thyroid Test
                                                    </p>
                                                </div>
                                                <div class="visit-date visit-stat pull-right">
                                                    <p class="mb-0 uppercase">closed</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list success">
                                            <div class="detail-info">
                                                <div class="visit-doc">
                                                    <small class="text-muted">
                                                        My hair is gone!
                                                    </small>
                                                    <p class="message">
                                                        Unhappy
                                                    </p>
                                                </div>
                                                <div class="visit-date visit-stat pull-right">
                                                    <p class="mb-0 uppercase">OPENED</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list warning">
                                            <div class="detail-info">
                                                <div class="visit-doc">
                                                    <small class="text-muted">
                                                        My hair is gone!
                                                    </small>
                                                    <p class="message">
                                                        Unhappy
                                                    </p>
                                                </div>
                                                <div class="visit-date visit-stat pull-right">
                                                    <p class="mb-0 uppercase">closed</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list danger">
                                            <div class="detail-info pb0">
                                                <div class="visit-doc">
                                                    <small class="text-muted">
                                                        Great Mediacal Care 
                                                    </small>
                                                    <p class="message">
                                                        Join Pain
                                                    </p>
                                                </div>
                                                <div class="visit-date visit-stat pull-right">
                                                    <p class="mb-0 uppercase">OPENED</p>
                                                </div>
                                            </div>
                                        </li>


                                    </ul>
                                </div>      
                            </div> 
                        </div>
                    </section> -->

                </div>

            
                <div class="clearfix"></div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
        <!-- END CONTENT -->
        <div class="page-chatapi hideit">

            <div class="search-bar">
                <input type="text" placeholder="Search" class="form-control">
            </div>

            <div class="chat-wrapper">

                <h4 class="group-head">Favourites</h4>
                <ul class="contact-list">

                    <li class="user-row " id='chat_user_1' data-user-id='1'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-1.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Joge Lucky</a></h4>
                            <span class="status available" data-status="available"> Available</span>
                        </div>
                        <div class="user-status available">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_2' data-user-id='2'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-2.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Folisise Chosiel</a></h4>
                            <span class="status away" data-status="away"> Away</span>
                        </div>
                        <div class="user-status away">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_3' data-user-id='3'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-3.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Aron Gonzalez</a></h4>
                            <span class="status busy" data-status="busy"> Busy</span>
                        </div>
                        <div class="user-status busy">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>

                </ul>

                <h4 class="group-head">More Contacts</h4>
                <ul class="contact-list">

                    <li class="user-row " id='chat_user_4' data-user-id='4'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-4.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Chris Fox</a></h4>
                            <span class="status offline" data-status="offline"> Offline</span>
                        </div>
                        <div class="user-status offline">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_5' data-user-id='5'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-5.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Mogen Polish</a></h4>
                            <span class="status offline" data-status="offline"> Offline</span>
                        </div>
                        <div class="user-status offline">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_6' data-user-id='6'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-1.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Smith Carter</a></h4>
                            <span class="status available" data-status="available"> Available</span>
                        </div>
                        <div class="user-status available">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_7' data-user-id='7'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-2.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Amilia Gozenal</a></h4>
                            <span class="status busy" data-status="busy"> Busy</span>
                        </div>
                        <div class="user-status busy">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_8' data-user-id='8'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-3.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Tahir Jemyship</a></h4>
                            <span class="status away" data-status="away"> Away</span>
                        </div>
                        <div class="user-status away">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_9' data-user-id='9'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-4.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Johanson Wright</a></h4>
                            <span class="status busy" data-status="busy"> Busy</span>
                        </div>
                        <div class="user-status busy">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_10' data-user-id='10'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-5.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Loni Tindall</a></h4>
                            <span class="status away" data-status="away"> Away</span>
                        </div>
                        <div class="user-status away">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_11' data-user-id='11'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-1.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Natcho Herlaey</a></h4>
                            <span class="status idle" data-status="idle"> Idle</span>
                        </div>
                        <div class="user-status idle">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>
                    <li class="user-row " id='chat_user_12' data-user-id='12'>
                        <div class="user-img">
                            <a href="#"><img src="<?=base_url()?>data/profile/avatar-2.png" alt=""></a>
                        </div>
                        <div class="user-info">
                            <h4><a href="#">Shakira Swedan</a></h4>
                            <span class="status idle" data-status="idle"> Idle</span>
                        </div>
                        <div class="user-status idle">
                            <i class="fa fa-circle"></i>
                        </div>
                    </li>

                </ul>
            </div>

        </div>

        <div class="chatapi-windows ">

        </div>
    </div>

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-analytics.js"></script> -->
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-auth.js"></script>
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-firestore.js"></script> -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase.js"></script>
    <script type="text/javascript">
        let userId = 0;

        /* Live Config*/
        /*var firebaseConfig = {
          apiKey: "AIzaSyDZ7K1eKwpV7fAWxzwtsS7Z7vX7JndEyU0",
          authDomain: "infocare-1bc19.firebaseapp.com",
          databaseURL: "https://infocare-1bc19.firebaseio.com",
          projectId: "infocare-1bc19",
          storageBucket: "infocare-1bc19.appspot.com",
          messagingSenderId: "1098835201039",
          appId: "1:1098835201039:web:28bb5e27bd7978274de77a",
          measurementId: "G-QLYP135T90"
        };*/

        /* Live Config*/

        /* Dev Config */
        const firebaseConfig = {
          apiKey: "AIzaSyCIlR2mg6EESWAY8342g9DLgrA7MkM0UDM",
          authDomain: "infocare-dev.firebaseapp.com",
          databaseURL: "https://infocare-dev.firebaseio.com",
          projectId: "infocare-dev",
          storageBucket: "infocare-dev.appspot.com",
          messagingSenderId: "518631793496",
          appId: "1:518631793496:web:197ace684472dfd8247b2d",
          measurementId: "G-SNM6GX9HBN"
        };
        /* Dev Config */
        firebase.initializeApp(firebaseConfig);

        //createUserWithEmailAndPassword
        //signInWithEmailAndPassword
        firebase.auth().signInWithEmailAndPassword("notifications@infocaresolutions.in", "d3d9590e65d35d464dc894547bfe4a82").catch(function(error) {
              var errorCode = error.code;
              var errorMessage = error.message;
              console.log('errorCode',errorCode);
              console.log('errorMessage',errorMessage);
        });

        /* var db = firebase.firestore();*/
        firebase.auth().onAuthStateChanged(function(user) {
          if(user){
            console.log('User',user);
            // Get a reference to the database service
            var database = firebase.database();
            var starCountRef = firebase.database().ref('reports/'); // changes pushed to users
            starCountRef.on('value', function(snapshot) { // Realtime listening function
              console.log(snapshot.val())
            });
             
          }else{
            console.log('Invalid Login');
          }
        });
        

        function writeUserData(data) {
            //firebase.database().ref('reports/' + userId).set(data);
            firebase.database().ref('reports/'+ data.report_id).set(data);
        }
    </script>
    <?php $this->load->view('templates/footer'); ?>

    <script>
/*Bar Chart*/
var randomScalingFactor = function() {
    return Math.round(Math.random() * 100)
};

var config = {
    type: 'line',
    data: {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
        datasets: [{
            label: 'Healthy',
            backgroundColor: 'rgb(34,139,34)',
            borderColor: 'rgb(34,139,34)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: false,
        }, {
            label: 'Sick',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: 'Bad Sick',
            fill: false,
            backgroundColor: 'rgb(255,0,0)',
            borderColor: 'rgb(255,0,0)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Weekly Health Statistics'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total no of branches'
                }
            }]
        }
    }
};
var ctx = document.getElementById('line-chartjs-1').getContext('2d');
window.myLine = new Chart(ctx, config);

var config2 = {
    type: 'line',
    data: {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
        datasets: [{
            label: 'Healthy',
            backgroundColor: 'rgb(34,139,34)',
            borderColor: 'rgb(34,139,34)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: false,
        }, {
            label: 'Sick',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: 'Bad Sick',
            fill: false,
            backgroundColor: 'rgb(255,0,0)',
            borderColor: 'rgb(255,0,0)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Weekly Health Statistics'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total no of students'
                }
            }]
        }
    }
};
var ctx2 = document.getElementById('line-chartjs-2').getContext('2d');
window.myLine2 = new Chart(ctx2, config2);

var config3 = {
    type: 'line',
    data: {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
        datasets: [{
            label: 'Healthy',
            backgroundColor: 'rgb(34,139,34)',
            borderColor: 'rgb(34,139,34)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: false,
        }, {
            label: 'Getting Better',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: 'Worst',
            fill: false,
            backgroundColor: 'rgb(255,0,0)',
            borderColor: 'rgb(255,0,0)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Weekly Health Statistics'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total no of special care students'
                }
            }]
        }
    }
};
var ctx3 = document.getElementById('line-chartjs-3').getContext('2d');
window.myLine3 = new Chart(ctx3, config3);

var config4 = {
    type: 'line',
    data: {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
        datasets: [{
            label: 'Healthy Schools',
            backgroundColor: 'rgb(34,139,34)',
            borderColor: 'rgb(34,139,34)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: false,
        }, {
            label: 'Sick Schools',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: 'Bad Sick',
            fill: false,
            backgroundColor: 'rgb(255,0,0)',
            borderColor: 'rgb(255,0,0)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Weekly Health Statistics'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total no of sports kids'
                }
            }]
        }
    }
};
var ctx4 = document.getElementById('line-chartjs-4').getContext('2d');
window.myLine4 = new Chart(ctx4, config4);


</script>