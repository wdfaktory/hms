<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="col-xs-12 col-md-6">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Sick Report</h2>
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="project-activity list-unstyled mb-0">
                                       <li class="activity-list warning">
                                            <div class="detail-info v2">
                                                <div class="visit-doc">
                                                    <p class="message">
                                                         Student Details
                                                    </p>
                                                    <small class="text-muted">
                                                        <?=date('d-m-Y',$report_data['sick_from']['$date']);?>
                                                    </small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list warning">
                                            <div class="detail-info v2">
                                                <div class="visit-doc">
                                                    <p class="message">
                                                         Sick From
                                                    </p>
                                                    <small class="text-muted">
                                                        <?=date('d-m-Y',$report_data['sick_from']['$date']);?>
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0"><?=date('d M',$report_data['sick_from']['$date']);?></p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix"></li>
                                        <li class="activity-list danger">
                                            <div class="detail-info v2">
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                         Sick upto
                                                    </p>
                                                    <small class="text-muted">
                                                        <?=date('d-m-Y',$report_data['sick_to']['$date']);?>
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0"><?=date('d M',$report_data['sick_to']['$date']);?></p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list info">
                                            <div class="detail-info v2">
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                        Sick Reason
                                                    </p>
                                                    <small class="text-muted">
                                                        <?=$report_data['sick_reason']?>
                                                    </small>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list success">
                                            <div class="detail-info v2">
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                        Submitted Date
                                                    </p>
                                                    <small class="text-muted">
                                                        <?=date('d-m-Y H:i:s',$report_data['created_date']['$date']);?>
                                                    </small>
                                                </div>
                                                <div class="visit-date pull-right">
                                                    <p class="mb-0"><?=date('d M',$report_data['created_date']['$date']);?></p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="activity-list info">
                                            <div class="detail-info v2 pb0">
                                                <div class="visit-doc">
                                                    <p class="message ">
                                                         Status
                                                    </p>
                                                    <small class="text-muted">
                                                        Pending
                                                    </small>
                                                </div>
                                                <button class="btn visit-date btn-danger pull-right">Reject</button>
                                                <button class="btn visit-date btn-success pull-right" style="right: 80px">Accept</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>      
                            </div> <!-- End .row -->
                        </div>
                    </section>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
    </div>

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-analytics.js"></script> -->
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-auth.js"></script>
    <!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-firestore.js"></script> -->
    <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase.js"></script>
    <script type="text/javascript">
        let userId = 0;

        /* Live Config*/
        /*var firebaseConfig = {
          apiKey: "AIzaSyDZ7K1eKwpV7fAWxzwtsS7Z7vX7JndEyU0",
          authDomain: "infocare-1bc19.firebaseapp.com",
          databaseURL: "https://infocare-1bc19.firebaseio.com",
          projectId: "infocare-1bc19",
          storageBucket: "infocare-1bc19.appspot.com",
          messagingSenderId: "1098835201039",
          appId: "1:1098835201039:web:28bb5e27bd7978274de77a",
          measurementId: "G-QLYP135T90"
        };*/

        /* Live Config*/

        /* Dev Config */
        const firebaseConfig = {
          apiKey: "AIzaSyCIlR2mg6EESWAY8342g9DLgrA7MkM0UDM",
          authDomain: "infocare-dev.firebaseapp.com",
          databaseURL: "https://infocare-dev.firebaseio.com",
          projectId: "infocare-dev",
          storageBucket: "infocare-dev.appspot.com",
          messagingSenderId: "518631793496",
          appId: "1:518631793496:web:197ace684472dfd8247b2d",
          measurementId: "G-SNM6GX9HBN"
        };
        /* Dev Config */
        firebase.initializeApp(firebaseConfig);

        //createUserWithEmailAndPassword
        //signInWithEmailAndPassword
        firebase.auth().signInWithEmailAndPassword("notifications@infocaresolutions.in", "d3d9590e65d35d464dc894547bfe4a82").catch(function(error) {
              var errorCode = error.code;
              var errorMessage = error.message;
              console.log('errorCode',errorCode);
              console.log('errorMessage',errorMessage);
        });

        /* var db = firebase.firestore();*/
        firebase.auth().onAuthStateChanged(function(user) {
          if(user){
            console.log('User',user);
            // Get a reference to the database service
            var database = firebase.database();
            var starCountRef = firebase.database().ref('reports/'); // changes pushed to users
            starCountRef.on('value', function(snapshot) { // Realtime listening function
              console.log(snapshot.val())
            });
             
          }else{
            console.log('Invalid Login');
          }
        });
        

        function writeUserData(data) {
            //firebase.database().ref('reports/' + userId).set(data);
            firebase.database().ref('reports/'+ data.report_id).set(data);
        }
    </script>
    <?php $this->load->view('templates/footer'); ?>