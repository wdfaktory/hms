<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" type="text/css">
	
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">	
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<title>Home</title>
</head>
<body>
<div class="login-container">
	<div class="">
		<div class="login-form text-center">
		<p class="logo"><img src="<?=base_url('assets/images/dummylogo.jpg')?>" /></p>
			<h2>Lorem ipsum dolor sit amet</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<form action="<?=base_url('login/check_login')?>" method="POST" class="login">
			  <div class="form-group">
			    <label for="type">Login Category (Select any one)</label><br>
			     <label class="container parents">
			     	<input type="radio" name="type" id="parents">
			     	<span class="checkmark"></span>
			     	<a href="<?=base_url('../user/login')?>"><b>Parents</b></a>
			     </label>
			    <label class="container school">
			    	<input type="radio" name="type" id="school">
			    	<span class="checkmark"></span>
			    	<a href="<?=base_url('login')?>"><b>School</b></a>
			    </label>
			  </div>
			    <div class="form-group">
			    <label for="name">Email</label>
			    <input type="email" class="form-control" name="name" id="name" required>
			  </div>
			    <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" class="form-control" name="password" id="password" required>
			  </div>
		    <div class="form-group">        
		        <div class="rememberMe">
		          <label><input type="checkbox" name="remember"> Remember me</label>
		        </div>
		        <div class="forgetPassword">
				<a href="<?=base_url().'users/open_reset_pwd'?>">Forgot Password</a>
		        </div>
		    </div>
		      <div class="form-group login-btn">        
        			<button type="submit" class="btn btn-primary">Log In</button>
    			</div>
			</form>	
			<p><a href="#">Learn more</a> about <b>Infocare Health Solutions</b></p>
<p style="margin-top: 25px">
				<a href="#" class="socialLink twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href="#" class="socialLink facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href="#" class="socialLink insta" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="#" class="socialLink pin" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
			</p>			
		</div>
	</div>
</div>



</body>
  
</html>