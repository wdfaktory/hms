<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>

        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box " style="width: 30%; margin-left: 30%;">
                        <header class="panel_header">
                            <h2 class="title pull-left"><b>Reset password</b></h2>                            
                        </header>
						<div class="alert alert-success" id="sent_ok" style="display:none;">
						  <span>
							<p >Password Reset Successfully!</p>
						  </span>
						</div>
						
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
								<?php if($this->uri->segment(3) != ''){?>
									<input type="hidden" value="<?php echo $this->uri->segment(3); ?>" id="user_id" >
								<?php }else{?>
									<input type="hidden" value="<?php echo $this->session->school_user_id; ?>" id="user_id" >
								<?php } ?>
									
								<form id="commentForm" method="POST" action="<?=base_url('/users/update_pwd')?>">
										<div class="container">
                                            
											<div class="form-group">
											   
												<div class="controls"> <label class="form-label"><b>Password :</b></label>
												 <input type="password" placeholder="New Password" class="form-control" style="width: 20%;" name="password" id="password" value=""  required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
												 <label class ='pwd-info' style='width:200px;'></label>
												   
												</div>
												<div class="controls"> <label class="form-label"><b>Confirm Password :</b></label>
												 <input type="password" placeholder="Confirm Password" class="form-control" style="width: 20%;" name="con_password" id="con_password" value=""  required>
												 <label class ='pwd-info1' style='width:200px;'></label> 
												</div>
											</div>
											<div class="clearfix"></div>
											<button type="submit" id="click_submit" style="margin-left: 74px;" class="btn btn-success">Submit</button>     
											<?php if($this->uri->segment(3) == ''){?>  
												<a href="<?=base_url('users/view_profile')?>" class="btn btn-primary">Cancel</a>
											
											<?php }else{?>
												<a id="login_id_pwd" style="display:none;" href="<?=base_url('login')?>" class="btn btn-primary">Login</a>
											<?php } ?>											
										</div>
                                       
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>      

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
//var pattern = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;
var pattern = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+-]).*$/;
$('#password').blur(function(){
	let password = $('#password').val();
	if(pattern.test(password)){
		$(".pwd-info").text("Strong").css('color','green');
	}
	else{
		$(".pwd-info").text('At least one uppercase, numeric, special character').css('color','red');
		$('#password').focus();
	}
});
$('#con_password').blur(function(){
	let password = $('#password').val();
	let con_password = $('#con_password').val();
	if(password != ''){
		if(con_password != password)
			$(".pwd-info1").text('Not matched with Password').css('color','red');
	}
	else{
		$(".pwd-info").text('At least one uppercase, numeric, special character').css('color','red');
		$('#password').focus();
	}
});

$("form").submit(function(){
	
	
	var BASE_URL = "<?php echo base_url();?>";
	var user_id = $('#user_id').val();
	var password = $('#password').val();
	var con_password = $('#con_password').val();
	
	
	if(password == ''){
		alert('Please enter password'); return false;
	}
	if(con_password == ''){
		alert('Please enter confirm password'); return false;
	}

	if(pattern.test(password) == false){
		alert('At least one uppercase, one numeric and one special character'); return false;
	}
	
	if(password != con_password){
		alert('Password and Confirm Password to be same'); return false;
	}
	
	
	/* $.ajax({
		url: BASE_URL+'/users/update_pwd',
		dataType : 'json',
		type: "post",
		data: {'user_id': user_id, 'password': password},
		success: function (response) {
			console.log(response);
			if(response.success == 1){
				$("#click_submit").attr("disabled", true);
				
				$('#sent_ok').show();
				$('#login_id_pwd').show();
			}
		}
	}); */
});
</script>		

   <?php $this->load->view('templates/footer'); ?>
