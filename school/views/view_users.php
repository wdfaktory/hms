<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row">
        <div class="col-lg-12">
            <section class="box">
                <header class="panel_header">
                    <h2 class="title pull-left">View Users<br/><span style="font-size: 14px;padding-top:0px">(Add your employees and grant access permissions)</span></h2>                                
                </header>
                <?php /*$curdPer = curd_access();             
                echo '<pre>';
                print_r($curdPer); die;*/
                ?>
                <a href="<?=base_url().'users/add_user'?>">Add User</a>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Designation</th>
                                            <th>Permissions</th>
                                            <th>Standard Permissions</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    foreach($user_details as $users){?>
                                        <tr <?php if($users->deleted==1) echo 'style="opacity:0.4"'?>>
                                            <td>
                                                <h6><?=$users->name?></h6>
                                            </td>
                                            <td><?=$users->email?></td>
                                            <td><?=$users->phone?></td>
                                            <td><?php 
                                            if($users->designation != 100){
                                                $schooldesig = school_designation($users->designation);
                                                echo isset($schooldesig->designation)?$schooldesig->designation:'NA';
                                            } else {                                                
                                                echo 'Health Co-Ordinator';
                                            }
                                            
                                            ?></td>
                                            <td><?php 
                                            $permissions = permssion_details($users->permission);
                                            echo $permissions;
                                            ?></td>
                                            <td><?php 
                                            $stdpermissions = health_standard($users->id,$users->school_id);
                                            echo $stdpermissions;
                                            ?></td>
                                            <td>
                                            <?php if($users->deleted == 0){ ?>
                                                <a href="<?=base_url().'users/edit_user/'.$users->id ?>">Edit</a>
                                                <a href="<?=base_url().'users/delete_user/'.$users->id ?>" onclick="return confirm('Are you sure you want to disable?')">Disable</a>
                                            <?php } else {?>
                                                <a href="<?=base_url().'users/delete_user/'.$users->id.'/0' ?>" onclick="return confirm('Are you sure you want to enable?')">Enable</a>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>  
</section>                    
<?php $this->load->view('templates/footer'); ?>