<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
        <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">
                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">User Management</h1>
                            <!-- PAGE HEADING TAG - END -->
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->

                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add Schools</h2>                            
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    <form id="commentForm" method="POST" action="<?=base_url().'users/add_school/'.(isset($schoolData->id)?$schoolData->id:0)?>">

                                        <div id="pills" class='wizardpills'>
                                            <ul class="form-wizard">
                                                <li><a href="#pills-tab1" data-toggle="tab"><span>Basic</span></a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab"><span>Profile</span></a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab"><span>Address</span></a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab"><span>Settings</span></a></li>
                                            </ul>
                                            <div id="bar" class="progress active">
                                                <div class="progress-bar progress-bar-primary " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <input type='hidden' name='schoolId' id='schoolId' value="<?=(isset($schoolData->id)?$schoolData->id:0)?>" />
                                            <div class="tab-content">
                                                <div class="tab-pane" id="pills-tab1">

                                                    <h4>Basic Information</h4>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="form-label">School Name</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="School Name" class="form-control" name="schoolname" id="txtSchoolName" value="<?=(isset($schoolData->school_name)?$schoolData->school_name:'')?>"  required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Email</label>
                                                        <div class="controls">
                                                            <input type="email" placeholder="Email" class="form-control" name="email" id="txtEmail"  value="<?=(isset($schoolData->email)?$schoolData->email:'')?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">City</label>
                                                        <div class="controls">
                                                            <input type="text" placeholder="City" class="form-control" name="city" id="txtCity" required value="<?=(isset($schoolData->city)?$schoolData->city:'')?>" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Address</label>
                                                        <textarea minlength="15" class="form-control" name="address" required> <?=(isset($schoolData->address)?$schoolData->address:'')?></textarea>
                                                    </div>                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="submit" value="Submit">                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>        
    </div>
   <?php $this->load->view('templates/footer'); ?>