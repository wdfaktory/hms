<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$studId = $studData->id;
?><!DOCTYPE html>
<link href="<?=base_url('assets/css/common.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/css/main.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css">
<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<style>
.form-group .error {
    color: #f44336 !important;
    font-weight: 400 !important;
    font-size: 13px !important;
}

table.dataTable thead .sorting:after {
    content: none; !important;
}

label.error{
	top: inherit !important;
}

</style>
 <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h3 class="pull-left">&nbsp;  Continence Form: <?=$studData->student_name ?> (Roll No: <?=$studData->roll_no?>)</h3>                            
                        </header>
                        <div class="content-body">
							<?php if($this->session->flashdata('status')):?>
								<div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
							<?php endif; ?>
                            <div class="row">

                                <div class="col-xs-12">
							      	<form action="<?=base_url().'students/updateContinenceData/'.$studId?>" method="POST" class="parent-dtl add-student custom" id="continenceForm" name="continenceForm" enctype='multipart/form-data' >
                                      <div class="form-group col-lg-12">
                                      <h3>General Conditions</h3> 
                                      </div>
							      		<div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="man" name="man" >
										  </div>
										  <label class="form-label" for="man">Medic Alert Number if required</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="lastReview" name="lastReview">
											</div>
											<label class="form-label" for="lastReview">Review date for this form / Last updated date</label>
								  		</div>

                                        <div class="form-group col-lg-12">
                                            <h3>Routine medical care needs and recommended support</h3> 
                                        </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="timeSupport" name="timeSupport" >
										  </div>
										  <label class="form-label" for="timeSupport">Support time needed</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="supportNature" name="supportNature">
											</div>
											<label class="form-label" for="supportNature">Nature of Support</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="catherisation" name="catherisation" >
										  </div>
										  <label class="form-label" for="catherisation">Catherisation</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="continenceSupplies" name="continenceSupplies">
											</div>
											<label class="form-label" for="continenceSupplies">Continence Supplies</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="unPlanedEvent" name="unPlanedEvent" >
										  </div>
										  <label class="form-label" for="unPlanedEvent">Unplanned events</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="catheterMangement" name="catheterMangement">
											</div>
											<label class="form-label" for="catheterMangement">Catheter mangement</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="additionalInfo" name="additionalInfo" >
										  </div>
										  <label class="form-label" for="additionalInfo">Additional Information</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="observationDoc" name="observationDoc">
											</div>
											<label class="form-label" for="observationDoc">Documented observations</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="privacyStatemet" name="privacyStatemet" >
										  </div>
										  <label class="form-label" for="privacyStatemet">Privacy Statement</label>
		                                </div>

                                        <div class="form-group col-lg-12">
                                            <h3>Authorization</h3> 
                                        </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="nameHealthPractioner" name="nameHealthPractioner" >
										  </div>
										  <label class="form-label" for="nameHealthPractioner">Name of the medical health practioner</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="professionalRole" name="professionalRole">
											</div>
											<label class="form-label" for="professionalRole">Professional Role</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="contact" name="contact">
											</div>
											<label class="form-label" for="contact">Contact details</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="nameParent" name="nameParent" >
										  </div>
										  <label class="form-label" for="nameParent">Name of the parents/guaridan</label>
		                                </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="signature" name="signature" >
										  </div>
										  <label class="form-label" for="signature">Signature</label>
		                                </div>
                                        <div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="dateAuthorization" name="dateAuthorization">
											</div>
											<label class="form-label" for="dateAuthorization">Date</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="authPrivacyStatement" name="authPrivacyStatement" >
										  </div>
										  <label class="form-label" for="authPrivacyStatement">Privacy Statement</label>
		                                </div>

								  		<div class="clearfix"></div>
								  		<input type="submit" value="Submit" id="submit">				    	
							      	</form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>
        </div>
    </div>  
</section>                    
<script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
<script type="text/javascript">
function fileValidation(){
    var fileInput = document.getElementById('studImg');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'" style="width:100px;height:auto;"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>
<?php $this->load->view('templates/footer'); ?>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/js/form-validation.js')?>'></script>
<script type="text/javascript">

$(document).ready(function(){
	$('#lastReview').datepicker({
		format: 'yyyy-mm-dd',
		endDate: '<?=date('Y-m-d')?>',
    });
    
    $('#dateAuthorization').datepicker({
		format: 'yyyy-mm-dd',
		endDate: '<?=date('Y-m-d')?>',
	});                
//$('#dateAuthorization','#lastReview').datepicker('setDate',stdDOB);
$("form[name='student_data_feed']").validate({
	rules: {
      roll_no: "required",
	  studentname: "required",
	  standard: "required",
	  section: "required",
	  dob: "required",
	  age:{ 
		  required: true,
		  number:true
		},
	  sex: "required",
	  bgroup: "required",
	  fname: "required",
	  mname: "required",
	  foccupation: "required",
	  mobileno:{ 
		  required: true,
		  //number:true,
		  minlength:10,
		  maxlength:13
		},
      email: {
        required: true,
        email: true
	  }
	},
	  messages: {
		roll_no: "Please enter your roll no",
		studentname: "Please enter your student name",
		mobileno: {
        required: "Please provide a mobile No",
        minlength: "Your password must be at least 10 numeric long"
      },
      email: "Please enter a valid email address"
    },
 
	  
	  submitHandler: function(form) {
      form.submit();
	}
});
 

});
</script>
<style type="text/css">
	.datepicker{
		z-index: 9999 !important;
	}
</style>
