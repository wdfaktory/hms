<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="wrapper main-wrapper row" style=''>

        <div class="clearfix"></div>
        <!-- MAIN CONTENT AREA STARTS -->
        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Edit Attendance</h2>                            
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="editAttendanceForm" method="POST">
                                <input type="hidden" name="studentId" value="<?= $attendance->stid?>">
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="workingDays">Student Name</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="studentName" id="studentName"  value="<?=$attendance->student_name?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="category">Category</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" name="category" id="category" value="<?=$attendance->category?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="standard">Standard</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="standard" id="standard"  value="<?=$attendance->standard?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="section">Section</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="section" id="section"  value="<?=$attendance->section?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="workingDays">No of working days</label>
                                    <div class="controls">
                                        <input type="text" placeholder="No of working days" class="form-control" name="workingDays" id="workingDays"  value="<?=$attendance->no_of_working_days?>">
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="form-label" for="sickLeave">No of sick leave</label>
                                    <div class="controls">
                                        <input type="text" placeholder="No of sick leave" class="form-control" name="sickLeave" id="sickLeave" value="<?=$attendance->no_of_sick_leave?>">
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="controls">
                                        <input type="submit" class="btn btn-primary" value="submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('templates/footer'); ?>
<script>
var BASE_URL = "<?= base_url()?>";

$('#editAttendanceForm').validate({
    rules: {
        workingDays: "required",
        sickLeave: "required",
    }
});

</script>