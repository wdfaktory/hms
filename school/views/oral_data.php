<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$studId = $studData->id;
?><!DOCTYPE html>
<link href="<?=base_url('assets/css/common.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/css/main.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css">
<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<style>
.form-group .error {
    color: #f44336 !important;
    font-weight: 400 !important;
    font-size: 13px !important;
}

table.dataTable thead .sorting:after {
    content: none; !important;
}

label.error{
	top: inherit !important;
}

.form_wrapper .box{ padding: 10px 20px; }
.form_wrapper .box .form{margin-bottom: 25px;}
.form_wrapper .box .form ul{margin:0px; padding: 5px 0px 15px 40px; }
.form_wrapper .box .form ul li{list-style:disc; margin-bottom: 5px;}
.sub_text{display: block; font-size: 11px; line-height: 16px;}
.authorisation_wr{border: 1px solid #ccc; border-radius: 5px; width: 100%;
clear: both;
padding: 0px; margin-bottom: 10px;}
.authorisation_wr table{width: 100%; margin: 0px 20px 10px;}
.authorisation_wr table tr td{padding: 10px 5px;}

</style>
 <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <div class="wrapper main-wrapper row" style=''>

                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h3 class="pull-left">&nbsp;  Oral Data: <?=$studData->student_name ?> (Roll No: <?=$studData->roll_no?>)</h3>                            
                        </header>
                        <div class="content-body">
							<?php if($this->session->flashdata('status')):?>
								<div class="alert alert-success"><?=$this->session->flashdata('status')?></div>	
							<?php endif; ?>
                            <div class="row">

                                <div class="col-xs-12">
							      	<form action="<?=base_url().'students/updateOralData/'.$studId?>" method="POST" class="parent-dtl add-student custom" id="oralForm" name="oralForm" enctype='multipart/form-data' >
                                      <div class="form-group col-lg-12">
                                      <h3>General Conditions</h3> 
                                      </div>
							      		<div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="man" name="man" >
										  </div>
										  <label class="form-label" for="man">Medic Alert Number if required</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="lastReview" name="lastReview">
											</div>
											<label class="form-label" for="lastReview">Review date for this form / Last updated date</label>
								  		</div>

                                        <div class="form-group col-lg-12">
                                            <h3>Routine medical care needs and recommended support</h3> 
                                        </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="levelSupport" name="levelSupport" >
										  </div>
										  <label class="form-label" for="levelSupport">Level of support required</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="levelSupervision" name="levelSupervision">
											</div>
											<label class="form-label" for="levelSupervision">Level of Supervision</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="timeMeal" name="timeMeal" >
										  </div>
										  <label class="form-label" for="timeMeal">Time required for mealtime</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="supportTypeNeeded" name="supportTypeNeeded">
											</div>
											<label class="form-label" for="supportTypeNeeded">Type of Support needed</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="communication" name="communication" >
										  </div>
										  <label class="form-label" for="man">Communication</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="foodPreparation" name="foodPreparation">
											</div>
											<label class="form-label" for="foodPreparation">Preparation and presentation of food and drink</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="learningTarget" name="learningTarget" >
										  </div>
										  <label class="form-label" for="learningTarget">Potential learning targets</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="observationDoc" name="observationDoc">
											</div>
											<label class="form-label" for="observationDoc">Documented observations</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="safetySupervision" name="safetySupervision" >
										  </div>
										  <label class="form-label" for="safetySupervision">General Supervision for Safety</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="toiletering" name="toiletering">
											</div>
											<label class="form-label" for="toiletering">Toiletering need</label>
								  		</div>


                                          <div class="form-group col-lg-12">
                                            <h3>First Aid</h3> 
                                        </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="firstAidDescription" name="firstAidDescription" >
										  </div>
										  <label class="form-label" for="firstAidDescription">Description</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="observable" name="observable">
											</div>
											<label class="form-label" for="observable">Observable / Sign and Reactions</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="FirstAidResponse" name="FirstAidResponse" >
										  </div>
										  <label class="form-label" for="FirstAidResponse">First aid response</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="privacyStatement" name="privacyStatement">
											</div>
											<label class="form-label" for="privacyStatement">Privacy Statement</label>
								  		</div>
                                          <!-- <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="timeMeal" name="timeMeal" >
										  </div>
										  <label class="form-label" for="timeMeal">Time required for mealtime</label>
		                                </div> -->

                                        <div class="form-group col-lg-12">
                                            <h3>Authorization</h3> 
                                        </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="nameHealthPractioner" name="nameHealthPractioner" >
										  </div>
										  <label class="form-label" for="nameHealthPractioner">Name of the medical health practioner</label>
		                                </div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="professionalRole" name="professionalRole">
											</div>
											<label class="form-label" for="professionalRole">Professional Role</label>
								  		</div>
								    	<div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="contact" name="contact">
											</div>
											<label class="form-label" for="contact">Contact details</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="nameParent" name="nameParent" >
										  </div>
										  <label class="form-label" for="nameParent">Name of the parents/guaridan</label>
		                                </div>
                                        <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="signature" name="signature" >
										  </div>
										  <label class="form-label" for="signature">Signature</label>
		                                </div>
                                        <div class="form-group col-lg-6">
											<div class="controls">
								    		<input type="text" class="form-control" id="dateAuthorization" name="dateAuthorization">
											</div>
											<label class="form-label" for="dateAuthorization">Date</label>
								  		</div>
                                          <div class="form-group col-lg-6">
										<div class="controls">
										  <input type="text" class="form-control" id="authPrivacyStatement" name="authPrivacyStatement" >
										  </div>
										  <label class="form-label" for="authPrivacyStatement">Privacy Statement</label>
		                                </div>

								  		<div class="clearfix"></div>
								  		<input type="submit" value="Submit" id="submit">				    	
							      	</form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
            </div>
        </section>

        </div>
    </div>  
</section>  
<section id="main-content" class="form_wrapper">
	 <div class="wrapper main-wrapper row">	
	 <div class="col-lg-12 ">
	 	<section class="box">	 		
<div class="form col-lg-12">
<h3>Support time needed</h3>
	 		<p>Information is needed about how frequently support is needed and for how long. The
school will endeavour to minimise disruption to the student’s socialization and
participation:</p>
	<div class="col-lg-6">
		<div class="checkbox"><label><input type="checkbox"> Indicates when toilet is needed</label></div>
		<div class="checkbox"><label><input type="checkbox"> May need to be changed</label></div>
		<div class="checkbox"><label><input type="checkbox"> Needs timing support</label></div>
	</div>
	<div class="col-lg-6">
		<div class="checkbox"><label><input type="checkbox"> Will always need to be changed/assisted</label></div>
		<div class="checkbox"><label><input type="checkbox"> Has continence aids (eg wears nappies or has catheter)</label></div>
	</div>
</div>

<div class="form col-lg-12">
<h3>Nature of support</h3>
<p>This student is likely to need support related to:</p>
	<div class="col-lg-6">
		<p><strong>Self-managed toileting</strong> (please describe):</p>
		<div class="checkbox"><label><input type="checkbox"> Reminders</label></div>
		<div class="checkbox"><label><input type="checkbox"> Timing</label></div>
		<div class="checkbox"><label><input type="checkbox"> Encouragement with fluid intake</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><strong>Assisted toileting</strong> (please describe):</p>
		<div class="checkbox"><label><input type="checkbox"> Verbal prompts</label></div>
		<div class="checkbox"><label><input type="checkbox"> Supervision</label></div>
		<div class="checkbox"><label><input type="checkbox"> Encouragement with fluid intake</label></div>
		<div class="checkbox"><label><input type="checkbox"> Assistance with clothing</label></div>
		<div class="checkbox"><label><input type="checkbox"> Support to weight-bear</label></div>
		<div class="checkbox"><label><input type="checkbox"> Lifting onto toilet</label></div>
		<div class="checkbox"><label><input type="checkbox"> Assistance with washing hands</label></div>
		<div class="checkbox"><label><input type="checkbox"> Support for transfer</label></div>
		<div class="checkbox"><label><input type="checkbox"> Assistance with hygiene (eg cleaning body, menstruation management)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><strong>Catheterisation</strong> (please describe):</p>
		<div class="checkbox"><label><input type="checkbox"> Allow for catheterisation at (specify preferred times)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Self-managed</label></div>
		<div class="checkbox"><label><input type="checkbox"> Self-catheterises with supervision</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other (assisted catheterisation by trained school staff )</label></div>
	</div>
</div>

<div class="form col-lg-12">
<h3>Continence Supplies</h3>
	<div class="row">
	<div class="form-group col-lg-6">
		<label>Equipment/continence aids that are required</label> <input type="text" class="form-control " name="">
	</div>
	</div>
	<div class="row">
	<div class="form-group col-lg-6">
		<label>Emergency contact for supplies</label> <input type="text" class="form-control " name="">
	</div>
</div></div>

<div class="form col-lg-12">
	<h3>Unplanned events</h3>
	<p>Are there any events, not covered in this form, which could happen infrequently? If so,
please give details of what could be expected and how it could be managed (e.g.
student is usually continent but could wet or soil occasionally; can change and clean up independently but will need reassurance)</p>
</div>

<div class="form col-lg-12">
	<h3>Catheter management</h3>
	<p>If a person is self-managing his or her catheter and has difficulty, the relevant school
staff will routinely:</p>
<ul>
	<li>reassure the person and encourage him or her to relax and try again</li>
	<li>suggest the person wait for half an hour and come back and try again</li>
</ul>
<p>If the student is still not successful, the parent/emergency contact will be informed.</p>
<p>A medical / health professional can be nominated by the family as the emergency
contact person in this case.</p>
<p>Staff will also contact the parent/emergency contact if the person displays signs of
possible difficulties such as sweating, discomfort, is flushed or pale, or has a headache.</p>
<strong>If no-one can be contacted, an ambulance may be called to transport the
person to medical assistance.</strong>
</div>

<div class="form col-lg-12">
	<h3>Additional information</h3>
	<p>Is there additional information required, such as further information regarding this student’s continence care, general information about the student’s health care needs:</p>

	<span class="sub_text">Privacy Statement</span>
	<span class="sub_text">The school collects personal information so as the school can plan and support the health care needs of the student. Without the provision
of this information the quality of the health support provided may be affected. The information may be disclosed to relevant school staff
and appropriate medical personnel, including those engaged in providing health support as well as emergency personnel, where
appropriate, or where authorised or required by another law. You are able to request access to the personal information that we hold about
you/your child and to request that it be corrected. Please contact the school directly or FOI Unit on 96372670.</span>
</div>

<div class="authorisation_wr">
	<div class="form">
		<h3 class="text-center">Authorisation:</h3>
		<div class="form-group">
		<table>
			<tr>
				<td><strong>Name of Medical/health practitioner:</strong></td><td></td>
			</tr>
				<tr><td>Professional Role:</td><td></td></tr>
				<tr><td>Signature:</td><td></td></tr>
				<tr><td>Date:</td><td></td></tr>
				<tr><td>Contact details:</td><td></td></tr>
			
		</table>
<hr/ style="border-color: #ccc;">
				<table>
			<tr>
				<td><strong>Name of Parent/Carer or adult/Mature minor**:</strong></td><td></td>
			</tr>
				<tr><td>Signature:</td><td></td></tr>
				<tr><td>Date:</td><td></td></tr>			
		</table>
			
		</div>
	</div>
</div>
<p>If additional advice is required, please attach it to this form</p>
<p>**Please note: Mature minor is a student who is capable of making their own decisions on a range of issues, before they reach eighteen
years of age. (  See: <a href="">Decision Making Responsibility for Students - School Policy &amp; Advisory Guide </a> ).</p>
	 	</section>
	 </div>
	 </div>
</section> 


<section id="main-content" class="form_wrapper">
	 <div class="wrapper main-wrapper row">	
	 <div class="col-lg-12 ">
	 	<section class="box">
	 	<h1>Routine mealtime care needs</h1>	 		
<div class="form col-lg-12">
<h3>Level of support required</h3>
	 		<p>Information is needed about how closely this student needs to be supervised and for how
long. Staff will routinely allow a maximum of 15 minutes per meal unless otherwise
negotiated.</p>
	<div class="col-lg-12">
	<i>Level of supervision</i>
		<div class="checkbox"><label><input type="checkbox"> Requires constant supervision: high risk of choking/ aspiration</label></div>
		<div class="checkbox"><label><input type="checkbox"> Requires close supervision (eg in small group)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Requires some assistance</label></div>
		<div class="checkbox"><label><input type="checkbox"> Independent</label></div>
	</div>
	<div class="col-lg-12">
	<strong>Time required for mealtime <i>(less for snacks)</i></strong> 
		<div class="checkbox"><label><input type="checkbox"> Less than 15 minutes</label></div>
		<div class="checkbox"><label><input type="checkbox"> About 15 minutes</label></div>
		<div class="checkbox"><label><input type="checkbox"> Negotiation if longer time recommended</label></div>
	</div>
	
</div>

<div class="form col-lg-12">
<h3>Type of support needed</h3>
	<div class="col-lg-6">
		<p><i>Preparation</i></p>
		<div class="checkbox"><label><input type="checkbox"> Additional hygiene/safety measures</label></div>
		<div class="checkbox"><label><input type="checkbox"> Positioning for comfort and safety</label></div>
		<div class="checkbox"><label><input type="checkbox"> Facilitation techniques (eg jaw support)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Stimulation (eg facial tapping/stroking)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Equipment</i></p>
		<div class="checkbox"><label><input type="checkbox"> Clothes protector</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified utensils (eg spoons)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified cup/plate etc</label></div>
		<div class="checkbox"><label><input type="checkbox"> Mirror</label></div>
		<div class="checkbox"><label><input type="checkbox"> Positioning equipment (eg specialchair/bolster)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Environmental changes</i></p>
		<div class="checkbox"><label><input type="checkbox"> Calm, consistent approach</label></div>
		<div class="checkbox"><label><input type="checkbox"> Positive reinforcement</label></div>
		<div class="checkbox"><label><input type="checkbox"> Minimal distractions</label></div>
		<div class="checkbox"><label><input type="checkbox"> Social settings</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Positioning and care after mealtimes</i></p>
		<div class="checkbox"><label><input type="checkbox"> Need to remain upright for minutes</label></div>
		<div class="checkbox"><label><input type="checkbox"> Need to check no food is left in the mouth/palate</label></div>
		<div class="checkbox"><label><input type="checkbox"> Teeth brushing</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
</div>

<div class="form col-lg-12">
<h3>Communication</h3>
	<div class="col-lg-6">
		<p><i>Communication by student</i></p>
		<div class="checkbox"><label><input type="checkbox"> Language</label></div>
		<div class="checkbox"><label><input type="checkbox"> Gesture</label></div>
		<div class="checkbox"><label><input type="checkbox"> Behaviour</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Communication by supporting staff</i></p>
		<div class="checkbox"><label><input type="checkbox"> Offer choice (indicate how many)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Simplify instructions/use key words</label></div>
		<div class="checkbox"><label><input type="checkbox"> Use picture cues</label></div>
		<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>

	</div>

<div class="form col-lg-12">
	<h3>Preparation and presentation of food and drink</h3>
	<p>The following information is provided as a safety check for staff. Food and drink should
routinely be brought to school already prepared. If some preparation is requested of staff,
this should be documented and negotiated with staff.</p>

<div class="col-lg-6">
		<p><i>Food consistency</i></p>
		<div class="checkbox"><label><input type="checkbox"> No restriction on consistency</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Quantity</i></p>
		<div class="checkbox"><label><input type="checkbox"> Self-directed</label></div>
		<div class="checkbox"><label><input type="checkbox"> Minimum amounts required (please specify)</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Food portions</i></p>
		<div class="checkbox"><label><input type="checkbox"> No restriction on amount taken at
a time</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Rate and order of intake</i></p>
		<div class="checkbox"><label><input type="checkbox"> Self-directed</label></div>
		<div class="checkbox"><label><input type="checkbox"> Direction/assistance required (please
specify</label></div>
	</div>

</div>

<div class="form col-lg-12">
	<h3>Preparation and presentation of food and drink, cont’d</h3>
	<div class="col-lg-6">
		<p><i>Drink consistency</i></p>
		<div class="checkbox"><label><input type="checkbox"> No restriction on consistency</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified</label></div>
		<p><i>Drink portions</i></p>
		<div class="checkbox"><label><input type="checkbox"> No restriction on amount taken at
each sip</label></div>
		<div class="checkbox"><label><input type="checkbox"> Modified</label></div>
	</div>
	<div class="col-lg-6">
		<p><i>Specific strategies required</i></p>
		<div class="checkbox"><label><input type="checkbox"> Spoon fed</label></div>
		<div class="checkbox"><label><input type="checkbox"> Finger food</label></div>
		<div class="checkbox"><label><input type="checkbox"> Drinking</label></div>
		<div class="checkbox"><label><input type="checkbox"> General (including behaviour
management issues)</label></div>
<div class="checkbox"><label><input type="checkbox"> Other</label></div>
	</div>
</div>

<div class="form col-lg-12">
	<h3>Potential learning targets</h3>
	<p>Mealtimes are considered a time for socialisation and enjoyment. Any specific learning
targets (eg in relation to trying new foods and textures) are generally addressed at
home. If some experimenting and promotion of new foods and tastes are requested,
this should be documented and negotiated with staff.</p>
<div class="col-lg-12">
		<div class="checkbox"><label><input type="checkbox"> Increasing independence (eg collects lunchbox, manages spoon)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Behaviour targets (eg remains in seat for five spoonfuls)</label></div>
		<div class="checkbox"><label><input type="checkbox"> Increasing intake (eg eats half a sandwich at lunchtime)</label></div>
		
	</div>
</div>

<div class="form col-lg-12">
	<h3>Documented observations</h3>
	<p>Upon negotiation, the school may assist the medical/health practitioner by documenting
mealtime observations for the student. If this is required, please indicate what
information is needed from the oral eating and drinking observations.</p>
</div>

<div class="form col-lg-12">
	<h3>General Supervision for safety</h3>
	<p>Unless otherwise negotiated, the school staff member will stop the eating/drinking
process if they observe any of the following signs:</p>
	<ul>
		<li>Self-reported distress or show other signs of distress</li>
		<li>Tried and unable to manager</li>
		<li>Gagging or coughing with unusual frequency</li>
		<li>Pale and sweaty</li>
		<li>Watery/glassy eyes</li>
		<li>Unusual change of voice</li>
		<li>Gurgling wet rattle in the throat</li>
		<li>Unable to cough, stops breathing (choking)</li>
	</ul>
	<p>If these signs are repeatedly observed, the student’s medical/health practitioner should
review this form and provide updated information.</p>
</div>

<div class="form col-lg-12">
<h3 class="text-center">First Aid</h3>
<p>If the student becomes ill or injury at school (such as if the student begins to choke), the school will administer first aid and call at ambulance
if necessary. If you anticipate the student will require anything other than a standard first aid response, please provide details on the next
page, so special arrangement can be negotiated.</p>

<div class="col-lg-6">
		<h4>Observable sign/reaction</h4>
		<div class="form-group"><input type="text" name=""> <button>Add</button></div>
	</div>
<div class="col-lg-6">
		<h4>First aid response</h4>
		<div class="form-group"><input type="text" name=""> <button>Add</button></div>
	</div>

	<span class="sub_text">Privacy Statement</span>
<span class="sub_text">The school collects personal information so as the school can plan and support the health care needs of the student. Without the provision of this
information the quality of the health support provided may be affected. The information may be disclosed to relevant school staff and appropriate medical
personnel, including those engaged in providing health support as well as emergency personnel, where appropriate, or where authorised or required by
another law. You are able to request access to the personal information that we hold about you/your child and to request that it be corrected. Please
contact the school directly or FOI Unit on 96372670.</span>
</div>


<div class="authorisation_wr">
	<div class="form">
		<h3 class="text-center">Authorisation:</h3>
		<div class="form-group">
		<table>
			<tr>
				<td><strong>Name of Medical/health practitioner:</strong></td><td></td>
			</tr>
				<tr><td>Professional Role:</td><td></td></tr>
				<tr><td>Signature:</td><td></td></tr>
				<tr><td>Date:</td><td></td></tr>
				<tr><td>Contact details:</td><td></td></tr>
			
		</table>
<hr/ style="border-color: #ccc;">
				<table>
			<tr>
				<td><strong>Name of Parent/Carer or adult/Mature minor**:</strong></td><td></td>
			</tr>
				<tr><td>Signature:</td><td></td></tr>
				<tr><td>Date:</td><td></td></tr>			
		</table>
			
		</div>
	</div>
</div>
<p>If additional advice is required, please attach it to this form</p>
<p>**Please note: Mature minor is a student who is capable of making their own decisions on a range of issues, before they reach eighteen
years of age. (  See: <a href="">Decision Making Responsibility for Students - School Policy &amp; Advisory Guide </a> ).</p>
	 	</section>
	 </div>
	 </div>
</section> 


<script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
<script type="text/javascript">
function fileValidation(){
    var fileInput = document.getElementById('studImg');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'" style="width:100px;height:auto;"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>
<?php $this->load->view('templates/footer'); ?>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js')?>'></script>
<script type="text/javascript" src='<?=base_url('assets/js/form-validation.js')?>'></script>
<script type="text/javascript">

$(document).ready(function(){
	$('#lastReview').datepicker({
		format: 'yyyy-mm-dd',
		endDate: '<?=date('Y-m-d')?>',
    });
    
    $('#dateAuthorization').datepicker({
		format: 'yyyy-mm-dd',
		endDate: '<?=date('Y-m-d')?>',
	});                
//$('#dateAuthorization','#lastReview').datepicker('setDate',stdDOB);
$("form[name='student_data_feed']").validate({
	rules: {
      roll_no: "required",
	  studentname: "required",
	  standard: "required",
	  section: "required",
	  dob: "required",
	  age:{ 
		  required: true,
		  number:true
		},
	  sex: "required",
	  bgroup: "required",
	  fname: "required",
	  mname: "required",
	  foccupation: "required",
	  mobileno:{ 
		  required: true,
		  //number:true,
		  minlength:10,
		  maxlength:13
		},
      email: {
        required: true,
        email: true
	  }
	},
	  messages: {
		roll_no: "Please enter your roll no",
		studentname: "Please enter your student name",
		mobileno: {
        required: "Please provide a mobile No",
        minlength: "Your password must be at least 10 numeric long"
      },
      email: "Please enter a valid email address"
    },
 
	  
	  submitHandler: function(form) {
      form.submit();
	}
});
 

});
</script>
<style type="text/css">
	.datepicker{
		z-index: 9999 !important;
	}
</style>
