<?php $this->load->view('templates/header'); ?>
<?php $this->load->view('templates/sidebar'); ?>
<section id="main-content" class=" ">
    <div class="col-lg-12">
        <section class="box">
            <header class="panel_header">
                <h2 class="title pull-left">View Users</h2>                                
            </header>
            <a href="<?=base_url().'users/schoolUpdate'?>">Add School</a>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Address</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($school_det as $school){?>
                                    <tr>
                                        <td>
                                            <h6><?=$school->school_name?></h6>
                                        </td>
                                        <td><?=$school->email?></td>
                                        <td><?=$school->city?></td>
                                        <td><?=$school->address?></td>
                                        <td>
                                            <a href="<?=base_url().'users/schoolUpdate/'.$school->id ?>">Edit</a>
                                            <a href="<?=base_url().'users/delete_school/'.$school->id ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>    
</section>                    
<?php $this->load->view('templates/footer'); ?>