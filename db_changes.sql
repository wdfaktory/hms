
ALTER TABLE `students`  ADD `pube` INT(1) NOT NULL DEFAULT '0'  AFTER `user_name`,  ADD `special_care` INT(1) NOT NULL DEFAULT '0'  AFTER `pube`,  ADD `vaccination` INT(1) NOT NULL DEFAULT '0'  AFTER `special_care`,  ADD `allergic` INT(1) NOT NULL DEFAULT '0'  AFTER `vaccination`,  ADD `asthma` INT(1) NOT NULL DEFAULT '0'  AFTER `allergic`;

ALTER TABLE `students`  ADD `buddy_up_accepted` TINYINT NOT NULL  AFTER `asthma`;
ALTER TABLE `students`  ADD `buddy_up_rejected` TINYINT NOT NULL  AFTER `buddy_up_accepted`;
ALTER TABLE `students`  ADD `buddy_up_req_Sent` TINYINT NOT NULL  AFTER `buddy_up_rejected`;

ALTER TABLE `reports`  ADD `stud_id` INT(11) NOT NULL  AFTER `r_id`;
ALTER TABLE `reports` CHANGE `r_id` `r_mongo_id` VARCHAR(100) NOT NULL;
ALTER TABLE `reports`  ADD `status` TINYINT NOT NULL  AFTER `type`;


17-05-2020
================
ALTER TABLE `reports` CHANGE `type` `type` TINYINT(4) NOT NULL COMMENT '1- Sick, 2-Medical Condition, 3 - Allergic, 4-Special Care';
ALTER TABLE `reports` CHANGE `created_date` `created_date` DATETIME NOT NULL;


Yet to update on Live 
========================
ALTER TABLE `reports` CHANGE `status` `status` TINYINT(4) NOT NULL COMMENT '1 - Approved, 2 - Rejected';
ALTER TABLE `schools` ADD `zone` VARCHAR(100) NOT NULL AFTER `address`;
UPDATE `permissions` SET `name` = 'Parents', `slug` = 'parents-dashboard', `type` = '1' WHERE `permissions`.`id` = 12;
UPDATE `user_permissions` SET `permission` = '3,5,11,12' WHERE `user_permissions`.`id` = 2;
INSERT INTO `curd_permissions` (`id`, `user_id`, `permission`, `curd`, `created_date`) VALUES (NULL, '2', '12', '1,2,3,4,5', '2020-04-17 16:24:37');