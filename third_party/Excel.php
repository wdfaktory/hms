<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


//require_once APPPATH . "/libraries/PHPExcel.php";
require_once __DIR__ ."/PHPExcel.php";

class Excel extends PHPExcel {

    public function __construct() {
        
    }

    public  static function create_excel_file($header,$file_name_path)
	{

		$CI =& get_instance();
		$CI->load->library('PHPExcel');		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
				    ->setCreator("Notionpress Publishing Solutions")
				    ->setLastModifiedBy("Notionpress Publishing Solutions")
				    ->setSubject("PRIS/EPZ Orders");
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Simple');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font' => array(
		        'bold' => true
		    ),
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		    ),
		 'borders' => array(
		        'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('argb' => '00000000'),
		        ),
		    ),
		);

		$objPHPExcel->getActiveSheet()->getStyle('A1:AD1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()
		    ->getStyle('D1')
		    ->getNumberFormat()
		    ->setFormatCode(
		        PHPExcel_Style_NumberFormat::FORMAT_GENERAL
		    );
		$objPHPExcel->getActiveSheet()->fromArray($header, NULL, 'A1');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		$objWriter->save(str_replace(__FILE__,$file_name_path,__FILE__));

		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		return  $file_name_path;

		//return $file_name_path;
	}
	
    
    
    public static function create_excel_file1($header, $type,$file_name_path) {

        $CI = & get_instance();
        $CI->load->library('PHPExcel');
        $objPHPExcel = new PHPExcel();
        /* $objPHPExcel->getProperties()
                ->setCreator("Notionpress Publishing Solutions")
                ->setLastModifiedBy("Notionpress Publishing Solutions")
                ->setSubject("PRIS/EPZ Orders"); */
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        $styleArray = array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
        if($type == 'admin')
            $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($styleArray);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->applyFromArray($styleArray);
        /* $objPHPExcel->getActiveSheet()
                ->getStyle('D1')
                ->getNumberFormat()
                ->setFormatCode(
                        PHPExcel_Style_NumberFormat::FORMAT_GENERAL
        ); */
        /* for($i=0;$i<100;$i++){
            $objPHPExcel->getActiveSheet()
                ->getStyle('E'.$i)
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);//toFormattedString(39984,PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY));//FORMAT_DATE_YYYYMMDD2);//FORMAT_DATE_DDMMYYYY));//        }
         */    
        $objPHPExcel->getActiveSheet()->fromArray($header['header'], NULL, 'A1');

        /* $continentColumn = 'N';
        $i = 0;
        $listcount = count($header['header']);

        foreach ($header['header'] as $key => $value) {

            $objPHPExcel->getActiveSheet()
                    ->setCellValue($continentColumn . ($i + 1), $value);

            $i++;
        }


        // Hide the dropdown data
        $objPHPExcel->getActiveSheet()
                ->getColumnDimension($continentColumn)
                ->setVisible(false);

        $objPHPExcel->addNamedRange(
            new PHPExcel_NamedRange(
            'Continents', 
            $objPHPExcel->getActiveSheet(), $continentColumn . '1:' . $continentColumn . $listcount
            )
        );
 */

        
		$filename=time().'_studentUpTemp.xlsx'; //save our workbook as this file name
		 header('Content-Type: application/vnd.ms-excel'); //mime type
		 header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		 header('Cache-Control: max-age=0'); //no cache
		 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		 //if you want to save it as .XLSX Excel 2007 format
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //force user to download the Excel file without writing it to server's HD
		 $objWriter->save('php://output');
        
    }
    
    
    public static function download_excel($data, $file = FALSE,$fileName = '') {
         $file_name_path = FCPATH . '/report_files/';

        if ($file == FALSE)
            $file = date('Ymd') . '_.xlsx';
        $filename = $file_name_path . $file;

        Excel::create_excel_file2($data, $filename,$fileName);
       
    }

    public static function download_excel1($data, $file = FALSE) {
        $file_name_path = FCPATH . '/report_files/';

        if ($file == FALSE)
            $file = date('Ymd') . '_.xlsx';
        $filename = $file_name_path . $file;

        Excel::create_excel_file1($data, $filename);

    }

    public static function downloadStudetTemp($data, $type='', $file = FALSE) {
        $file_name_path = FCPATH . '/report_files/';
        if ($file == FALSE)
            $file = date('Ymd') . '_.xlsx';
        $filename = $file_name_path . $file;

        Excel::create_excel_file1($data, $type, $filename);

    }

    public static function create_excel_file3($header, $type,$file_name_path) {

        $CI = & get_instance();
        $CI->load->library('PHPExcel');
        $objPHPExcel = new PHPExcel();
        /* $objPHPExcel->getProperties()
                ->setCreator("Notionpress Publishing Solutions")
                ->setLastModifiedBy("Notionpress Publishing Solutions")
                ->setSubject("PRIS/EPZ Orders"); */
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        $styleArray = array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
        if($type == 'admin')
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
        /* $objPHPExcel->getActiveSheet()
                ->getStyle('D1')
                ->getNumberFormat()
                ->setFormatCode(
                        PHPExcel_Style_NumberFormat::FORMAT_GENERAL
        ); */
        /* for($i=0;$i<100;$i++){
            $objPHPExcel->getActiveSheet()
                ->getStyle('E'.$i)
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);//toFormattedString(39984,PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY));//FORMAT_DATE_YYYYMMDD2);//FORMAT_DATE_DDMMYYYY));//        }
         */    
        $objPHPExcel->getActiveSheet()->fromArray($header['header'], NULL, 'A1');

        /* $continentColumn = 'N';
        $i = 0;
        $listcount = count($header['header']);

        foreach ($header['header'] as $key => $value) {

            $objPHPExcel->getActiveSheet()
                    ->setCellValue($continentColumn . ($i + 1), $value);

            $i++;
        }


        // Hide the dropdown data
        $objPHPExcel->getActiveSheet()
                ->getColumnDimension($continentColumn)
                ->setVisible(false);

        $objPHPExcel->addNamedRange(
            new PHPExcel_NamedRange(
            'Continents', 
            $objPHPExcel->getActiveSheet(), $continentColumn . '1:' . $continentColumn . $listcount
            )
        );
 */

        
		$filename=time().'_studentAttendanceUpTemp.xlsx'; //save our workbook as this file name
		 header('Content-Type: application/vnd.ms-excel'); //mime type
		 header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		 header('Cache-Control: max-age=0'); //no cache
		 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		 //if you want to save it as .XLSX Excel 2007 format
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //force user to download the Excel file without writing it to server's HD
		 $objWriter->save('php://output');
        
    }

    public static function downloadAttendanceTemp($data, $type='', $file = FALSE) {
        $file_name_path = FCPATH . '/report_files/';
        if ($file == FALSE)
            $file = date('Ymd') . '_.xlsx';
        $filename = $file_name_path . $file;

        Excel::create_excel_file3($data, $type, $filename);

    }

}
