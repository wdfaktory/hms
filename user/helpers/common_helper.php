<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function connectMongo(){
    $ci =& get_instance();
    $ci->load->config('static_configs');
    $db_name = $ci->config->item('mongodb_name');
    $username = $ci->config->item('mongodb_username');
    $password = $ci->config->item('mongodb_password');
    // Need to convert as singleton pattern
    /*if($mongodb == NULL){*/
        $path = str_replace('\\', '/', BASEPATH).'../vendor/autoload.php';
        require $path;
        $client = new MongoDB\Client("mongodb+srv://$username:$password@first-cluster-c9whb.mongodb.net/?ssl=true&authSource=admin&serverSelectionTryOnce=false&serverSelectionTimeoutMS=15000&w=majority");
        // Select Database
        $mongodb = $client->{$db_name};
    /*}*/
    return $mongodb;
}

function send_email($from,$to,$subject,$message,$type = 'plain'){
    $path = str_replace('\\', '/', BASEPATH).'../third_party/sendgrid/sendgrid-php.php';
    include $path;
    $email = new \SendGrid\Mail\Mail();
    $from_name = 'infocaresolutions';
    $email->setFrom($from, $from_name);
    $email->setSubject($subject);
    $email->addTo($to);
    if($type == 'plain')
        $email->addContent("text/plain", $message);
    else
        $email->addContent("text/html", $message);

    $sendgrid = new \SendGrid('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');
    try {
        return $response = $sendgrid->send($email);
        /*print $response->statusCode() . "\n";
        print_r($response->headers());
        print $response->body() . "\n";*/
    } catch (Exception $e) {
        return false;
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}

?>
