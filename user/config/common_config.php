<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['symptoms_text'] 	= array('Mild/Heavy Fever', 'Cough', 'Sneeze', 'Stuffy or Runny nose', 'Aches', 'Sore throat', 'Fatigue and Weakness', 'Headache');

$config['allergic_text'] = array( 
	'mild' => array( 
		'signs' => array('Swelling of lips, face, eyes','Hives or welts', 'Tingling mouth', 'Abdominal pain, vomiting (these are signs of anaphylaxis for insect allergy)'),
		'actions' => array('For insect allergy - flick out sting if visible', 'Seek medical help', 'Stay with person and call for help', 'Phone family/emergency contact')
	),
	'severe' => array(
		'signs' => array('Difficult/noisy breathing', 'Swelling of tongue', 'Swelling/tightness in throat', 'Wheeze or persistent cough', 'Difficulty talking and/or hoarse voice', 'Persistent dizziness or collapse', 'Pale and floppy (young children)'),
		'actions' => array(
			'Lay person flat - do NOT allow them to stand or walk. If unconscious, place in recovery position. If breathing is difficult allow them to sit',
			'Give adrenaline (epinephrine) autoinjector if available',
			'Phone ambulance - 108',
			'Phone family/emergency contact',
			'Transfer person to hospital for at least 4 hours of observation'
		)
	)
);


?>