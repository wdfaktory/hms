<?php 
class Parents_model extends MY_Model {
        public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }

        public function getStudentAllData($id){
                $this->db->select('s.*,sc.school_name,sc.address as "school_address",sc.city');
                $this->db->where('s.id',$id);
                $this->db->where('s.deleted',0);
                $this->db->where('p.id',$this->session->parent_id);
                $this->db->join('schools sc','sc.id = s.school_id');
                $this->db->join('parents p','s.parent_id = p.id','left');
                $res = $this->db->get('students s');
                return $res->row();
        }
}

?>