<?php 
class MY_Model extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
            $this->load->database();
    }

    // Return all records in the table
    public function get_all($table)
    {
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->result();
        }
        return array();
    }


    // Return only one row
    public function get_row($table,$whrArr = array(),$selectArr = array())
    {
        if(!empty($selectArr)){
            $this->db->select($selectArr);
        }
        $this->db->where($whrArr);
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->row();
        }
        return false;
    }

    // Return only one row
    public function get_row_array($table,$whrArr = array(),$selectArr = array())
    {
        if(!empty($selectArr)){
            $this->db->select($selectArr);
        }
        $this->db->where($whrArr);
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->row_array();
        }
        return false;
    }

    // Return one only field value
    public function get_data($table,$primaryfield,$fieldname,$id)
    {
        $this->db->select($fieldname);
        $this->db->where($primaryfield,$id);
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->result();
        }
        return array();
    }



    // Insert into table
    public function insert($table,$data)
    {
        return $this->db->insert($table, $data);
    }
 
    // Update data to table
    public function update($table,$dataArr = array(),$whereArr=array())
    {
        $this->db->where($whereArr);
        $q = $this->db->update($table, $dataArr);
        return $q;
    }
 
    // Delete record from table
    public function delete($table,$primaryfield,$id)
    {
    	$this->db->where($primaryfield,$id);
    	$this->db->delete($table);
    }


    public function insert_or_update($table, $dataArr = array(), $whereArr = array()){
        $check = $this->get_row($table,$whereArr);
        if($check != NULL){
            $this->update($table,$dataArr,$whereArr);
        }else{
            $dataArr['created_date'] = date('Y-m-d H:i:s',time());
            $this->insert($table, $dataArr);
        }
    }

    public function get_count($table,$whrArr = array())
    {
        if(!empty($whrArr))
            $this->db->where($whrArr);
        $q = $this->db->get($table);
        return $q->num_rows();
        
    }
}

?>