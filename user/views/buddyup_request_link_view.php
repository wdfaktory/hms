<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
	<!-- <link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	 -->
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Buddyup Request</title>
</head>
<body>
<div class="chooseChild">
	<div class="wrapper">
		<h3><?php echo "You have $action the buddy up request of $studentName"; ?> </h3>
	</div>
	<a href="<?=base_url('parents')?>"><button> Return to Dashboard</button></a>
	</div>
</div>
</body>
  <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.js"></script>
  <script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
</html>