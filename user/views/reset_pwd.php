<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" type="text/css">
	
	<link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<title>Home</title>
</head>
<body>
<div class="login-container">
	<div class="">
		<div class="login-poster">
			<img src="<?=base_url('assets/images/log.jpg')?>" />
		</div>
		<div class="login-form text-center">
			<h2>Forgot Password</h2>
			<br><br>
			<div class="alert alert-success" id="sent_ok" style="display:none;">
			  <span>
				<p style="color:green;">Mail Sent Successfully Kindly Check!</p>
			  </span>
			</div>
			<div class="alert alert-error" id="sent_not_ok" style="display:none;">
			  <span>
				<p style="color:red;">Invalid Email Id!</p>
			  </span>
			</div>
			<form action="#" method="POST" class="login">
				<input type="hidden" value="<?php echo base_url(); ?>" id="base_url">
				<div class="form-group">
					<label for="name">Email</label>
					<input type="email" class="form-control" name="email" id="email" required>
				</div>
				<button type="button" id="click_reset" style="margin-left: 74px;" class="btn btn-primary">Click Here</button>
			</form>	
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$("#click_reset").click(function(){
	
	
	var BASE_URL = "<?php echo base_url();?>";
	var email = $('#email').val();
	
	if(email == ''){
		alert('Please enter email'); return false;
	}
	//alert(base_url); return false;
	$.ajax({
		url: BASE_URL+'/users/reset_pwd',
		dataType : 'json',
		type: "post",
		data: {'email': email},
		success: function (response) {
			//console.log(response.success);
			if(response.success == 1){
				$('#sent_not_ok').hide();
				$('#sent_ok').show();
			}else{
				$('#sent_ok').show();
				//$('#sent_ok').hide();
				//$('#sent_not_ok').show();
			}
			
		   // You will get response from your PHP page (what you echo or print)
		}
	});
});
</script>

</body>
  
</html>