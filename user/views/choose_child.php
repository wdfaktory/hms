<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
	<!-- <link href="<?=base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">	 -->
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Select Child</title>
</head>
<body>
<div class="chooseChild">
	<div class="wrapper">
		<h2>Select Child</h2>
		<ul class="list">
		<?php foreach($students_data as $std) {
			echo "<a href='".base_url('parents/studView/'.$std->id)."'><li></li><span>$std->student_name</span></a>";
		 } ?>
			<!-- <li><a href="#">A</a><span>Children 2</span></li>
			<li><a href="#">A</a><span>Children 3</span></li>
			<li><a href="#">A</a><span>Children 4</span></li> -->
		</div>
	</div>
</div>
</body>
  <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.js"></script>
  <script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
  <script type="text/javascript">
  $('li').each(function() {
  var back = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];//["green","blue","gray","yellow","orange","purple","skyblue","pink","red"];
  var rand = back[Math.floor(Math.random() * back.length)];
  $(this).css('background',rand);
});
  </script>
</html>