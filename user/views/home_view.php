<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>HMS | Health Management Solutions</title>

	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<!-- master stylesheet -->
	<link rel="stylesheet" href="<?=base_url('resources/css/')?>style.css">
	<!-- Responsive stylesheet -->
	<link rel="stylesheet" href="<?=base_url('resources/css/')?>responsive.css">
    <!--Color Switcher Mockup-->
    <link rel="stylesheet" href="<?=base_url('resources/css/')?>color-switcher-design.css">
    <!--Color Themes-->
    <link rel="stylesheet" href="<?=base_url('resources/css/')?>color-themes/default-theme.css" id="theme-color-file">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('resources/images/')?>favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?=base_url('resources/images/')?>favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=base_url('resources/images/')?>favicon/favicon-16x16.png" sizes="16x16">
    
</head>

<body>
<div class="boxed_wrapper">

<div class="preloader"></div> 
 
<!--Start header area-->
<header class="header-area">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="inner-content clearfix">
                        <div class="header-left float-left">
                            <div class="logo">
                                <a href="<?=base_url()?>">
                                    <img src="<?=base_url()?>assets/images/dummylogo.jpg" alt="Awesome Logo">
                                </a>
                            </div>
                        </div>
                       <!--  <div class="header-middle clearfix float-left">
                            <ul class="info-box">
                                <li>
                                    <div class="icon-holder">
                                        <span class="icon-multimedia thm-clr"></span>    
                                    </div>
                                    <div class="title-holder">
                                        <h5>Mail us</h5> 
                                        <p>Austins@Behealthy.com</p>   
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <span class="icon-time thm-clr"></span>    
                                    </div>
                                    <div class="title-holder">
                                        <h5>Mon - Sat 8.00 - 18.00</h5> 
                                        <p>Sunday Closed</p>   
                                    </div>
                                </li>     
                            </ul>   
                        </div> -->
                        <!-- <div class="header-right float-right">
                            <a href="#" class="btn-one thm-bg-clr">Book Now</a>      
                        </div> -->
                        <div class="header-bottom-right float-right">
                            <span>Stay Connected:</span> 
                            <ul class="header-social-links">
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                            </ul>   
                        </div>                        
                        <!-- <div class="header-right float-right" style="padding-left: 10px">
                            <a href="<?=base_url('school/login')?>" class="btn-one thm-bg-clr">School Login</a>      
                        </div> -->
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="header-bottom stricky">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="inner-content clearfix">
                        <div class="header-bottom-left clearfix float-left">
                            <!--Start mainmenu-->
                            <nav class="main-menu clearfix">
                                <div class="navbar-header clearfix">   	
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current"><a href="<?=base_url()?>">Home</a></li>
                                        <li><a href="#">About</a></li>
                                       <!--  <li class="dropdown"><a href="#">Services</a>
                                            <ul>
                                                <li><a href="#">Nutrition Strategies</a></li>
                                                <li><a href="#">Workout Routines</a></li>
                                                <li><a href="#">Fitness & Performance</a></li>
                                                <li><a href="#">Support & Motivation</a></li>
                                                <li><a href="#">Balance Body & Mind</a></li>
                                                <li><a href="#">Physical Activities</a></li>
                                            </ul>
                                        </li> -->
                                        <li class="dropdown"><a href="#">Health Living</a></li>
                                        <li class="dropdown"><a href="#">Health Dashboard</a></li>
                                        <li class="dropdown"><a href="#">Programs</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!--End mainmenu-->
                            <!-- <div class="outer-search-box">
                                <div class="seach-toggle"><i class="fa fa-search"></i></div>
                                <ul class="search-box">
                                    <li>
                                        <form method="post" action="http://st.ourhtmldemo.com/new/Nourish/<?=base_url()?>">
                                            <div class="form-group">
                                                <input type="search" name="search" placeholder="Search Here" required>
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </div> --> 
                        </div>
                        <div class="header-right float-right" style="padding-left: 10px">
                            <a href="<?=base_url('login')?>" class="btn-one thm-bg-clr">Login</a>      
                        </div>
                    </div>    
                </div>    
            </div>    
        </div>    
    </div>
</header>  
<!--End header area-->       
    
<!--Main Slider-->
<section class="main-slider main-slider-one">
    <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_486_1_wrapper" data-source="gallery">
        <div class="rev_slider fullwidthabanner" id="rev_slider_486_1" data-version="5.4.1">
            <ul>

                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?=base_url('resources/images/')?>slides/1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?=base_url('resources/images/')?>slides/1.jpg"> 

                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-70','-80','-80','-40']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">
                            <div class="title">Hi Friends!</div>
                            <div class="big-title">I'm Nourish Jacob</div>
                        </div>   
                    </div>
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['25','20','20','80']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">   
                            <div class="text">Personal & Family <span>Health Coacher</span></div>
                        </div>
                    </div>
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['110','110','110','180']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">
                            <div class="btn-box">
                                <a href="#" class="btn-one thm-bg-clr">Let’s Start</a>
                                <a class="video" href="#"><span class="icon-play"></span>My Coaching Class</a>
                            </div>
                        </div>
                    </div>
                   
                </li>

                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?=base_url('resources/images/')?>slides/2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?=base_url('resources/images/')?>slides/2.jpg"> 

                    <div class="tp-caption tp-resizeme text-center" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['900','900','800','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-70','-80','-80','-40']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content">
                            <div class="title">Be Healthy</div>
                            <div class="big-title">Certified Health Coach</div>
                        </div>  
                    </div>
                    <div class="tp-caption tp-resizeme text-center" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['700','700','700','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['25','20','20','80']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content">   
                            <div class="text">We have <span>25+ years</span> experience</div>
                        </div>
                    </div>
                    <div class="tp-caption tp-resizeme text-center" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['650','600','500','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['110','110','110','180']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content">
                            <div class="btn-box">
                                <a href="#" class="btn-one thm-bg-clr">About Us</a>
                                <a href="#" class="btn-one thm-bg-clr">Services</a>
                            </div>
                        </div>
                    </div>
                   
                </li>
                
                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?=base_url('resources/images/')?>slides/3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?=base_url('resources/images/')?>slides/3.jpg"> 

                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-70','-80','-80','-40']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">
                            <div class="title">A natural way of</div>
                            <div class="big-title">Improving health</div>
                        </div>   
                    </div>
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['25','20','20','80']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">   
                            <div class="text">With our dedication <span>Support</span></div>
                        </div>
                    </div>
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-whitespace="normal"
                    data-width="['800','800','600','400']"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['110','110','110','180']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                        <div class="slide-content left-slide">
                            <div class="btn-box">
                                <a href="#" class="btn-one thm-bg-clr">Services</a>
                                <a href="#" class="btn-one thm-bg-clr">Our events</a>
                            </div>
                        </div>
                    </div>
                   
                    
                </li>
            </ul>
        </div>
    </div>
</section>
<!--End Main Slider-->

<!--Start Welcome area-->
<section class="welcome-area">
    <div class="container">
        <div class="row">
            
            <div class="col-xl-6 col-lg-6">
                <div class="welcome-image-holder">
                    <img src="<?=base_url('resources/images/')?>resources/welcome.jpg" alt="Awesome Image">
                    <div class="overlay-content">
                        <h2>I’m your new personal health coach for your better life.</h2>
                    </div> 
                </div>   
            </div>
            
            <div class="col-xl-6 col-lg-6">
                <div class="text-holder">
                    <div class="sec-title">
                        <div class="title left">Welcome to Health Coach</div>
                        <div class="dector thm-bg-clr"></div>
                    </div>
                    <div class="inner-content">
                        <div class="text">
                            <p>Work praising pain was born and will give you can complete design account sed the system, and expound the actual teachngs interior of the great design explorer of the truth master-builders design of human happiness one seds rejects, dislikes, or avoids pleasures give of the master-builder of human itself.</p>
                            
                            <p>Idea of denouncing pleasure  praising pain was born and i will give you a complete account of the system, &expound the actual teachings the great explorer of the truth, the master.</p>
                        </div>   
                    </div>
                </div> 
                <div class="accordion-holder style-one">
                    <!-- Accordion -->
                    <article class="accordion">
                        <div class="acc-btn active">
                            <div class="toggle-icon"><span class="icon-shapes thm-clr"></span></div> 
                            <h3>Caring & Respect</h3>
                        </div>
                        <div class="acc-content collapsed">
                            <div class="inner">
                                <p>People are most productive when they wake up, and Walking, running, dancing, swimming, yoga are a few physical activity.</p>
                            </div>
                        </div>
                    </article>
                    <!-- Accordion -->
                    <article class="accordion">
                        <div class="acc-btn">
                            <div class="toggle-icon"><span class="icon-medical thm-clr"></span></div> 
                            <h3>Brod Vision</h3>
                        </div>
                        <div class="acc-content">
                            <div class="inner">
                                <p>People are most productive when they wake up, and Walking, running, dancing, swimming, yoga are a few physical activity.</p>
                            </div>
                        </div>
                    </article>
                </div>        
            </div>
            
        </div> 
    </div>    
</section>
<!--End Welcome area-->

<!--Start services area-->
<section class="services-area" style="background-image: url(<?=base_url('resources/images/')?>parallax-background/service-bg.jpg);">
    <div class="container">
        <div class="sec-title text-center">
            <div class="title clr-white">Services We Provide</div>
            <div class="dector thm-bg-clr center"></div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="service-carousel-box owl-carousel owl-theme">
                    <!--Start single service item--> 
                    <div class="single-service-item">
                        <div class="inner-content">
                            <div class="title-holder top">
                                <div class="icon">
                                    <span class="icon-fruit thm-clr"></span>    
                                </div>
                                <div class="title">
                                    <h3><a href="#">Nutrition Strategies</a></h3>
                                    <span>All Age Groups</span>
                                </div>   
                            </div>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>services/1.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <div class="text-holder">
                                                <div class="title-holder">
                                                    <div class="icon">
                                                        <span class="icon-fruit thm-clr"></span>    
                                                    </div>
                                                    <div class="title">
                                                        <h3><a href="#">Nutrition Strategies</a></h3>
                                                        <span>All Age Groups</span>
                                                    </div>   
                                                </div>
                                                <div class="text">
                                                    <p>Finding a workout is as easy as scrolling down, picking out the session matches your goals and getting.</p>
                                                    <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Read More</a>  
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-button">
                                <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Let’s Start</a>    
                            </div>
                        </div>
                    </div>
                    <!--End single service item-->
                    <!--Start single service item--> 
                    <div class="single-service-item">
                        <div class="inner-content">
                            <div class="title-holder top">
                                <div class="icon">
                                    <span class="icon-sports thm-clr"></span>    
                                </div>
                                <div class="title">
                                    <h3><a href="#">Workout Routines</a></h3>
                                    <span>Adults Only</span>
                                </div>   
                            </div>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>services/2.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <div class="text-holder">
                                                <div class="title-holder">
                                                    <div class="icon">
                                                        <span class="icon-sports thm-clr"></span>    
                                                    </div>
                                                    <div class="title">
                                                        <h3><a href="#">Workout Routines</a></h3>
                                                        <span>Adults Only</span>
                                                    </div>   
                                                </div>
                                                <div class="text">
                                                    <p>Finding a workout is as easy as scrolling down, picking out the session matches your goals and getting.</p>
                                                    <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Read More</a>  
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-button">
                                <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Let’s Start</a>    
                            </div>
                        </div>
                    </div>
                    <!--End single service item-->
                    <!--Start single service item--> 
                    <div class="single-service-item">
                        <div class="inner-content">
                            <div class="title-holder top">
                                <div class="icon">
                                    <span class="icon-exercise thm-clr"></span>    
                                </div>
                                <div class="title">
                                    <h3><a href="#">Fitness & Performace</a></h3>
                                    <span>All Age Groups</span>
                                </div>   
                            </div>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>services/3.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <div class="text-holder">
                                                <div class="title-holder">
                                                    <div class="icon">
                                                        <span class="icon-exercise thm-clr"></span>    
                                                    </div>
                                                    <div class="title">
                                                        <h3><a href="#">Fitness & Performace</a></h3>
                                                        <span>All Age Groups</span>
                                                    </div>   
                                                </div>
                                                <div class="text">
                                                    <p>Finding a workout is as easy as scrolling down, picking out the session matches your goals and getting.</p>
                                                    <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Read More</a>  
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-button">
                                <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Let’s Start</a>    
                            </div>
                        </div>
                    </div>
                    <!--End single service item-->
                    <!--Start single service item--> 
                    <div class="single-service-item">
                        <div class="inner-content">
                            <div class="title-holder top">
                                <div class="icon">
                                    <span class="icon-fruit thm-clr"></span>    
                                </div>
                                <div class="title">
                                    <h3><a href="#">Nutrition Strategies</a></h3>
                                    <span>All Age Groups</span>
                                </div>   
                            </div>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>services/1.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <div class="text-holder">
                                                <div class="title-holder">
                                                    <div class="icon">
                                                        <span class="icon-fruit thm-clr"></span>    
                                                    </div>
                                                    <div class="title">
                                                        <h3><a href="#">Nutrition Strategies</a></h3>
                                                        <span>All Age Groups</span>
                                                    </div>   
                                                </div>
                                                <div class="text">
                                                    <p>Finding a workout is as easy as scrolling down, picking out the session matches your goals and getting.</p>
                                                    <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Read More</a>  
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-button">
                                <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Let’s Start</a>    
                            </div>
                        </div>
                    </div>
                    <!--End single service item-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="service-bottom-text">
                    <p>Our focus is on your overall well being and helping you achieve optimal health and esthetics with best coaches & health experts.</p>
                </div>
            </div>
        </div>
<!--         <div class="row">
            <div class="col-xl-12">
                <div class="service-bottom-button clearfix">
                    <a href="#">
                        <div class="icon-box-one">
                            <span class="icon-two one"></span>    
                        </div>
                        <div class="icon-box-two">
                            <span class="icon-arrows-3 two"></span>    
                        </div>
                        Request for Free Consultation
                    </a>
                </div>
            </div>
        </div> -->
    </div>
</section>
<!--End services area-->

<!--Start tips area-->
<section class="tips-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="title">Tips for Healthy Life</div>
            <div class="dector thm-bg-clr center"></div>
        </div>
        <div class="row">
            <!--Start single tips item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-tips-item text-center">
                    <div class="icon-holder">
                        <span class="icon-berry"></span>        
                    </div>
                    <div class="border-box"></div>
                    <div class="text-holder">
                        <h3><a href="#">Eat Healthy Food</a></h3>
                        <p>How all this mistaken idea of denouncing pleasures and praising our work.</p>
                    </div>
                    <div class="readmore-button">
                        <a href="#" class="btn-one thm-bg-clr readmore"><i class="fa fa-angle-right" aria-hidden="true"></i></a>    
                    </div>
                </div>  
            </div>
            <!--End single tips item-->
            <!--Start single tips item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-tips-item text-center">
                    <div class="icon-holder">
                        <span class="icon-transport"></span>        
                    </div>
                    <div class="border-box"></div>
                    <div class="text-holder">
                        <h3><a href="#">Exercise Regularly</a></h3>
                        <p>Complete account of system & expounds the actually master explorer.</p>
                    </div>
                    <div class="readmore-button">
                        <a href="#" class="btn-one thm-bg-clr readmore"><i class="fa fa-angle-right" aria-hidden="true"></i></a>    
                    </div>
                </div>  
            </div>
            <!--End single tips item-->
            <!--Start single tips item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-tips-item text-center">
                    <div class="icon-holder">
                        <span class="icon-food"></span>       
                    </div>
                    <div class="border-box"></div>
                    <div class="text-holder">
                        <h3><a href="#">Avoiding Bad Habits</a></h3>
                        <p>Expounds the actual teaching that great explorer of there levelon builder won.</p>
                    </div>
                    <div class="readmore-button">
                        <a href="#" class="btn-one thm-bg-clr readmore"><i class="fa fa-angle-right" aria-hidden="true"></i></a>    
                    </div>
                </div>  
            </div>
            <!--End single tips item-->
            <!--Start single tips item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-tips-item text-center">
                    <div class="icon-holder">
                        <span class="icon-hand"></span>       
                    </div>
                    <div class="border-box"></div>
                    <div class="text-holder">
                        <h3><a href="#">Follow Good Hygiene</a></h3>
                        <p>Denouncing pleasure praising pain born will completed of the system.</p>
                    </div>
                    <div class="readmore-button">
                        <a href="#" class="btn-one thm-bg-clr readmore"><i class="fa fa-angle-right" aria-hidden="true"></i></a>    
                    </div>
                </div>  
            </div>
            <!--End single tips item-->
        </div>
    </div>
</section>
<!--End tips area-->

<!--Start testimonial area-->
<!-- <section class="testimonial-area">
    <div class="container">
        <div class="sec-title">
            <div class="title left">Customer Feedback</div>
            <div class="dector thm-bg-clr"></div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="client-box owl-carousel owl-theme">
                    <div class="single-testimonial-box style2">
                        <div class="inner-box">
                            <div class="image-holder">
                                <img src="<?=base_url('resources/images/')?>testimonial/1.png" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <h2>Hardley <br>Richardson</h2>
                            </div>
                        </div>
                        <div class="text-holder">
                            <div class="icon-holder">
                                <span class="icon-interface-1"></span>
                            </div>
                            <div class="text">
                                <p>Over the past two years, he was able to slowly and steadily bring that down before having an incredible breakthrough allowed.</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <div class="border-box thm-bg-clr"></div>
                            <div class="title">
                                <h3 class="thm-clr">Lost 120 Pounds &amp; 2 Sizes </h3>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial-box style2">
                        <div class="inner-box">
                            <div class="image-holder">
                                <img src="<?=base_url('resources/images/')?>testimonial/4.png" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <h2>Melissa <br>Shredinger</h2>
                            </div>
                        </div>
                        <div class="text-holder">
                            <div class="icon-holder">
                                <span class="icon-interface-1"></span>
                            </div>
                            <div class="text">
                                <p>Over the past two years, he was able to slowly and steadily bring that down before having an incredible breakthrough allowed.</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <div class="border-box thm-bg-clr"></div>
                            <div class="title">
                                <h3 class="thm-clr">Finally Feeling Like Myself</h3>
                            </div>
                        </div>
                    </div>
                </div>       
            </div>
        </div>
            
    </div>
</section> -->
<!--End testimonial area-->

<!--Start fact counter area-->
<!-- <section class="fact-counter-area" style="background-image: url(<?=base_url('resources/images/')?>parallax-background/fact-counter-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="single-fact-counter text-center">
                    <div class="icon-holder">
                        <img src="<?=base_url('resources/images/')?>icon/fact-counter-1.png" alt="Icon">
                    </div>
                    <div class="borders"></div>
                    <div class="count-box">
                        <h1><span class="timer" data-from="1" data-to="10" data-speed="5000" data-refresh-interval="50">10</span></h1>
                        <div class="title">
                            <h3>Years of Experience</h3>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="single-fact-counter text-center">
                    <div class="icon-holder">
                        <img src="<?=base_url('resources/images/')?>icon/fact-counter-2.png" alt="Icon">
                    </div>
                    <div class="borders"></div>
                    <div class="count-box">
                        <h1><span class="timer" data-from="1" data-to="200" data-speed="5000" data-refresh-interval="50">10</span></h1>
                        <div class="title">
                            <h3>Workout Sesisons</h3>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="single-fact-counter text-center">
                    <div class="icon-holder">
                        <img src="<?=base_url('resources/images/')?>icon/fact-counter-3.png" alt="Icon">
                    </div>
                    <div class="borders"></div>
                    <div class="count-box">
                        <h1><span class="timer" data-from="1" data-to="1230" data-speed="5000" data-refresh-interval="50">1230+</span></h1>
                        <div class="title">
                            <h3>Happy Customers</h3>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="single-fact-counter text-center">
                    <div class="icon-holder">
                        <img src="<?=base_url('resources/images/')?>icon/fact-counter-4.png" alt="Icon">
                    </div>
                    <div class="borders"></div>
                    <div class="count-box">
                        <h1><span class="timer" data-from="1" data-to="360" data-speed="5000" data-refresh-interval="50">360</span></h1>
                        <div class="title">
                            <h3>Days of Programs</h3>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section> -->
<!--End fact counter area--> 

<!--Start latest blog area-->
<section class="latest-blog-area">
    <div class="container inner-content">  
        <div class="sec-title text-center">
            <div class="title">News & articles</div>
            <div class="dector center thm-bg-clr"></div>
        </div>
        <div class="row">
            <!--Start Single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post style-two">
                    <div class="img-holder">
                        <img src="<?=base_url('resources/images/')?>blog/lat-blog-1.jpg" alt="Awesome Image">
                        <div class="overlay-style-one">
                            <div class="box">
                                <div class="content">
                                    <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="date-box">
                            <h3>August 20, 2018</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#">by Erik Momsen</a></li>
                            </ul>    
                        </div>
                        <div class="text">
                            <h3 class="blog-title"><a href="#">Build an Athletic Body With In Eight Weeks Time</a></h3>
                            <p>At Integrative Nutrition, we teach the concept of Primary Food which is everything that nourishes your life [...]</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single blog post-->
            
            <!--Start Single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post style-two">
                    <div class="img-holder">
                        <img src="<?=base_url('resources/images/')?>blog/lat-blog-2.jpg" alt="Awesome Image">
                        <div class="overlay-style-one">
                            <div class="box">
                                <div class="content">
                                    <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="date-box">
                            <h3>Octobor 09, 2018</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#">by Erik Momsen</a></li>
                            </ul>    
                        </div>
                        <div class="text">
                            <h3 class="blog-title"><a href="#">Stop Getting Annoyed If You Want Better Health</a></h3>
                            <p>We teach the concept of Primary sed Food which that nourishes your life perspiciatis seds unde omniste [...]</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single blog post-->
            
            <!--Start Single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post style-two">
                    <div class="img-holder">
                        <img src="<?=base_url('resources/images/')?>blog/lat-blog-3.jpg" alt="Awesome Image">
                        <div class="overlay-style-one">
                            <div class="box">
                                <div class="content">
                                    <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="date-box">
                            <h3>August 03, 2018</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#">by Erik Momsen</a></li>
                            </ul>    
                        </div>
                        <div class="text">
                            <h3 class="blog-title"><a href="#">Yoga and Healthy Food Making Your Family Happy</a></h3>
                            <p>At Integrative Nutrition, we teach the concept of Primary Food which is everything that nourishes your life [...]</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single blog post-->
        </div>
    </div>
</section>
<!--End latest blog area--> 

<!--Start consultation area-->
<section class="consultation-area">
    <div class="consultation-bg">
        <img src="<?=base_url('resources/images/')?>pattern/consultation-bg.jpg" alt="Bg Image">
    </div>
    <div class="container inner-content">
        <div class="img-box">
            <img src="<?=base_url('resources/images/')?>resources/consulation-img.png" alt="Awesome Image">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="sec-title">
                    <div class="title left">Get in touch</div>
                    <div class="dector thm-bg-clr"></div>
                </div>
                <div class="text">
                    <p><span>For Enquiry:</span> Fill our short consultation form or you can also send us an <span>email</span> and we’ll get in touch shortly, or Troll Free Number – <span>(+91) 00-700-6202.</span></p>
                    <div class="button">
                        <a href="#" class="btn-two"><span class="icon-arrows-4"></span>Leave a message</a>    
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="consultation">
                    <div id="getInTouchFormMsgDiv" class="text-success" style="display:none;">
                        <p>Message successfully sent!</p>
                    </div>
                    <form class="consultation-form" id="getInTouchForm" method="post" action="#">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-box">
                                            <input id="getInTouchFormName" tabindex="1" type="text" name="firstname" value="" placeholder="First Name">
                                        </div>
                                        <div class="single-box">
                                            <input type="text" name="phone" tabindex="3" value="" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-box">
                                            <input type="text" name="lastname" tabindex="2" value="" placeholder="Last Name">
                                        </div>
                                        <div class="single-box"> 
                                            <input type="email" name="email" tabindex="4" value="" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="single-box"> 
                                            <textarea name="message" tabindex="5" placeholder="Message..."></textarea> 
                                            <button type="submit"><span class="icon-arrows-4"></span></button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End consultation area-->   
  
<!--Start footer area-->  
<footer class="footer-area" style="background-color: #000">
    <div class="container">
        <div class="row">
            <!--Start single footer widget-->
            <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                <div class="single-footer-widget">
                    <div class="our-info">
                        <!-- <div class="footer-logo">
                            <a href="<?=base_url()?>">
                                <img src="<?=base_url('resources/images/')?>footer/footer-logo.png" alt="Awesome Logo">
                            </a>
                        </div> -->
                        <div class="info-text">
                            <p>Your transformation is our passion & We are personal trainers, trained nutritionist, your supplement experts... Read More</p>
                        </div>
                        <div class="quick-contact">
                            <h5>Quick Contact</h5>
                            <ul>
                                <li>198 Tech RoadBridge Str, Newyork 11226 USA.</li>
                                <li>+8 (321) 666-12-34 | austins@behelalthy.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--End single footer widget-->
            <!--Start single footer widget-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                <div class="single-footer-widget martop pdtop-50">
                    <div class="title">
                        <h3>Instagram</h3>
                    </div>
                    <ul class="instagram">
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-1.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-2.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-3.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-4.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-5.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-6.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-7.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="<?=base_url('resources/images/')?>footer/instagram-8.jpg" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>    
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <!--End single footer widget-->
            <!--Start single footer widget-->
            <div class="col-xl-4 col-lg-10 col-md-6 col-sm-12">
                <div class="single-footer-widget martop pdtop-50 mar-top50">
                    <div class="title">
                        <h3>Newsletter</h3>
                        <div class="decor thm-bg-clr"></div>
                    </div>
                    <div class="subscribe-form">
                        <div class="text">
                            <p>You will get many interesting things for the strengthening of your body.</p>
                        </div>
                        <div id="newsletterSuccessMsgDiv" class="text-success" style="display:none;">
                            <p>We will notify you in future!</p>
                        </div>
                        <form action="#" id="newsletterForm">
                            <input id="newsletterEmail" type="text" name="email" placeholder="Your email id">
                            <button id="newsletterBtn" class="thm-bg-clr" type="button"><span class="icon-paper"></span></button>
                        </form>
                    </div>
                    <div class="footer-social-links">
                        <span>Stay Connected:</span> 
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul>   
                    </div>        
                </div>
            </div>
            <!--End single footer widget-->
        </div>
    </div>
    
    <div class="footer-bottom">
        <div class="container inner-content">
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                    <ul class="footer-menu">
                        <li><a href="<?=base_url('login')?>" class="btn-two"><span class="icon-arrows-4"></span>Parents Login</a></li>
                        <li><a href="<?=base_url('login')?>">School Login</a></li>
                    </ul>
                </div>
                <div class="col-xl-8 col-lg-6 col-md-12 col-sm-12">
                    <div class="copyright-text pull-left">
                        <p>Copyrights © <?php echo date('Y',time())?> <a class="thm-clr" href="http://infocaresolutions.in/">Info care solutions</a> All Right Reserved.</p>
                    </div>   
                </div>
            </div>
        </div>    
    </div> 
    
</footer>   
<!--End footer area--> 

</div>  

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target thm-bg-clr" data-target="html"><span class="fa fa-angle-up"></span></div>

<!-- Color Palate / Color Switcher -->
<div class="color-palate">
    <div class="color-trigger">
        <i class="fa fa-gear"></i>
    </div>
    <div class="color-palate-head">
        <h6>Choose Your Color</h6>
    </div>
    <div class="various-color clearfix">
        <div class="colors-list">
            <span class="palate default-color active" data-theme-file="<?=base_url('resources/css/')?>color-themes/default-theme.css"></span>
            <span class="palate teal-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/teal-theme.css"></span>
            <span class="palate navy-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/navy-theme.css"></span>
            <span class="palate yellow-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/yellow-theme.css"></span>
            <span class="palate blue-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/blue-theme.css"></span>
            <span class="palate purple-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/purple-theme.css"></span>
            <span class="palate olive-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/olive-theme.css"></span>
            <span class="palate red-color" data-theme-file="<?=base_url('resources/css/')?>color-themes/red-theme.css"></span>
        </div>
    </div>
    <div class="palate-foo">
        <span>You can easily change and switch the colors.</span>
    </div>
</div>
<!-- /.End Of Color Palate -->

<!-- main jQuery -->
<script src="<?=base_url('resources/js/')?>jquery.js"></script>
<!-- Wow Script -->
<script src="<?=base_url('resources/js/')?>wow.min.js"></script>
<!-- bootstrap -->
<script src="<?=base_url('resources/js/')?>bootstrap.min.js"></script>
<!-- bx slider -->
<script src="<?=base_url('resources/js/')?>jquery.bxslider.min.js"></script>
<!-- count to -->
<script src="<?=base_url('resources/js/')?>jquery.countTo.js"></script>
<script src="<?=base_url('resources/js/')?>jquery.appear.js"></script>
<!-- owl carousel -->
<script src="<?=base_url('resources/js/')?>owl.js"></script>
<!-- validate -->
<script src="<?=base_url('resources/js/')?>validation.js"></script>
<!-- mixit up -->
<script src="<?=base_url('resources/js/')?>jquery.mixitup.min.js"></script>
<!-- isotope script-->
<script src="<?=base_url('resources/js/')?>isotope.js"></script>
<!-- Easing -->
<script src="<?=base_url('resources/js/')?>jquery.easing.min.js"></script>
<!-- Gmap helper -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<!--Gmap script-->
<script src="<?=base_url('resources/js/')?>gmaps.js"></script>
<script src="<?=base_url('resources/js/')?>map-helper.js"></script>
<!-- jQuery ui js -->
<script src="<?=base_url('resources/assets/')?>jquery-ui-1.11.4/jquery-ui.js"></script>
<!-- Language Switche  -->
<script src="<?=base_url('resources/assets/')?>language-switcher/jquery.polyglot.language.switcher.js"></script>
<!-- jQuery timepicker js -->
<script src="<?=base_url('resources/assets/')?>timepicker/timePicker.js"></script>
<!-- Bootstrap select picker js -->
<script src="<?=base_url('resources/assets/')?>bootstrap-sl-1.12.1/bootstrap-select.js"></script> 
<!-- html5lightbox js -->                              
<script src="<?=base_url('resources/assets/')?>html5lightbox/html5lightbox.js"></script>
<!--Color Switcher-->
<script src="<?=base_url('resources/js/')?>color-settings.js"></script>

<!--Revolution Slider-->
<script src="<?=base_url('resources/plugins/')?>revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?=base_url('resources/plugins/')?>revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?=base_url('resources/js/')?>main-slider-script.js"></script>

<script src="<?=base_url('resources/js/')?>custom.js"></script>
<script>
var BASE_URL = "<?php echo base_url();?>";
$("#newsletterBtn").click(function(){
	var email = $('#newsletterEmail').val();
	
	if(email == ''){
		alert('Please enter email'); return false;
	}
    else{
        $.ajax({
            url: BASE_URL+'user/home/subscribe_newsletter',
            dataType : 'json',
            type: "post",
            data: {'email': email},
            success: function (response) {
                if(response.success == 1){
                    $('#newsletterEmail').val('');
                    $('#newsletterSuccessMsgDiv').show();
                }
            }
        });
    }
});
$('#newsletterEmail').keypress(function(){
    $('#newsletterSuccessMsgDiv').hide();
});

$('#getInTouchForm').validate({
    rules:{
        firstname: "required",
        lastname: "required",
        phone: "required",
        email:{
            required: true,
            email: true
        },
        message:"required"
    },
    messages:{
        firstname: 'Please enter your first name',
        lastname: 'Please enter your last name',
        phone: 'Please enter your phone number',
        email: 'Please enter your email',
        message: 'Please enter your message'
    },
    submitHandler: function(form) {
        $('#getInTouchFormMsgDiv').hide();
        $.ajax({
            url: BASE_URL+'user/home/get_in_touch',
            type: form.method,
            data: $(form).serialize(),
            dataType: 'json'       
        })
        .done(function( response ) {
            if(response.success){
                $(form)[0].reset();
                $('.selectmenu').val('-1');
                $('.selectmenu').selectmenu('refresh');
                $('#getInTouchFormMsgDiv').show();
            }
        })
        .fail(function( jqXHR, textStatus ) {
            console.log( "Request failed: " + textStatus );
        });
    }
});
$('#getInTouchFormName').keypress(function(){
    $('#getInTouchFormMsgDiv').hide();
});
</script>

</body>

</html>