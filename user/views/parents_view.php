<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>Parent Dashboard</title>
	<meta charset="utf-8">
	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/new_style.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/animate.min.css')?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body ng-app="parentsApp" ng-controller="parents" ng-cloak>
	<div class='userPage'>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">
		      	<img src="" />
		      </a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
		      <li><a href="#" id="notification"><i class="fa fa-bell" aria-hidden="true"></i></a>
		      	<ul class="dropdown-menu notifications animated" id="notifications">
                    <li class="total">
                        <span class="small">
                        You have <strong>3</strong> new notifications.
                    	</span>
                        <a href="javascript:;" class="pull-right"><span class="small">Mark all as Read</span></a>
                    </li>
                    <li class="list ps-container">

                        <ul class="dropdown-menu-list list-unstyled ps-scrollbar">
                            <li class="unread available">
                                <!-- available: success, warning, info, error -->
                                <a href="javascript:;">
                                    <div class="notice-icon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <div>
                                        <span class="name">
                                            <strong>Successful transaction of 0.01 BTC</strong>
                                            <span class="time small">15 mins ago</span>
                                        </span>
                                    </div>
                                </a>
                            </li>
                            <li class="unread away">
                                <!-- available: success, warning, info, error -->
                                <a href="javascript:;">
                                    <div class="notice-icon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div>
                                        <span class="name">
                                            <strong>4 of Pending Transactions!</strong>
                                            <span class="time small">45 mins ago</span>
                                        </span>
                                    </div>
                                </a>
                            </li>
                            <li class=" busy">
                                <!-- available: success, warning, info, error -->
                                <a href="javascript:;">
                                    <div class="notice-icon">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div>
                                        <span class="name">
                                            <strong>Cancelled Order of 200 ICO</strong>
                                            <span class="time small">1 hour ago</span>
                                        </span>
                                    </div>
                                </a>
                            </li>
                           
                            <li class=" available">
                                <!-- available: success, warning, info, error -->
                                <a href="javascript:;">
                                    <div class="notice-icon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <div>
                                        <span class="name">
                                            <strong>Great Speed Notify of 1.34 LTC</strong>
                                            <span class="time small">14th Mar</span>
                                        </span>
                                    </div>
                                </a>
                            </li>

                        </ul>

                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></li>

                    <li class="external">
                        <a href="javascript:;">
                            <span>Read All Notifications</span>
                        </a>
                    </li>
                </ul>
		      </li>
		      <li><a href="<?=base_url('login/logout')?>">Logout</a></li>
		    </ul>
		  </div>
		</nav>

		<section class="wrapper">
			<aside class="col-4 nav-right-menu">
				<ul id="nav-right">
					<li class="active dashboard"> <a data-toggle="tab" href="#dashboard"><span>Dashboard</span></a> </li>
					<li class="health"> <a data-toggle="tab" href="#health-status"><span>Health Status</span></a> </li>	
					<li class="Buddy"> <a data-toggle="tab" href="#buddy-up"><span>Buddy Up</span></a> </li>
					<li class="info"> <a data-toggle="tab" href="#personal-info"><span>Vaccination</span></a> </li>
					<li class="info"> <a data-toggle="tab" href="#personal-info"><span>Personal Info</span></a> </li>
					<li class="activity"> <a href=""><span>Activity</span></a> </li>	
					<li class="articles"> <a href=""><span>Articles</span></a> </li>	
					<li class="notification"> <a href=""><span>Notification</span></a> </li>
					<li class="payments"> <a href=""><span>Payments & Subsc</span></a> </li>	
													
				</ul> 
			</aside>
			<article class="col-8">
			<section class="dashboard_wrapper right-container fade in active" id="dashboard">
				<div class="health_score">
					<div class="row">
						<div class="col-lg-3 text-center"><h4><i class="fa fa-heart-o" aria-hidden="true"></i> Health Score</h4>
						<div class="human_body_score_wr">
						 <div class="human_body_score bg ">
						 	<img src="<?=base_url('assets/images/')?>human_body.png">
						 	<span class="score_50"></span>
						 </div>	
							<div class="progress_wr">
							 <div class="progress">
							  <div class="progress-bar bg-color2" style="width:10%"></div>
							  <div class="progress-bar bg-color3" style="width:20%"></div>
							  <div class="progress-bar bg-color1" style="width:40%"></div>
							</div> 
							</div>
						 </div>	
						</div> 
						<div class="col-lg-9">
						  <div class="gauge-container">
						    <div class="gauge"></div>
						    <div class="gauge"></div>
						    <div class="gauge"></div>
						  </div>

							<svg width="0" height="0" version="1.1" class="gradient-mask" xmlns="http://www.w3.org/2000/svg">
							  <defs>
							      <linearGradient id="gradientGauge">
							        <!-- <stop class="color-red" offset="0%"/>
							        <stop class="color-yellow" offset="17%"/> -->
							        <stop class="color-green" offset="100%"/>
							        <!-- <stop class="color-yellow" offset="87%"/>
							        <stop class="color-red" offset="100%"/> -->
							      </linearGradient>
							  </defs>  
							</svg>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8"><div class="chart_wr"><canvas id="line-chartjs-1" height="350" width="600"></canvas></div></div>
					<div class="col-lg-4">
					<div class="calendar_wr">
							<h4>Health Status Report</h4>
							<table>
								<tr><td>Sick Report</td><td>24 May</td><td><a href=""><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>
								<tr><td>Sick Report</td><td>24 May</td><td><a href=""><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>
								<tr><td>Asthma Report</td><td>24 May</td><td><a href=""><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>
								<tr><td>Allergy Report</td><td>24 May</td><td><a href=""><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>
							</table>
							<button class="btn btn-primary">View All</button>
					</div>
					</div>
				
				</div>
			</section>

				<section class="personal-info right-container fade in" id="personal-info">
					  <ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#tab1">Parent Details</a></li>
					    <li><a data-toggle="tab" href="#tab2">Children Details</a></li>
					    <li><a data-toggle="tab" href="#tab3">Emergency Contact Details</a></li>
					  </ul>

					  <div class="tab-content">
					    <div id="tab1" class="tab-pane fade in active">
					      	<!-- <h3>Parent Details</h3> -->
					      	<form ng-submit="submitparent()" class="custom parent-dtl" id="parent-dtl">
						    	<div class="form-group col-lg-6">
						    		<input type="text" ng-model="parents.father_name" placeholder="Father name" class="form-control" id="fname_1">
						    		<label for="name">Father name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" ng-model="parents.mother_name" placeholder="Mother name" class="form-control" id="mname">
						    		<label for="name">Mother name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" placeholder="Father Occupation" ng-model="parents.occupation" id="foccupation">
						    		<label for="occupation">Father Occupation</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" placeholder="Phone Number" ng-model="parents.phone_number" id="phoneno">
						    		<label for="phone">Primary Contact number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" placeholder="Mobile number" ng-model="parents.mobile_number" id="mobileno">
						    		<label for="mobile">Secondary Contact Number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="email" class="form-control" placeholder="Email Id" ng-model="parents.email" id="email">
						    		<label for="email">Email Id</label>
						  		</div>
						    	<div class="form-group col-lg-12">
						    		<textarea minlength="15" ng-model="parents.address" placeholder="Address" class="form-control" id="address"></textarea>
						    		<label for="address">Address</label>
						  		</div>
						  		<div class="form-group col-lg-12">
						  		<span class="parent_success"></span>
						  		<span class="parent_error"></span>
					      		<button class="btn btn-primary">Update Parent</button>
					      		</div>
					      	</form>
					    </div>
					    <div id="tab2" class="tab-pane fade">
					      <!-- <h3>Children Details</h3> -->
					      	<form ng-submit="submitChildren()" class="custom student-dtl" id="student-dtl">
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="sname" ng-model="students.student_name" placeholder="Student name">
						    		<label for="name">Student name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="age" ng-model="students.age" placeholder="Age">
						    		<label for="age">Age</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="sex" ng-model="students.sex" placeholder="Sex">
						    		<label for="sex">Sex</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="bgroup" ng-model="students.blood_group" placeholder="RH Type">
						    		<label for="bgroup">RH Type</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="class" ng-model="students.standard" placeholder="Class">
						    		<label for="class">Class</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="section" ng-model="students.section" placeholder="Section">
						    		<label for="section">Section</label>
						  		</div>
						  		<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" id="shlname" ng-model="students.school_id" placeholder="School Name">
						    		<label for="shlname">School Name</label>
						  		</div>
						  		<div class="form-group col-lg-12">
						  		<span class="children_success"></span>
						  		<span class="children_error"></span>
					      		<button class="btn btn-primary">Update Children</button>
					      		</div>
					      	</form>					    
					      </div>
					    <div id="tab3" class="tab-pane fade">
					      <!-- <h3>Emergency Contact Details</h3> -->
					      	<form ng-submit="submitEmergency()" class="custom emg-dtl" id="emg-dtl">
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" ng-model="emergency.name" id="name">
						    		<label for="name">Name</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" ng-model="emergency.phone_number" id="ph-no">
						    		<label for="ph-no">Primary Contact number</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" ng-model="emergency.mobile_number" id="m-no">
						    		<label for="m-no">Secondary Contact Number</label>
						  		</div>
								  
								<div class="form-group col-lg-6">
						    		<input type="text" class="form-control" ng-model="emergency.email" id="e-mail">
						    		<label for="m-no">E Mail ID</label>
						  		</div>
								  
								<div class="form-group col-lg-6">
									<textarea minlength="15" class="form-control" ng-model="emergency.address" id="address" required> <?=(isset($studData->address))?$studData->address:'';?> </textarea>
						    		<label for="m-no">Address</label>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<!-- <span>Relationship</span> -->
						    		<div class="dropdown">
									    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{emergency.relationship}}
									    <span class="caret"></span></button>
									    <ul class="dropdown-menu">
									      <li><a ng-click="selEmergency('Parent')">Parent</a></li>
									      <li><a ng-click="selEmergency('Guardian')">Guardian</a></li>
									    </ul>
									</div>
						  		</div>
						  		<span class="emergency_success"></span>
						  		<span class="emergency_error"></span>
					      		<button class="btn btn-primary update">Update</button>
					      	</form>	
					     </div>
					  </div>					
				</section>

				<section class="buddy-up right-container fade" id="buddy-up">
					<div class="buddy-search" style="background-color: '#eee';">
						<h3>SEARCH YOUR BUDDY</h3>
				      <form action="" method="" class="custom buddy-dtl">
					    	<div class="form-group col-lg-6">
					    		<input type="text" class="form-control" id="search_buddy" />					    		
					    		<!-- <label for="search_buddy">Search Buddy by Name/Roll Number</label> -->
					  		</div>
					    	<div class="form-group col-lg-6">
					    		<select class="form-control" ng-model=selectedSchool>
										<option value="0"> Select School</option>
										<option ng-repeat="x in schoolsData" value="{{x.id}}">{{x.school_name}}</option>
					    		</select>
					  		</div>
					    	<div class="form-group col-lg-6">
					    		<select class="form-control" ng-model=selectedStandard>
										<option value="0"> Select Standard</option>
										<option ng-repeat="x in standardData" value="{{x}}">{{x}}</option>
					    		</select>
					  		</div>
					    	<div class="form-group col-lg-6">
					    		<select class="form-control" ng-model=selectedSection>
										<option value="0"> Select Section</option>
										<option ng-repeat="x in sectionData" value="{{x}}">{{x}}</option>
					    		</select>
					  		</div>
					  		<div class="form-group col-lg-12"><button class="btn btn-primary" ng-click="buddySearch()">Search</button></div>
					  	</form>
					</div>
			  	<div class="col-lg-12 alert_wrapper" id="buddy-up-success" style="display:none;">
			  	<div class="alert alert-success alert-dismissible fade in">
			  	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  		Buddy Up request successfully sent
			  		</div>
			  	</div>
						<h3 class="buddy_search_title" ng-show="buddySearchResults != '' && buddySearchResults != 'No Data'">STUDENTS FOUND</h3>
					<div class="col-lg-12" ng-show="buddySearchResults != '' && buddySearchResults != 'No Data'" ng-repeat="value in buddySearchResults" style="margin-bottom: 20px;">
						<div class="col-lg-12 buddy_search_list_wrapper">
						<img src="<?=base_url('assets/images/')?>user.png">
							<p><strong>Name:</strong>	{{value.student_name}}</p>
							<p><strong>Standard:</strong>	{{value.standard}}</p>
							<p><strong>Section:</strong>	{{value.section}}</p>
							<p><strong>Roll Number:</strong>	{{value.roll_no}}</p>
							<form>
					    <button class="btn btn-primary" ng-click="sendBuddyUpRequest(value.id, students.student_name,students.section, students.standard)">Send Buddy Up Request</button>
					  </form>
						</div>
					</div>
						<div class="col-lg-12 alert_wrapper" ng-show="buddySearchResults == 'No Data'">
						<div class="alert alert-danger alert-dismissible fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							Oops...No students found by that Name/Roll Number
							</div>
						</div>
					<div class="col-lg-12" style="margin-bottom: 20px;">
						<h3 ng-show="acceptedBuddyUpReq || pendingBuddyUpReq">BUDDY LISTS</h3>			
					    <div class="buddy-list" ng-model="acceptedBuddyUpReq" ng-repeat="(key,value) in acceptedBuddyUpReq">
					    	<h4>Buddy{{key+1}} <span>{{value.type}}</span> <button class="btn btn-primary">View Health Status</button><button class="btn btn_unfollow">unfollow</button></h4>					    	
					      	<form action="" method="" class="custom buddy-dtl">
						    	<div class="form-group col-lg-6">
						    		<label for="sname">Student Name</label>
						    		<span id="sname">{{value.student_name}}</span>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<label for="name">Father name</label>
						    		<span id="fname_3">{{value.father_name}}</span>
						  		</div>
						    	<div class="form-group col-6">
						    		<label for="name">Mother name</label>
						    		<span id="mname">{{value.mother_name}}</span>
						  		</div>
					      	</form>
					    </div>
					    <div class="buddy-list pending" ng-repeat="(key,value) in pendingBuddyUpReq">
					    	<h4>Buddy{{key+1}} <span>{{value.type}}</span> <button class="btn btn-primary">View Health Status</button></h4>					    	
					      	<form action="" method="" class="custom buddy-dtl">
						    	<div class="form-group col-lg-6">
						    		<label for="sname">Student Name</label>
						    		<span id="sname">{{value.student_name}}</span>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<label for="name">Father name</label>
						    		<span id="fname_3">{{value.father_name}}</span>
						  		</div>
						    	<div class="form-group col-6">
						    		<label for="name">Mother name</label>
						    		<span id="mname">{{value.mother_name}}</span>
						  		</div>
					      	</form>
					    </div>
					  <!--   <div class="buddy-list" ng-repeat="(key,value) in rejectedBuddyUpReq">
					    	<h4>Buddy{{key+1}} <span>{{value.type}}</span> <button class="btn btn-primary">View Health Status</button><button class="btn btn_unfollow">unfollow</button></h4>					    	
					      	<form action="" method="" class="custom buddy-dtl">
						    	<div class="form-group col-lg-6">
						    		<label for="sname">Student Name</label>
						    		<span id="sname" >{{value.student_name}}</span>
						  		</div>
						    	<div class="form-group col-lg-6">
						    		<label for="name">Father name</label>
						    		<span id="fname_3">{{value.father_name}}</span>
						  		</div>
						    	<div class="form-group col-6">
						    		<label for="name">Mother name</label>
						    		<span id="mname" >{{value.mother_name}}</span>
						  		</div>
					      	</form>
					    </div> -->	
				</section>

				<section class="health-status right-container fade" id="health-status">
					<ul class="nav nav-tabs">
					    <li class="active"><a href="#htab1" data-toggle="tab">Sick Reports</a></li>
					    <li><a href="#htab2" data-toggle="tab">Medical Conditions</a></li>
					    <li><a href="#htab3" data-toggle="tab">Allergic Conditions</a></li>
					</ul>

					<div class="tab-content">
					    <div id="htab1" class="tab-pane fade in active">
					    <div class="col-lg-12">
					    	<h3>Reported sick lists <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Report a sick</button></h3>
					    	<div class="filter col-lg-12">
					    		<div class="col-lg-4 nav_list">
					    			<label>Page </label> <select class="form-control"><option>1</option><option>2</option><option>3</option></select>
					    		</div>
					    		<div class="col-lg-8 text-right filter_search"> 
						    		<input type="text" placeholder="Search by" class="form-control" name=""> <button ng-click="show_filter_options()"><i class="fa fa-filter" aria-hidden="true"></i>
									</button> 
									<div class="filter_ad_search">
										<div class="form-group">
											<label>From</label>
											<div class='input-group date' id='datetimepicker1'>
											<input type='text' class="form-control" />
											<span class="input-group-addon">
											<i class="fa fa-calendar" aria-hidden="true"></i>
											</span>
											</div>
										</div>
										<div class="form-group">
											<label>To</label>
											<div class='input-group date' id='datetimepicker1'>
											<input type='text' class="form-control" />
											<span class="input-group-addon">
											<i class="fa fa-calendar" aria-hidden="true"></i>
											</span>
											</div>
										</div>
										<div class="form-group">
											<label>Symptoms</label>
											<input type='text' class="form-control" />
										</div>
										<div class="form-group text-right">
										<button class="btn btn-primary">Apply All</button>
										</div>

									</div>
								</div>
					    	</div>
					    	<div class="clearfix"></div>
					    	<div class="desc row" ng-repeat="(k,v) in sick_data">
					      		<!-- <h3>Report {{k+1}}</h3> -->
					      		<div class="health-describ">
					      		<div class="sick_reason_list">
					      			<div class="col">
					      				
					      				<strong class="date"><span>Sick from</span>{{v.sick_from.date}}<span>{{v.sick_from.month}}</span></strong>
					      			</div>
						      		<div class="col-lg-9">
							      		<!-- <p><b>Sick from: </b>{{v.sick_from}}</p>
							      		<p><b>Sick upto: </b>{{v.sick_to}}</p> -->
					      				<p class="sick_reason"><b>Sick reason: </b>{{v.sick_reason}}</p>
						      			<p class="symptoms"><b>Symptoms: </b><span ng-repeat="(key,val) in v.symptoms">{{symptoms_txt[val]}}{{(key+1 != v.symptoms.length)?', ':''}}</span></p>
						      		</div>
						      		<div class="col">
					      				
					      				<strong class="date"><span>Sick upto</span>{{v.sick_to.date}}<span>{{v.sick_to.month}}</span></strong>
					      			</div>
						      		</div>
					      		</div>
					      	</div>

							<div class="asthmaCare">
								<form class="asthmaForm custom" ng-show="active_form == 6">
									<h5><b>Managing an asthma attack</b></h5>
									<p>Staff are trained in asthma first aid (see overleaf). Please write down anything different this child might need if they have an asthma attack:</p>
									<div class="form-group">
										<textarea minlength="15" placeholder="" class="form-control" id="asthmaAttack_1"></textarea>
									</div>
									<h5>Daily asthma management</h5>
									<div>
										<p>This child’s usual asthma signs</p>
										<div class="form-group col-lg-4">
											<input type="checkbox" name="" class="form-control" />
											<label>Cough</label>
										</div>
										<div class="form-group col-lg-4">
											<input type="checkbox" name="" class="form-control" />
											<label>Wheeze</label>
										</div>
										<div class="form-group col-lg-4">
											<input type="checkbox" name="" class="form-control" />
											<label>Difficulty breathing</label>
										</div>								
										<div class="form-group col-lg-12">
											<input type="checkbox" name="" class="form-control" />
											<label>Other (please describe)</label>
										</div>
										<div class="form-group col-lg-5">
											<input type="text" name="" class="form-control" id="others1"/>
										</div>									
									</div>
									<div style="clear: both">
										<p>Frequency and severity</p>
										<div class="form-group col-lg-3">
											<input type="checkbox" name="" class="form-control" />
											<label>Daily/most days</label>
										</div>
										<div class="form-group col-lg-5">
											<input type="checkbox" name="" class="form-control" />
											<label>Frequently (more than 5 x per year)</label>
										</div>
										<div class="form-group col-lg-4" style="padding: 0">
											<input type="checkbox" name="" class="form-control" />
											<label>Occasionally (less than 5 x per year) </label>
										</div>								
										<div class="form-group col-lg-12">
											<input type="checkbox" name="" class="form-control" />
											<label>Other (please describe)</label>
										</div>
										<div class="form-group col-lg-5">
											<input type="text" name="" class="form-control" id="others2"/>
										</div>									
									</div>		
									<div style="clear: both">
										<p>Known triggers for this child’s asthma (eg exercise*, colds/flu, smoke) — please detail: </p>
										<div class="form-group col-lg-12">
											<input type="text" name="" class="form-control" id="knowasthma"/>
										</div>						
									</div>	
									<div>
										<p>Does this child usually tell an adult if s/he is having trouble breathing? 
											<span class='radioButton'><input type="radio"><label style="padding: 0 20px 0 10px">Yes</label> <input type="radio"><label style="padding: 0 20px 0 10px">No</label></span></p>
										<p>Does this child need help to take asthma medication? 
											<span class='radioButton'><input type="radio"><label style="padding: 0 20px 0 10px">Yes</label>	<input type="radio"><label style="padding: 0 20px 0 10px">No</label></span></p>
										<p>Does this child use a mask with a spacer? 
											<span class='radioButton'><input type="radio"><label style="padding: 0 20px 0 10px">Yes</label> <input type="radio"><label style="padding: 0 20px 0 10px">No</label></span></p>
										<p>*Does this child need a blue reliever puffer medication before exercise? 
											<span class='radioButton'><input type="radio"><label style="padding: 0 20px 0 10px">Yes</label> <input type="radio"><label style="padding: 0 20px 0 10px">No</label></span></p>	
									</div>	
									<h5><b>Medication plan</b></h5>
									<p>If this child needs asthma medication, please detail below and make sure the medication and spacer/mask are supplied to staff.</p>
							  		<div class="medicationPlan">
								  		<div class="medicationPlan_container row">
								  			<div class="form-group col-lg-4" style="padding-left: 0">
									    		<textarea minlength="15" class="form-control a" id="medication"></textarea>
									    		<label for="address">Name of medication and colour</label>	  				
								  			</div>
								  			<div class="form-group col-lg-4" style="padding-left: 0">
									    		<textarea minlength="15" class="form-control a" id="noOfPuff"></textarea>
									    		<label for="address">Dose/number of puffs</label>	  				
								  			</div>
								  			<div class="form-group col-lg-2" style="padding-left: 0">
									    		<textarea minlength="15" class="form-control a" id="timeRequired"></textarea>
									    		<label for="address">Time required</label>	  				
								  			</div>			  				  
								  			<div class="col-lg-2 newRes">
								  				<button class="addNewmedicPlan"> 
								  					Add New
								  					<i class="fa fa-plus"></i>
								  				</button>
								  			</div>			
								  		</div>
								  	</div>
								</form>
							</div>					      						      	
					    </div>
					    </div>
					    <div id="htab2" class="tab-pane fade">
					      	<h3>Your childs medical conditions <button ng-show="active_form != 4" type="button" ng-click="sel_form(4)" class="btn btn-primary btn-lg">Add New Medical Condition</button> <button ng-show="active_form == 4" type="button" ng-click="sel_form(7)" class="btn btn-primary btn-lg">Cancel</button></h3>

					      	<div id="medicalContions" ng-show="active_form != 4">
						      	<div class="desc row" ng-repeat="(k,v) in med_conditions">
						      		<!-- <h3>Condition {{k+1}}</h3> -->
						      		<div class="health-describ">
						      			<div class="sick_reason_list">
						      				<div class="col">
						      					<strong class="date">{{v.created.date}}<span>{{v.created.month}}</span></strong>
						      				</div>
						      				<div class="col-lg-10">
									      		<p class="symptoms"><b>Obsersable Sign and Symptoms: </b>{{v.observSign}}</p>
									      		<p class="sick_reason"><b>Frequency and Severity: </b>{{v.frequency}}</p>
									      		<p class="sick_reason"><b>Triggers : </b>{{v.trigger}}</p>
									      		<p class="sick_reason"><b>Possible impact on school-based activities : </b>{{v.activity}}</p>
									      		<p class="symptoms"><b>Observable sign/reaction:</b> <span>{{v.first_aid.sign}}</span></p>
									      		<p class="sick_reason"><b>Reactions:</b> <span>{{v.first_aid.res}}</span></p>
						      				</div>
						      			</div>
						      		</div>
						      	</div>
					      	</div>
							<div class="genMedical" ng-show="active_form == 4">
								<form action="" method="" id="genMedical" class="custom">
							    	<div class="form-group">
							    		<textarea minlength="15" name="observSign" class="form-control a required observSign" ng-model="mc.observSign"></textarea>
							    		<label for="address">Observable signs and symptoms <span class="required">*</span></label>
							  		</div>
							    	<div class="form-group">
							    		<textarea minlength="15" name="frequency" class="form-control a required frequency" ng-model="mc.frequency"></textarea>
							    		<label for="address">Frequency and severity <span class="required">*</span></label>
							  		</div>
							    	<div class="form-group">
							    		<textarea minlength="15" name="trigger" class="form-control a required trigger" ng-model="mc.trigger"></textarea>
							    		<label for="address">Triggers (if applicable)</label>
							  		</div>
							    	<div class="form-group">
							    		<textarea minlength="15" name="activities" class="form-control a required activities" ng-model="mc.activities"></textarea>
							    		<label for="address">Possible impact on school-based activities (student’s learning, physical activities) <span class="required">*</span></label>
							  		</div>
							  		<div class="firstAid">
								  		<div class="firstAid_container row">
								  			<div class="form-group col-lg-5" style="padding-left: 0">
									    		<textarea minlength="15" name="firstaidSign" class="form-control a required firstaidSign"></textarea>
									    		<label for="address">Observable sign/reaction <span class="required">*</span></label>	  				
								  			</div>
								  			<div class="form-group col-lg-5" style="padding: 0">
									    		<textarea minlength="15" name="firstaidRes" class="form-control a required firstaidRes"></textarea>
									    		<label for="address">First aid response <span class="required">*</span></label>	  				
								  			</div>
								  		</div>
								  	</div>	
								  	<div class="form-group col-lg-12">
							    		<label id='form_2_Success'></label>
							  		</div>
								  	<button class="btn btn-primary" ng-click="addMedicalCondition()">Submit</button>
								</form>
							</div>					      						      	
					    </div>
					    <div id="htab3" class="tab-pane fade">
					      	<h3>Allergies<button type="button" ng-click="sel_form(5)" ng-show="active_form != 5" class="btn btn-primary btn-lg">Add Allergies</button><button ng-show="active_form == 5" type="button" ng-click="sel_form(8)" class="btn btn-primary btn-lg">Cancel</button></h3>
					      	<div id="medicalContions" ng-show="active_form != 5">
				      	      	<div class="desc row" ng-repeat="(key,allergy) in allergic_conditions">
						      		<!-- <h3>Allergy {{key+1}}</h3> -->
						      		<div class="health-describ">
						      			<div class="sick_reason_list">
						      				<div class="col">
						      					<strong class="date">{{allergy.created.date}}<span>{{allergy.created.month}}</span></strong>
						      				</div>
						      				<div class="col-lg-10">
								      			<p class="sick_reason"><b>Confirmed Allergies:</b>{{allergy.confirmed_allergies}}</p>

								      			<p class="symptoms"><b>Signs of Mild to Moderate Allergic Reaction:</b></p>
								      			<ol class="sick_reason">
								      				<li ng-repeat="(k,v) in allergy.moderate_sign">{{allergic_text.mild.signs[v]}}</li>
								      			</ol>
								      			<p class="sick_reason"><b>Action for Mild to Moderate Allergic Reaction:</b></p>
								      			<ol>
								      				<li ng-repeat="(k,v) in allergy.moderate_sign">{{allergic_text.mild.actions[v]}}</li>
								      			</ol>
								      			<p class="symptoms"><b>Watch for any one of the Following Signs of Anaphylaxis (Severe Allergic Reaction)</b></p>
								      			<ol>
								      				<li ng-repeat="(k,v) in allergy.moderate_sign">{{allergic_text.severe.signs[v]}}</li>
								      			</ol>
								      			<p class="sick_reason"><b>Action For Anaphylaxis:</b></p>
								      			<ol>
								      				<li ng-repeat="(k,v) in allergy.moderate_sign">{{allergic_text.severe.actions[v]}}</li>
								      			</ol>
								      			<p class="sick_reason"><b>Action Plan Due Date: </b>{{allergy.action_due_date}}</p>
						      				</div>
						      			</div>
						      		</div>
						      	</div>
					      	</div>
							<div class="allergicReaction" ng-show="active_form == 5">
								<form action="" method="" id="allergicReaction" class="custom">
									<div class="form-group">
							    		<textarea minlength="15" name="confirmed_allergies" class="form-control a required confirmed_allergies" id="confirmed_allergies" ng-model="mc.observSign"></textarea>
							    		<label for="confirmed_allergies">Confirmed Allergies <span class="required">*</span></label>
							  		</div>
									<div class="form-group">
										<h3>Signs of Mild to Moderate Allergic Reaction</h3>
										<ul>
											<li ng-repeat="(k,v) in allergic_text.mild.signs"><input type="checkbox" name="moderate_sign[]" value="{{k}}"> {{v}}</li>
										</ul>						
									</div>
									<div class="form-group">
										<h3>Action for Mild to Moderate Allergic Reaction</h3>
										<ul>
											<li ng-repeat="(k,v) in allergic_text.mild.actions"><input type="checkbox" name="moderate_action[]" value="{{k}}"> {{v}}</li>
										</ul>				
									</div>
									
									<div class="form-group">
										<h3>Watch for any one of the Following Signs of Anaphylaxis (Severe Allergic Reaction)</h3>
										<ul>
											<li ng-repeat="(k,v) in allergic_text.severe.signs"><input type="checkbox" name="servere_sign[]" value="{{k}}"> {{v}}</li>
										</ul>	
										<!-- <form>
											<div class="form-group">
												<label>Upload Medications</label>
											<input type="file" name="upload" class="form-control">
											</div>
										</form> -->
												
									</div>
									<div class="form-group">
										<h3>Action For Anaphylaxis</h3>
										<ul>
											<li ng-repeat="(k,v) in allergic_text.severe.actions"><input type="checkbox" name="servere_action[]" value="{{k}}"> {{v}}</li>
										</ul>
									</div>
									<div class="form-group col-lg-12">
										<label for="action_due_date">Action Plan Due Date <span class="required">*</span></label>
							    		<input type="text" name='action_due_date' id='action_due_date'  autocomplete="off" required placeholder="Action Plan due for review" class="form-control datepicker">
							  		</div>
							  		<div class="form-group col-lg-12">
							  			<div class="form-group col-lg-12">
								    		<label id='form_3_Success'></label>
								  		</div>
										<button class="btn btn-primary" ng-click="addAllergicConditions()">Submit</button>
									</div>
								</form>
							</div>					      						      	
					    </div>					    					    
					</div>
				</section>

			</article>
		</section>		
	</div>
	  <!-- Modal -->
	  <div class="modal fade addNewCase" id="myModal" role="dialog">
	    <div class="modal-dialog">			    
	      <!-- Modal content-->
	      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Add Sick Report</h4>
		        </div>
		        <div class="modal-body">
		        	<p>Sick from and to date <span class="required">*</span></p>
	        		<form class="custom parent-dtl parentAddSick" id="parentAddSick">
				    	<div class="form-group col-lg-6">
				    		<input type="text" name='sickFrom' id='sickFrom'  autocomplete="off" required placeholder="Sick From Date" class="form-control datepicker">
				  		</div>
				    	<div class="form-group col-lg-6">
				    		<input type="text" name='sickTo' id='sickTo'  autocomplete="off" required class="form-control datepicker" placeholder="To Date">
				  		</div>
				  		<div class="form-group col-lg-12">
				    		<label for="mobile">Having any of the below symptoms <span class="required">*</span></label>
				  			<div class="checkbox">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="1">Mild/Heavy Fever</label>
						    </div>
						    <div class="checkbox">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="2">Cough</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="3">Sneeze</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="4">Stuffy or Runny nose</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="5">Aches</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="6">Sore throat</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="7">Fatigue, Weakness</label>
						    </div>
						    <div class="checkbox disabled">
						      <label><input type="checkbox" class='checkSick' name="conditions[]" value="8">Headache</label>
						    </div>
				  		</div>
				    	<div class="form-group col-lg-12">
				    		<label for="mobile">Explain Sickness <span class="required">*</span></label>
				    		<textarea minlength="15" name='explSick' id='explSick' placeholder="Explain any health conditions your child facing. e.g. Fever, Cough, diarrhea, etc." class="form-control"></textarea>
				  		</div>
						<div class="form-group col-lg-12">
				    		<label id='formErr'></label>
				  		</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-default" ng-click="addUpateSickReport()"  value='Submit Report' />
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
			      	</form>
				</div>
			</div>
	     </div>
	      
	    </div>
	</div>	

</body>
  <script type="text/javascript">
  	var base_url = "<?=base_url()?>";
  	var buddyUpSentRequests = <?=$buddyUpSentRequests?>;
  	var parents_data = <?=$parents_data?>;
  	var students_data = <?=$students_data?>;
  	var schools_data = <?=$schools_data?>;

  	console.log('students_data',students_data);
  	console.log('parents_data',parents_data);
  	var emergency_data = <?=$emergency_data?>;
  	var sick_data = <?=$sick_data?>;
  	var med_conditions = <?=$med_conditions?>;
  	var allergic_conditions = <?=$allergic_conditions?>;
  	var symptoms_text = <?=$symptoms_text?>;
  	var allergic_text = <?=$allergic_text?>;
  </script>
  <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.js"></script>
  <script src="<?=base_url('assets/js/parents.js')?>"></script>
  <script type="text/javascript" src="<?=base_url('assets/js/custom.js')?>"></script>
  <script>
	  $( function() {
	    $( ".datepicker" ).datepicker({
	    	'minDate': "-30D",
	    	'maxDate': "+30D",
	    	'dateFormat': 'dd-mm-yy'
	    });
	  });
  </script>
  <style type="text/css">
  	.allergicReaction li{
  		list-style-type: none;
  	}
  	.required {
	    color: red;
	    font-size: 20px;
	    position: relative;
	    top: 5px;
	}

	#form_2_Success, #form_3_Success{
	    background: #199e1c;
		display: none;
		color: #fff;
	    padding: 15px 10px;
	    width: 50%;
	}

	label.error {
	    position: absolute;
	    margin: 20px 0px;
	    top: -55px !important;
	    color: #f50c07 !important;
	}

  </style>
  	<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
  	<script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-app.js"></script>
  	<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  	<!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-analytics.js"></script> -->
  	<!-- Add Firebase products that you want to use -->
  	<script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-auth.js"></script>
  	<!-- <script src="https://www.gstatic.com/firebasejs/7.12.0/firebase-firestore.js"></script> -->
  	<script src="https://www.gstatic.com/firebasejs/7.12.0/firebase.js"></script>
  	<script type="text/javascript">
		let userId = 0;

		/* Live Config*/
		/*var firebaseConfig = {
		  apiKey: "AIzaSyDZ7K1eKwpV7fAWxzwtsS7Z7vX7JndEyU0",
		  authDomain: "infocare-1bc19.firebaseapp.com",
		  databaseURL: "https://infocare-1bc19.firebaseio.com",
		  projectId: "infocare-1bc19",
		  storageBucket: "infocare-1bc19.appspot.com",
		  messagingSenderId: "1098835201039",
		  appId: "1:1098835201039:web:28bb5e27bd7978274de77a",
		  measurementId: "G-QLYP135T90"
		};*/

		/* Live Config*/

		/* Dev Config */
		const firebaseConfig = {
		  apiKey: "AIzaSyCIlR2mg6EESWAY8342g9DLgrA7MkM0UDM",
		  authDomain: "infocare-dev.firebaseapp.com",
		  databaseURL: "https://infocare-dev.firebaseio.com",
		  projectId: "infocare-dev",
		  storageBucket: "infocare-dev.appspot.com",
		  messagingSenderId: "518631793496",
		  appId: "1:518631793496:web:197ace684472dfd8247b2d",
		  measurementId: "G-SNM6GX9HBN"
		};
		/* Dev Config */
		firebase.initializeApp(firebaseConfig);

		//createUserWithEmailAndPassword
		//signInWithEmailAndPassword
		firebase.auth().signInWithEmailAndPassword("notifications@infocaresolutions.in", "d3d9590e65d35d464dc894547bfe4a82").catch(function(error) {
			  var errorCode = error.code;
			  var errorMessage = error.message;
			  console.log('errorCode',errorCode);
			  console.log('errorMessage',errorMessage);
		});

		/* var db = firebase.firestore();*/
		firebase.auth().onAuthStateChanged(function(user) {
		  if(user){
		    // Get a reference to the database service
		    var database = firebase.database();
		    var starCountRef = firebase.database().ref('reports/1000/'); // changes pushed to users
		    starCountRef.on('value', function(snapshot) { // Realtime listening function
		      console.log(snapshot.val());
		    });
		     
		  }else{
		    console.log('Invalid Login');
		  }
		});
		

		function writeUserData(data) {
		    //firebase.database().ref(	'reports/' + userId).set(data);
		    firebase.database().ref('reports/'+ data.report_id).set(data);
		}
	</script>

<script src="<?=base_url('assets/js/codepen.js')?>"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<script src="https://cdn3.devexpress.com/jslib/17.1.6/js/dx.all.js"></script>
<script src="<?=base_url('assets/js/chart/jquery.sparkline.min.js')?>"></script>
<script src="<?=base_url('assets/js/chart/Chart_2.9.3.min.js')?>"></script>
<script src="<?=base_url('assets/js/chart/chart-chartjs.js')?>"></script>
    
<script id="rendered-js">
$(function () {

  class GaugeChart {
    constructor(element, params) {
      this._element = element;
      this._initialValue = params.initialValue;
      this._higherValue = params.higherValue;
      this._title = params.title;
      this._subtitle = params.subtitle;
    }

    _buildConfig() {
      let element = this._element;

      return {
        value: this._initialValue,
        valueIndicator: {
          color: '#fff' },

        geometry: {
          startAngle: 180,
          endAngle: 360 },

        scale: {
          startValue: 0,
          endValue: this._higherValue,
          customTicks: [0, 250, 500, 780, 1050, 1300, 1560],
          tick: {
            length: 8 },

          label: {
            font: {
              color: '#87959f',
              size: 9,
              family: '"Open Sans", sans-serif' } } },



        title: {
          verticalAlignment: 'bottom',
          text: this._title,
          font: {
            family: '"Open Sans", sans-serif',
            color: '#fff',
            size: 10 },

          subtitle: {
            text: this._subtitle,
            font: {
              family: '"Open Sans", sans-serif',
              color: '#fff',
              weight: 700,
              size: 28 } } },



        onInitialized: function () {
          let currentGauge = $(element);
          let circle = currentGauge.find('.dxg-spindle-hole').clone();
          let border = currentGauge.find('.dxg-spindle-border').clone();

          currentGauge.find('.dxg-title text').first().attr('y', 48);
          currentGauge.find('.dxg-title text').last().attr('y', 28);
          currentGauge.find('.dxg-value-indicator').append(border, circle);
        } };


    }

    init() {
      $(this._element).dxCircularGauge(this._buildConfig());
    }}


  $(document).ready(function () {

  	let temp = ['Health Score', 'BMI Score', 'BMR Score'];
    $('.gauge').each(function (index, item) {
      let params = {
        initialValue: 780,
        higherValue: 1560,
        title: temp[index],
        subtitle: '780 ºC'};


      let gauge = new GaugeChart(item, params);
      gauge.init();
    });

    $('#random').click(function () {

      $('.gauge').each(function (index, item) {
        let gauge = $(item).dxCircularGauge('instance');
        let randomNum = Math.round(Math.random() * 1560);
        let gaugeElement = $(gauge._$element[0]);

        gaugeElement.find('.dxg-title text').last().html(`${randomNum} ºC`);
        gauge.value(randomNum);
      });
    });
  });

});
//# sourceURL=pen.js



/* Chart Js */

var randomScalingFactor = function() {
    return Math.round(Math.random() * 100)
};

var config = {
    type: 'line',
    data: {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
        datasets: [{
            label: 'Healthy',
            backgroundColor: 'rgb(34,139,34)',
            borderColor: 'rgb(34,139,34)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: false,
        }, {
            label: 'Sick',
            fill: false,
            backgroundColor: 'rgb(255, 159, 64)',
            borderColor: 'rgb(255, 159, 64)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: 'Bad Sick',
            fill: false,
            backgroundColor: 'rgb(255,0,0)',
            borderColor: 'rgb(255,0,0)',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Weekly Health Statistics'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total no of branches'
                }
            }]
        }
    }
};
var ctx = document.getElementById('line-chartjs-1').getContext('2d');
window.myLine = new Chart(ctx, config);
/* Chart Js */
</script>

</html>