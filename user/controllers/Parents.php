<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends CI_Controller {

	private $parents = 'parents';
	private $students = 'students';
	private $emergency = 'emergency_details';

	public function __construct(){
		parent::__construct();
		$funcName = $this->router->fetch_method();
        $sesNotReq = array('open_reset_pwd','change_pwd','update_pwd','user_change_pwd');
        if(!in_array($funcName,$sesNotReq)){
			if(!$this->session->parent_id) {
				if($this->uri->segment(1) == 'accept-buddy-up-request' || $this->uri->segment(1) == 'reject-buddy-up-request') {
					$var1 = $this->uri->segment(1);
					$var2 = $this->uri->segment(2);
					$var3 = $this->uri->segment(3);
					redirect('login?returnurl='.base_url($var1.'/'.$var2.'/'.$var3));
				}
				redirect('login');

			} 
        }
		
		$this->load->model('parents_model');
	}


	public function buddySearch() {
		$rawData = file_get_contents("php://input");
		$postData = json_decode($rawData,TRUE);
		$schoolId = $postData['school_id'];
		$standard = $postData['standard'];
		$section = $postData['section'];
		$searchValue = $postData['searchValue'];
		$selectedSchool = $postData['selectedSchool'];
		$selectedSection = $postData['selectedSection'];
		$selectedStandard = $postData['selectedStandard'];
		$student_id = $this->session->student_id;

		if(is_numeric($searchValue)) {
			$column = 'roll_no';
			$searchValue = filter_var($searchValue, FILTER_SANITIZE_NUMBER_INT);
			$operator = '=';
		} else {
			$searchValue = filter_var($searchValue, FILTER_SANITIZE_STRING);
			if(!preg_match("/^[a-zA-Z-'\s]+$/", $searchValue)) {
				$searchValue = '';
			} else {
				$searchValue = "%".$searchValue."%";
			}
			$column = 'student_name';
			$operator = 'LIKE';
		}

		$standardCondition = ($selectedStandard != '0') ? "AND standard=$selectedStandard": '';
		$sectionCondition = ($selectedSection != '0') ? "AND section='$selectedSection'": '';
		$schoolCondition = ($selectedSchool != '0') ? "AND school_id=$selectedSchool": '';

		$studentsData = [];	
		// echo "SELECT id,student_name, roll_no, standard, section from $this->students WHERE $column $operator '$searchValue' AND id!=$student_id $standardCondition $sectionCondition $schoolCondition";
// echo "SELECT id,student_name, roll_no, standard, section from $this->students WHERE $column $operator '$searchValue' AND school_id=$schoolId AND standard=$standard AND section='$section'";
		$searchResults = $this->db->query("SELECT id,student_name, roll_no, standard, section from $this->students WHERE $column $operator '$searchValue' AND id!=$student_id $standardCondition $sectionCondition $schoolCondition")->result_array();

		if(!empty($searchResults)) {
			foreach ($searchResults as $key => $value) {
				$studentsData[] = $searchResults[$key];
			}
			echo json_encode($studentsData);
		} else {
			echo "No Data";
		}
	}


	public function sendBuddyUpRequest() {
		$rawData = file_get_contents("php://input");
		$postData = json_decode($rawData,TRUE);
		$requestorStudentId = $postData['requestorStudentId'];
		$requesteeStudentName = $postData['requesteeStudentName'];
		$requesteeStudentSection = $postData['requesteeStudentSection'];
		$requesteeStudentStandard = $postData['requesteeStudentStandard'];
		$requesteeStudentId = $postData['requesteeStudentId'];

		$parentId = $this->db->query("SELECT parent_id from $this->students WHERE id=$requesteeStudentId")->result_array();
		$parentId = $parentId[0]['parent_id'];
		$parentEmail = $this->db->query("SELECT email from $this->parents WHERE id=$parentId")->result_array(); 
		$parentEmail = $parentEmail[0]['email'];
			$template_val =' <p>Hello Parent<b>,</p>
                        <p>You have a Buddy Up Request from '.$requesteeStudentName.'</p><br>
						
						<span> <a href="'.base_url('accept-buddy-up-request/'.$requestorStudentId.'/'.$requesteeStudentId).'" target="_blank">Accept</a></span><br>
						<br>
						<span><a href="'. base_url('reject-buddy-up-request/'.$requestorStudentId.'/'.$requesteeStudentId) .'" target="_blank">Reject</a></span><br>
						<br>';
			
			$from = 'notifications@infocaresolutions.in';
			//$to = 	$_POST['email'];
			$to = 	$parentEmail;
			// echo $parentEmail;
			$subject = 'Buddy Up Request';
			$mailSent = send_email($from, $to, $subject,$template_val,'html');
			// print_r($mailSent);
			if($mailSent) {
				$getReqSentData = $this->db->query("SELECT student_name, buddy_up_req_Sent from $this->students WHERE id=$requestorStudentId")->result_array();
				$reqSentData = $getReqSentData[0]['buddy_up_req_Sent'];
				if($reqSentData == 0) {
					$updateReqSentData = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$requesteeStudentId' WHERE id=$requestorStudentId");
				} else {
					$reqSentData .= ','.$requesteeStudentId; 
					$updateReqSentData = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$reqSentData' WHERE id=$requestorStudentId");
				}
			}	
			echo json_encode( array('success' => 1) );
		// if(!empty($searchResults)) {
		// 	foreach ($searchResults as $key => $value) {
		// 		$studentsData[] = $searchResults[$key];
		// 	}
		// 	echo json_encode($studentsData);
		// } else {
		// 	echo "No Data";
		// }
	}

	public function rejectBuddyUpRequest() {
		$requestorStudentId = $this->uri->segment(2);
		$requesteeStudentId = $this->uri->segment(3);
		$updateReqSentValue = '0';
		$getRejectedData = $this->db->query("SELECT student_name, buddy_up_rejected from $this->students WHERE id=$requestorStudentId")->result_array();
		$rejectedData = $getRejectedData[0]['buddy_up_rejected'];
		$data['action'] = 'rejected';
		$data['studentName'] = $getRejectedData[0]['student_name'];
		if($rejectedData == 0) {
			$updateRejectedData = $this->db->query("UPDATE $this->students SET buddy_up_rejected='$requesteeStudentId' WHERE id=$requestorStudentId");
		} else {
			$rejectedData .= ','.$requesteeStudentId; 
			$updateRejectedData = $this->db->query("UPDATE $this->students SET buddy_up_rejected='$rejectedData' WHERE id=$requestorStudentId");
		}
		$getReqSentData = $this->db->query("SELECT student_name, buddy_up_req_Sent from $this->students WHERE id=$requestorStudentId")->result_array();
		$reqSentData = $getReqSentData[0]['buddy_up_req_Sent'];
		$reqSentDataExplode = explode(',', $reqSentData);

		foreach ($reqSentDataExplode as $key => $value) {

			if($value == $requesteeStudentId) {
				unset($reqSentDataExplode[$key]);
			}
		}

		if(empty($reqSentDataExplode)) {

			$updateReqSentValue = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$updateReqSentValue' WHERE id=$requestorStudentId");
		} else {
			$updateReqSentValue = implode(',', $reqSentDataExplode);
			$updateReqSentValue = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$updateReqSentValue' WHERE id=$requestorStudentId");
		}

		$this->load->view('buddyup_request_link_view',$data);
	}

	public function getBuddyUpSentRequests($student_id) {
		$data = $this->db->query("SELECT buddy_up_accepted, buddy_up_rejected, buddy_up_req_Sent from $this->students WHERE id=$student_id")->result_array();
		$acceptedStudents = $data[0]['buddy_up_accepted'];
		$rejectedStudents = $data[0]['buddy_up_rejected'];
		$pendingApprovalStudents = $data[0]['buddy_up_req_Sent'];
		// $acceptedData = $this->db->query("SELECT id, student_name, father_name, mother_name from $this->students WHERE id IN($acceptedStudents)")->result_array();
		$acceptedData = $this->db->query("SELECT s.id, p.father_name,p.mother_name, s.student_name FROM parents AS p LEFT JOIN students AS s ON p.id = s.parent_id WHERE s.id IN($acceptedStudents)")->result_array();	
		foreach ($acceptedData as $key => $value) {
			$acceptedData[$key]['type'] = 'accepted';
		}	
		// $acceptedData = json_encode($acceptedData);
		$rejectedData = $this->db->query("SELECT s.id, p.father_name,p.mother_name, s.student_name FROM parents AS p LEFT JOIN students AS s ON p.id = s.parent_id WHERE s.id IN($rejectedStudents)")->result_array();		
		foreach ($rejectedData as $key => $value) {
			$rejectedData[$key]['type'] = 'rejected';
		}	
		// $rejectedData = json_encode($rejectedData);
		$pendingData = $this->db->query("SELECT s.id, p.father_name,p.mother_name, s.student_name FROM parents AS p LEFT JOIN students AS s ON p.id = s.parent_id WHERE s.id IN($pendingApprovalStudents)")->result_array();		
		foreach ($pendingData as $key => $value) {
			$pendingData[$key]['type'] = 'pending';
		}	
		// $pendingData = json_encode($pendingData);
		return array($acceptedData,$rejectedData,$pendingData);
	}

	public function acceptBuddyUpRequest() {
		$requestorStudentId = $this->uri->segment(2);
		$requesteeStudentId = $this->uri->segment(3);
		$updateReqSentValue = '0';

		$getAcceptedData = $this->db->query("SELECT student_name, buddy_up_accepted from $this->students WHERE id=$requestorStudentId")->result_array();

		$acceptedData = $getAcceptedData[0]['buddy_up_accepted'];
		$data['action'] = 'accepted';
		$data['studentName'] = $getAcceptedData[0]['student_name'];
		if($acceptedData == 0) {
			$updateAcceptedData = $this->db->query("UPDATE $this->students SET buddy_up_accepted='$requesteeStudentId' WHERE id=$requestorStudentId");
		} else {
			$acceptedData .= ','.$requesteeStudentId;
			$updateAcceptedData = $this->db->query("UPDATE $this->students SET buddy_up_accepted='$acceptedData' WHERE id=$requestorStudentId");
		}

		$getReqSentData = $this->db->query("SELECT student_name, buddy_up_req_Sent from $this->students WHERE id=$requestorStudentId")->result_array();
		$reqSentData = $getReqSentData[0]['buddy_up_req_Sent'];
		$reqSentDataExplode = explode(',', $reqSentData);

		foreach ($reqSentDataExplode as $key => $value) {

			if($value == $requesteeStudentId) {
				unset($reqSentDataExplode[$key]);
			}
		}

		if(empty($reqSentDataExplode)) {

			$updateReqSentValue = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$updateReqSentValue' WHERE id=$requestorStudentId");
		} else {
			$updateReqSentValue = implode(',', $reqSentDataExplode);
			$updateReqSentValue = $this->db->query("UPDATE $this->students SET buddy_up_req_Sent='$updateReqSentValue' WHERE id=$requestorStudentId");
		}
		$this->load->view('buddyup_request_link_view',$data);
	}


	public function studView($student_id)
	{
		$this->session->set_userdata('student_id',$student_id);
		$student_id = $this->session->student_id;
		$parent_id = $this->session->parent_id;
		//$data['students_data'] = json_encode($this->parents_model->getStudentAllData($student_id));
	
		$selArr = array('father_name','mother_name','occupation','phone_number','mobile_number','email','address','deleted');
		$parents_data = $this->parents_model->get_row_array($this->parents,array('id' => $parent_id),$selArr);
		$data['parents_data'] = json_encode($parents_data);
		$data['buddyUpSentRequests'] = json_encode($this->getBuddyUpSentRequests($student_id));
		$selstArr = array('id','school_id','student_name','standard','sex','section','age','blood_group','deleted');
		$students_data = $this->parents_model->get_row_array($this->students,array('id'=>$student_id),$selstArr);
		$data['students_data'] = json_encode($students_data); 
		$schools_data =  $this->db->query('select id, school_name from schools')->result_array();
		$schools_data = json_encode($schools_data);
		$data['schools_data'] = $schools_data;

		$selstArr = array('name','relationship','phone','mobile','email','address');
		$emergency_data = $this->parents_model->get_row_array($this->emergency,array('stud_id'=>$student_id),$selstArr);
		$data['emergency_data'] = json_encode($emergency_data);

		$data['sick_data'] = $this->getSick();
		$data['symptoms_text'] = json_encode($this->config->item('symptoms_text'));
		$data['allergic_text'] = json_encode($this->config->item('allergic_text'));

		$data['med_conditions'] = $this->fetchMedicalConditions();
		$data['allergic_conditions'] = $this->fetchAllergicConditions();

		$this->load->view('parents_view',$data);
	}


	public function save_emergency_details(){
		$respArr['status'] = 0;
		$respArr['message'] = 'Something went wrong';
		
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required');
        $this->form_validation->set_rules('relationship', 'Relationship Type', 'required');

        if ($this->form_validation->run())
        {
			$student_id = $this->session->student_id;
	        $where = array('stud_id' => $student_id);
	        $dataArr = array(
	        	'stud_id' => $student_id,
	        	'name' => $this->input->post('name'),
	        	'relationship' => $this->input->post('relationship'),
	        	'mobile' => $this->input->post('mobile_number'),
	        	'phone' => $this->input->post('phone_number'),
	        	'email' => $this->input->post('email'),
	        	'address' => $this->input->post('address'),
	        );

	        $res = $this->parents_model->insert_or_update($this->emergency, $dataArr, $where);
	    	$respArr['status'] = 1;
			$respArr['message'] = 'Successfully Saved';
        }else{
        	$form_err = $this->form_validation->error_array();
            $respArr['message'] = $form_err;
        }
        echo json_encode($respArr);
	}

	public function save_children_details(){
		$respArr['status'] = 0;
		$respArr['message'] = 'Something went wrong';
		
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('student_name', 'Student name', 'required');
        $this->form_validation->set_rules('age', 'Age', 'required');
        $this->form_validation->set_rules('sex', 'Sex', 'required');
        $this->form_validation->set_rules('blood_group', 'RH Type', 'required');
        $this->form_validation->set_rules('standard', 'Class', 'required');
        $this->form_validation->set_rules('section', 'Section', 'required');
        $this->form_validation->set_rules('school_id', 'School Name', 'required');        

        if ($this->form_validation->run())
        {
			$student_id = $this->session->student_id;
	        $where = array('id' => $student_id);
	        $dataArr = array(
	        	'student_name' => $this->input->post('student_name'),
	        	'age' => $this->input->post('age'),
	        	'sex' => $this->input->post('sex'),
	        	'blood_group' => $this->input->post('blood_group'),
	        	'standard' => $this->input->post('standard'),
	        	'section' => $this->input->post('section'),
	        	'school_id' => $this->input->post('school_id'),
	        );

	        $res = $this->parents_model->insert_or_update($this->students, $dataArr, $where);
	    	$respArr['status'] = 1;
			$respArr['message'] = 'Successfully Saved';
        }else{
        	$form_err = $this->form_validation->error_array();
            $respArr['message'] = $form_err;
        }
        echo json_encode($respArr);
	}

	public function save_parent_details(){
		$respArr['status'] = 0;
		$respArr['message'] = 'Something went wrong';
		
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('father_name', 'Father name', 'required');
        $this->form_validation->set_rules('mother_name', 'Mother name', 'required');
        $this->form_validation->set_rules('occupation', 'Father Occupation', 'required');
        $this->form_validation->set_rules('phone_number', 'Primary Contact number', 'required');
        $this->form_validation->set_rules('mobile_number', 'Secondary Contact Number', 'required');
        $this->form_validation->set_rules('email', 'Email Id', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');        

        if ($this->form_validation->run())
        {
			$parent_id = $this->session->parent_id;
	        $where = array('id' => $parent_id);
	        $dataArr = array(
	        	'father_name' => $this->input->post('father_name'),
	        	'mother_name' => $this->input->post('mother_name'),
	        	'occupation' => $this->input->post('occupation'),
	        	'phone_number' => $this->input->post('phone_number'),
	        	'mobile_number' => $this->input->post('mobile_number'),
	        	'email' => $this->input->post('email'),
	        	'address' => $this->input->post('address'),
	        );

	        $res = $this->parents_model->insert_or_update($this->parents, $dataArr, $where);
	    	$respArr['status'] = 1;
			$respArr['message'] = 'Successfully Saved';
        }else{
        	$form_err = $this->form_validation->error_array();
            $respArr['message'] = $form_err;
        }
        echo json_encode($respArr);
	}

	public function index(){
		$parent_id = $this->session->parent_id;
		$selstArr = array('id','school_id','parent_id','student_name','standard','sex','section','age','blood_group','deleted');
		$data['students_data'] = $this->parents_model->get_data($this->students,'parent_id',$selstArr,$parent_id);
		$this->load->view('choose_child',$data);
	}

	function addSick(){
		$this->load->library('mongo_db');
		$this->mongo_db->connect();

		$rawData = file_get_contents("php://input");
		$postData = json_decode($rawData,TRUE);

		$postData['sick_from'] = new MongoDB\BSON\UTCDateTime(strtotime($postData['sick_from']));
		$postData['sick_to'] = new MongoDB\BSON\UTCDateTime(strtotime($postData['sick_to']));
		$postData['type'] = 1;
		$postData['stud_id'] = $this->session->student_id;
		$postData['parent_id'] = $this->session->parent_id;
		$postData['school_id'] = $this->session->school_id;
		$postData['status'] = 1;
		$postData['report_name'] = 'General Sick Report';
		$postData['created_date'] = new MongoDB\BSON\UTCDateTime(time());

		$mongoid = $this->mongo_db->insert('sick_report',$postData);

		


		/* **Status**
			1 - Sent
			2 - Read
			3 - Approved
			4 - Rejected
		*/
		$reportsData = array(
			'r_mongo_id' => $mongoid,
			'stud_id' => $this->session->student_id,
			'type' => 1,
			'status' => 1,
			'created_date' => date('d-m-Y H:i:s',time())
		);
		$ins = $this->db->insert('reports',$reportsData);

		if(!empty($mongoid) && $ins) echo 'success'; else 'error';
	}


	private function getSick(){

		$this->load->library('mongo_db');
		$this->mongo_db->connect();

		$stud_id = $this->session->student_id;
		$this->mongo_db->where('stud_id',$stud_id);
		$this->mongo_db->where('type',1);
		$result = $this->mongo_db->get('sick_report');

		/* Mongodb date will be returning milliseconds, We got to convert */
		foreach ($result as $k => $v) {
			$result[$k]['sick_from'] = array('date' => date('j',$v['sick_from']['$date']), 'month' => date('F',$v['sick_from']['$date']));
			$result[$k]['sick_to'] = array('date' => date('j',$v['sick_to']['$date']), 'month' => date('F',$v['sick_to']['$date']));
			if(isset($result[$k]['created_date'])){
				$result[$k]['created_date'] = date('d-m-Y H:i:s',$v['created_date']['$date']);
			}
		}
		return json_encode($result);
	}

	public function addMedicalCondition(){
		$this->load->library('mongo_db');
		$this->mongo_db->connect();

		$rawData = file_get_contents("php://input");
		$postData = json_decode($rawData,TRUE);

		$postData['type'] = 2;
		$postData['stud_id'] = $this->session->student_id;
		$postData['parent_id'] = $this->session->parent_id;
		$postData['school_id'] = $this->session->school_id;
		$postData['report_name'] = 'General Medical Condition';
		$postData['created_date'] = new MongoDB\BSON\UTCDateTime(time());
		
		$ins = $this->mongo_db->insert('sick_report',$postData);
		if($ins) echo 'success';
	}



	private function fetchMedicalConditions(){
		$this->load->library('mongo_db');
		$this->mongo_db->connect();
		$stud_id = $this->session->student_id;
		$this->mongo_db->where('stud_id',$stud_id);
		$this->mongo_db->where('type',2);
		$result = $this->mongo_db->get('sick_report');

		/* Mongodb date will be returning milliseconds, We got to convert */
		foreach ($result as $k => $v) {
			$result[$k]['created_date'] = date('d-m-Y H:i:s',$v['created_date']['$date']);
			$result[$k]['created'] = array('date' => date('j',$v['created_date']['$date']), 'month' => date('F',$v['created_date']['$date']));
		}
		return json_encode($result);
	}


	public function addAllergicCondition(){
		$this->load->library('mongo_db');
		$this->mongo_db->connect();
		$rawData = file_get_contents("php://input");
		$postData = json_decode($rawData,TRUE);
		$postData['type'] = 3;
		$postData['stud_id'] = $this->session->student_id;
		$postData['parent_id'] = $this->session->parent_id;
		$postData['school_id'] = $this->session->school_id;
		$postData['report_name'] = 'Allergic Sign/Reactions';
		$postData['created_date'] = new MongoDB\BSON\UTCDateTime(time());
		$postData['action_due_date'] = new MongoDB\BSON\UTCDateTime(strtotime($postData['action_due_date']));
		$ins = $this->mongo_db->insert('sick_report',$postData);
		if($ins) echo 'success';
	}


	private function fetchAllergicConditions(){
		$this->load->library('mongo_db');
		$this->mongo_db->connect();
		$stud_id = $this->session->student_id;
		$this->mongo_db->where('stud_id',$stud_id);
		$this->mongo_db->where('type',3);
		$result = $this->mongo_db->get('sick_report');

		/* Mongodb date will be returning milliseconds, We got to convert */
		foreach ($result as $k => $v) {
			$result[$k]['created_date'] = date('d-m-Y H:i:s',$v['created_date']['$date']);
			$result[$k]['created'] = array('date' => date('j',$v['created_date']['$date']), 'month' => date('F',$v['created_date']['$date']));
			$result[$k]['action_due_date'] = date('d-m-Y',$v['action_due_date']['$date']);
		}
		return json_encode($result);
	}

	public function open_reset_pwd(){
		$this->load->view('reset_pwd');		
	}
	
	public function reset_pwd(){
		
		$chk_email = $this->db->query('select * from users where email = "'.$_POST['email'].'"')->row();
		if(empty($chk_email))
			$chk_email = $this->db->query('select * from parents where email = "'.$_POST['email'].'"')->row();
		//echo '<pre>'; print_r($chk_email); die;
		if(!empty($chk_email)){
			$template_val =' <p>Hi <b>'.ucfirst($chk_email->name).',</p>
                        <p>Kindly click the below link to reset the password of your account.</p><br>
						
						<span><b>Link</b> : <a href="'. base_url('change_pwd/'.$chk_email->id) .'" target="_blank">Click to Login</a></span><br>
						<br>
						<br>
                        <p>Good luck! Hope it works.</p>';
			
			$from = 'shabeer77777@gmail.com';
			//$to = 	$_POST['email'];
			$to = 	'sun123@yopmail.com';
			$subject = 'Forgot Password';
			send_email($from, $to, $subject,$template_val,'html');		
			echo json_encode( array('success' => 1) );
		}else{
			echo json_encode( array('success' => 0) );
			
		}
	}
	
	public function change_pwd(){
		$this->load->library('session');
		$user_id =$this->uri->segment(3);
		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
		if($chk_email != NULL){
			$this->load->view('change_pwd');		
		} 
	}
	
	
	public function update_pwd(){
		$insertArr = array(
			'password'=>md5($_POST['password'])
		);
		$this->db->update('users', $insertArr,array('id' => $_POST['user_id']));
		echo json_encode( array('success' => 1) );
	}
	
	
	public function user_change_pwd(){
		$this->load->library('session');
		$user_id =$this->session->student_id;
		$chk_email = $this->db->query('select * from users where id = "'.$user_id.'"')->row();
		if($chk_email != NULL){
			$this->load->view('change_pwd');		
		} 
	}

}

?>
