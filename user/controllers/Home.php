<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('newsletter_model'));
	}

	public function index()
	{
		if($this->session->student_id) redirect(base_url('parents'));
		$this->load->view('home_view');
	}

	public function subscribe_newsletter()
	{
		if($this->input->method()=='post'){
			$this->db->insert('newsletter',$this->input->post());
			echo json_encode( array('success' => 1) );
		}
	}

	public function get_in_touch()
	{
		if($this->input->method()=='post'){
			$from = $_POST['email'];
			$to = 'notifications@infocaresolutions.in';
			$subject = "Get in touch";

			$templateVal = '
				<span><b>Name</b> : '.$_POST['firstname'].' '.$_POST['lastname'].'</span><br><br>
				<span><b>Email</b> : '.$_POST['email'].'</span><br><br>
				<span><b>Phone</b> : '.$_POST['phone'].'</span><br><br>
				<span><b>Message</b> : <br><p>'.$_POST['message'].'</p>';

			$response = send_email($from, $to, $subject, $templateVal,'html');
			if($response){
				echo json_encode(array('success'=>1));
			}
			else{
				echo json_encode(array('success'=>0));
			}
		}
	}

}
