<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	private $parents_table = 'parents';
	private $students_table = 'students';
	private $users_table = 'users';
	private $permission_table = 'user_permissions';
	private $schools_table = 'schools';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$isReturnUrl = $this->input->get('returnurl');
		if($this->session->student_id) redirect(base_url('parents'));
		$data['returnurl'] = $isReturnUrl;
		$this->load->view('login',$data);
	}


	public function check_login(){
		$this->load->library('session');
		$this->load->model('login_model');
		$user_name = $this->input->post('user_name');
		$password = $this->input->post('user_password');
		$loginType = $this->input->post('login');
		$returnUrl = $this->input->post('return_url');

		$md5_pass = md5($password);
		if($loginType == 'parents'){
			$wherArr = array('email' => $user_name, 'password' => $md5_pass);
			$chk = $this->login_model->get_row($this->parents_table, $wherArr);
			if($chk != NULL){
				$remember = ($this->input->post('remember') == 'on') ? 1 : 0;
				$sessArr = array('user_name' => $user_name, 'parent_id' => $chk->id);
				$this->session->set_userdata($sessArr);
				if($returnUrl !== '') {
					redirect($returnUrl);
				} else {
					redirect(base_url('parents'));
				}
				
				/* $chk_school = $this->login_model->get_row($this->students_table,array('id' => $chk->student_id));
				if($chk_school != NULL){
					$remember = ($this->input->post('remember') == 'on') ? 1 : 0;
					$school_id = $chk_school->school_id;
					$sessArr = array('user_name' => $user_name, 'parent_id' => $chk->id, 'student_id' => $chk->student_id, 'school_id' => $school_id,'remember'=>$remember);
					$this->session->set_userdata($sessArr);
					redirect(base_url('parents'));
				} */
			}
			else{
				$this->session->set_flashdata('error', 'Incorrect username or password');
				redirect(base_url('login'));
			}
		}
		if($loginType == 'school'){
			$wherArr = array('email' => $user_name, 'password' => $md5_pass, 'permissions' => 2);
			$chk = $this->login_model->get_row($this->users_table, $wherArr);
			if($chk){
				$remember = ($this->input->post('remember') == 'on') ? 1 : 0;
				$sessArr = array('school_user_name' => $user_name, 'school_user_id' => $chk->id,'user_type'=>$chk->type,'school_id' => $chk->school_id,'remember'=>$remember);
				$this->session->set_userdata($sessArr);
				redirect(base_url('school/dashboard'));
			}
			else{
				$this->session->set_flashdata('error', 'Incorrect username or password');
				redirect(base_url('login'));
			}

		}
		
		//redirect(base_url());
	}

	public function logout(){
		$this->load->library('session');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('student_id');
		redirect(base_url('login'));
	}

}
