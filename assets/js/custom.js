
$(document).ready(function() {
	$('#nav-right li a').on('click', function (e) {
		e.preventDefault();
		let hash = e.target.hash.split('#');
		$('#nav-right li').removeClass('active');
		$(this).parent('li').addClass('active');
		$('article section').removeClass('in active');
		$('article section').each( function () {
			if ($(this).attr('id') == hash[1]) {
				$(this).addClass('in active');
			}
		});
	});
});  

var nt = document.getElementById('notification');
nt.addEventListener('click', function () {
	if (nt.parentElement.classList.contains('open')) {
		nt.parentElement.classList.remove('open');	
		document.getElementById('notifications').classList.remove('fadeIn');
	} else {
		nt.parentElement.classList.add('open');
		document.getElementById('notifications').classList.add('fadeIn');
	}
})

const form = document.querySelectorAll('.custom');

form.forEach(function (e) {
	e.addEventListener('blur', (event) => {
	  // if (event.target.type == 'input' || event.target.type == 'textarea') {	
	  // if (event.target.value != '') {
	  	// event.target.nextElementSibling.style.top = '6px';
	  // } else {
	  	// event.target.nextElementSibling.style.top = '18px';
	  // }
	  // }
	}, true);
	e.addEventListener('focus', (event) => {
	  	// event.target.nextElementSibling.style.top = '6px';
	  // if (event.target.type == 'input' || event.target.type == 'textarea') { 	
	  // if (event.target.value == '' || event.target.value != '') {
	  	// event.target.nextElementSibling.style.top = '6px';
	  // } else {
	  	// event.target.nextElementSibling.style.top = '18px';
	  // }	  	
	// }
	}, true);
});
document.addEventListener('DOMContentLoaded', function () {
	const input = document.querySelectorAll('input');
	input.forEach(function (e) {
		// if (e.type != 'checkbox' && e.type != 'radio' && e.type != 'file') {
		// let v = document.getElementById(e.id).value;
		// if (v.length > 0) {
			// document.getElementById(e.id).nextElementSibling.style.top = '6px';
		// }
		// }
	});
	// const textArea = document.querySelectorAll('textarea');
	// textArea.forEach(function (e) {
		// let v = document.getElementById(e.id).value;
		// if (v.length > 0) {
			// document.getElementById(e.id).nextElementSibling.style.top = '6px';
		// }
	// });	
});
$(document).ready(function () {
	$('body').on('click', '.addNewFirstAid', function () {
		let getEle = `<div class="firstAid_container">
		  			<div class="form-group col-lg-5" style="padding-left: 0">
			    		<textarea minlength="15" class="form-control a" id="firstaidSign"></textarea>
			    		<label for="address">Observable sign/reaction</label>	  				
		  			</div>
		  			<div class="form-group col-lg-5" style="padding: 0">
			    		<textarea minlength="15" class="form-control a" id="firstaidRes"></textarea>
			    		<label for="address">First aid response</label>	  				
		  			</div>	  
		  			<div class="col-lg-2 newRes">
		  				<button class="addNewFirstAid"> 
		  					Add New
		  					<i class="fa fa-plus"></i>
		  				</button>
		  			</div>			
		  		</div>`;
		$('.firstAid').append(getEle);
	});
	$('body').on('click', '.addNewmedicPlan', function () {
		let getNewEle = `<div class="medicationPlan_container">
			  			<div class="form-group col-lg-4" style="padding-left: 0">
				    		<textarea minlength="15" class="form-control a" id="medication"></textarea>
				    		<label for="address">Name of medication and colour</label>	  				
			  			</div>
			  			<div class="form-group col-lg-4" style="padding-left: 0">
				    		<textarea minlength="15" class="form-control a" id="noOfPuff"></textarea>
				    		<label for="address">Dose/number of puffs</label>	  				
			  			</div>
			  			<div class="form-group col-lg-2" style="padding-left: 0">
				    		<textarea minlength="15" class="form-control a" id="timeRequired"></textarea>
				    		<label for="address">Time required</label>	  				
			  			</div>			  				  
			  			<div class="col-lg-2 newRes">
			  				<button class="addNewmedicPlan"> 
			  					Add New
			  					<i class="fa fa-plus"></i>
			  				</button>
			  			</div>			
			  		</div>`;
		$('.medicationPlan').append(getNewEle);	  	
	});
});

