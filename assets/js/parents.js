var app = angular.module('parentsApp', []);
app.controller('parents', [ '$scope','$http', function($s,$h) {
  $h.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  //$s.parents = parents_data;
  $s.student_id = students_data.id;
  $s.buddySearchResults = '';
  $s.buddyUpSentRequestsData = buddyUpSentRequests;
  $s.acceptedBuddyUpReq = $s.buddyUpSentRequestsData[0];
  $s.rejectedBuddyUpReq = $s.buddyUpSentRequestsData[1];
  $s.pendingBuddyUpReq = $s.buddyUpSentRequestsData[2];
  $s.schoolsData = schools_data;
  $s.selectedSchool = '0';
  $s.standardData = [1,2,3,4,5,6,7,8,9,10,11,12];
  $s.sectionData=['A','B','C','D','E','F','G','H','I','J','K'];
  $s.selectedStandard="0";
  $s.selectedSection="0";
  
  $.buddyUpRequestsSent = {};
  if(students_data.sex == '1'){
  	students_data.sex = 'Male';
  }else{
  	students_data.sex = 'Female';
  }
  $s.sick_data = sick_data;
  $s.med_conditions = med_conditions;
  $s.allergic_conditions = allergic_conditions;
  $s.symptoms_txt = symptoms_text;
  $s.allergic_text = allergic_text;
  $s.count = 0;
  $s.emergency = {
    'name':'',
    'relationship':'',
    'phone_number':'',
    'mobile_number':'',
    'email':'',
    'address':'',
  };

  $s.students = {
    'student_name':'',
    'age':'',
    'sex':'',
    'blood_group':'',
    'standard':'',
    'section':'',
    'school_id':'',
  };

  $s.parents = {
    'father_name':'',
    'mother_name':'',
    'occupation':'',
    'phone_number':'',
    'mobile_number':'',
    'email':'',
    'address':'',
  };
  
  if(emergency_data != null){
      $s.emergency.relationship = emergency_data.relationship;
      $s.emergency.name = emergency_data.name;
      $s.emergency.phone_number = emergency_data.phone;
      $s.emergency.mobile_number = emergency_data.mobile;
      $s.emergency.email = emergency_data.email;
      $s.emergency.address = emergency_data.address;
  }
 if(students_data != null){
      $s.students.student_name = students_data.student_name;
      $s.students.age = students_data.age;
      $s.students.sex = students_data.sex;
      $s.students.blood_group = students_data.blood_group;
      $s.students.standard = students_data.standard;
      $s.students.section = students_data.section;
      $s.students.school_id = students_data.school_id;
  }

  if(parents_data != null){
      $s.parents.father_name = parents_data.father_name;
      $s.parents.mother_name = parents_data.mother_name;
      $s.parents.occupation = parents_data.occupation;
      $s.parents.phone_number = parents_data.phone_number;
      $s.parents.mobile_number = parents_data.mobile_number;
      $s.parents.email = parents_data.email;
      $s.parents.address = parents_data.address;
  }

  $s.buddySearch = function(){
    let searchValue = $('#search_buddy').val();
    const data = {
      school_id: $s.students.school_id,
      standard: $s.students.standard,
      section: $s.students.section,
      searchValue: searchValue,
      selectedSchool: $s.selectedSchool,
      selectedSection: $s.selectedSection,
      selectedStandard: $s.selectedStandard
    }
    const url = base_url+'buddy-search';

    if(data !== '') {
      $h({ 
        method: 'POST', 
        url: url, data: data
      }).then(function successCallback(response) {
        console.log(response.data);
        $s.buddySearchResults = response.data;


      }, function errorCallback(response) {
          
      });
    }
  }


  $s.sendBuddyUpRequest = function(requesteeStudentId, requesteeStudentName,requesteeStudentSection, requesteeStudentStandard){
    const data = {
      requesteeStudentId: requesteeStudentId,
      requesteeStudentName: requesteeStudentName,
      requesteeStudentStandard: requesteeStudentStandard,
      requesteeStudentSection: requesteeStudentSection,
      requestorStudentId: $s.student_id
    }

    const url = base_url+'buddy-up-request';
      $h({ 
        method: 'POST', 
        url: url, data: data
      }).then(function successCallback(response) {

        $s.buddySearchResults = '';
        $('#buddy-up-success').show();
        setTimeout(function() {
          $('#buddy-up-success').hide();
        },3500);
          // if(response.data.status){
          //   $('.emergency_error').hide();
          //   $('.emergency_success').show().html(response.data.message);
          // }else{
          //   $('.emergency_success').hide();
          //   let message = '';
          //   for(key in response.data.message){
          //       message += response.data.message[key]+'<br/>';
          //   }
          //   $('.emergency_error').show().html(message);
          // }
      }, function errorCallback(response) {
          
      });

  }


  $s.submitEmergency = function(){
    let data = $s.toQueryString($s.emergency);
  	let url = base_url+'save-emergency';
    $h({ 
      method: 'POST', 
      url: url, data: data
    }).then(function successCallback(response) {
        if(response.data.status){
          $('.emergency_error').hide();
          $('.emergency_success').show().html(response.data.message);
        }else{
          $('.emergency_success').hide();
          let message = '';
          for(key in response.data.message){
              message += response.data.message[key]+'<br/>';
          }
          $('.emergency_error').show().html(message);
        }
    }, function errorCallback(response) {
        
    });
  }

  $s.submitChildren = function(){
    let data = $s.toQueryString($s.students);
    console.log('data',data);
    console.log('students',$s.students);
    let url = base_url+'save-children';
    $h({ 
      method: 'POST', 
      url: url, data: data
    }).then(function successCallback(response) {
        if(response.data.status){
          $('.children_error').hide();
          $('.children_success').show().html(response.data.message);
        }else{
          $('.children_success').hide();
          let message = '';
          for(key in response.data.message){
              message += response.data.message[key]+'<br/>';
          }
          $('.children_error').show().html(message);
        }
    }, function errorCallback(response) {
        
    });
  }

  
  $s.submitparent = function(){
    let data = $s.toQueryString($s.parents);    
    let url = base_url+'save-parent';
    $h({ 
      method: 'POST', 
      url: url, data: data
    }).then(function successCallback(response) {
        if(response.data.status){
          $('.parent_error').hide();
          $('.parent_success').show().html(response.data.message);
        }else{
          $('.parent_success').hide();
          let message = '';
          for(key in response.data.message){
              message += response.data.message[key]+'<br/>';
          }
          $('.parent_error').show().html(message);
        }
    }, function errorCallback(response) {
        
    });
  }

  $s.emergency.relationship = 'Choose your relationship';
  $s.selEmergency = function(val){    
  	$s.emergency.relationship = val;
  }

  $s.toQueryString = function(data){
    let queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&')
    return queryString;
  }

  $s.active_form = 1;
  $s.sel_form = function(x){
    $s.active_form = x;

    if([4,5].indexOf(x) != -1){
      $s.add_form_open = 1;
    }else{
      $s.add_form_open = 0;
    }
  }

  var report = 1000;
  $s.addUpateSickReport = function(){
      var errMsg = '';
      var sickFrom =  $('#sickFrom').val();
      var sickTo = $('#sickTo').val();
      var formVal = false;
      var atLeastOneIsChecked = false;

      $('input[name="conditions[]"]').each(function () {
        if ($(this).is(':checked')) {
          atLeastOneIsChecked = true;
        }
      });

      if((sickFrom == '' || sickTo == '') || sickFrom > sickTo){
        errMsg += 'Invalid sick from and to date';
        if(errMsg) errMsg += '<br>';
      }
      if(!atLeastOneIsChecked){
        errMsg += 'Check at least one symptom';
        if(errMsg) errMsg += '<br>';
      }
      if($('#explSick').val() == ''){
        errMsg += 'Explain about Sickness';
        if(errMsg) errMsg += '<br>';
      }

      if(errMsg == ''){
        
        let symptoms = $('input[name="conditions[]"]:checked').map(function(_, el) {
            return $(el).val();
        }).get();

        let postData = {
          'sick_from':sickFrom,
          'sick_to':sickTo,
          'sick_reason':$('#explSick').val(),
          'symptoms': symptoms,
        };

        $h({
            url: base_url+'add-sick-report',
            method: "POST",
            data: postData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response, status, headers, config) {
          if(response == 'success'){
            $s.sick_data.push(postData);
            $('#sickFrom').val('');
            $('#sickTo').val('');
            $('#explSick').val('');
            $('#myModal').modal('hide');


            var data = {
              'report_id': report++,
              'stud_id':parents_data.student_id,
              'report_type':1,
              'subject':'New Sick Report',
              'approval_status':0,
              'school_id':students_data.school_id,
            };
            writeUserData(data);
          }
        }).error(function (data, status, headers, config) {
            console.log('Error Occured!')
            console.log('data',data);
        });

        
      }else{
        $("#formErr").html(errMsg).css('color','red');
        return false;
      }
  }



  $s.addMedicalCondition = function(){
      $('#genMedical').validate({
          rules: {observSign: {required: true}, frequency: {required: true, }, trigger: {required: true, }, activities: {required: true }, firstaidSign: {required: true }, firstaidRes: {required: true } },          
          submitHandler: function (form) {

              var errMsg = '';
              var observSign =  $('.observSign').val();
              var frequency = $('.frequency').val();
              var trigger = $('.trigger').val();
              var activities = $('.activities').val();
              var firstaidSign = $('.firstaidSign').val();
              var firstaidRes = $('.firstaidRes').val();


              $("#form_2_Success").hide();

              let postData = {
                'observSign':observSign,
                'frequency':frequency,
                'trigger':trigger,
                'activity': activities,
                'first_aid': {'sign': firstaidSign, 'res': firstaidRes}
              };

              console.log('postData',postData);
              $h({
                  url: base_url+'add-medical-condition',
                  method: "POST",
                  data: postData,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function (response, status, headers, config) {
                if(response == 'success'){
                  $('#form_2_Success').html('Successfully Inserted!').show();

                  setTimeout(function(){
                    $('#form_2_Success').hide();
                  },5000);

                  $s.med_conditions.push(postData);
                  $('.observSign').val('');
                  $('.frequency').val('');
                  $('.trigger').val('');
                  $('.activities').val('');
                  $('.firstaidSign').val('');
                  $('.firstaidRes').val('');
                }
              }).error(function (data, status, headers, config) {
                  console.log('Error Occured!');
              });
          }
      });
  }


  $s.addAllergicConditions = function(){
      $('#allergicReaction').validate({
          rules: 
          {
            'moderate_sign[]': {required: true}, 
            'moderate_action[]': {required: true}, 
            'servere_sign[]': {required: true}, 
            'servere_action[]': {required: true}
          },
          messages: {
          'moderate_sign[]': {required: 'You must check at least one sign of mild alergic reaction'}, 
          'moderate_action[]': {required: 'You must check at least one action for mild allergic reaction'}, 
          'servere_sign[]': {required: 'You must check at least one sign of anaphylaxis'}, 
          'servere_action[]': {required: 'You must check at least one action for anaphylaxis'}},
          submitHandler: function (form) {
              $("#form_2_Success").hide();
              var errMsg = '';
              let moderate_sign = $('input[name="moderate_sign[]"]:checked').map(function(_, el) {
                  return $(el).val();
              }).get();
              let moderate_action = $('input[name="moderate_action[]"]:checked').map(function(_, el) {
                  return $(el).val();
              }).get();
              let servere_sign = $('input[name="servere_sign[]"]:checked').map(function(_, el) {
                  return $(el).val();
              }).get();
              let servere_action = $('input[name="servere_action[]"]:checked').map(function(_, el) {
                  return $(el).val();
              }).get();

              var confirmed_allergies = $('#confirmed_allergies').val();
              var action_due_date = $('#action_due_date').val();

              $("#form_3_Success").hide();
              let postData = {
                'moderate_sign':moderate_sign,
                'moderate_action':moderate_action,
                'servere_sign':servere_sign,
                'servere_action': servere_action,
                'confirmed_allergies': confirmed_allergies,
                'action_due_date': action_due_date
              };

              $h({
                  url: base_url+'add-allergic-condition',
                  method: "POST",
                  data: postData,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function (response, status, headers, config) {
                if(response == 'success'){
                  $('#form_3_Success').html('Successfully Inserted!').show();
                  setTimeout(function(){
                    $('#form_3_Success').hide();
                  },5000);

                  $('input[name="moderate_sign[]"]').prop('checked', false);
                  $('input[name="moderate_action[]"]').prop('checked', false);
                  $('input[name="servere_sign[]"]').prop('checked', false);
                  $('input[name="servere_action[]"]').prop('checked', false);

                  //$s.med_conditions.push(postData);
                }
              }).error(function (data, status, headers, config) {
                  console.log('Error Occured!');
              });
          }
      });
  }

$s.show_filter_options = function(){
  $('.filter_ad_search').toggle();
}
}]);
